### OpenMRS core system install method

The OpenMRS core system install method is in the /codes/openmrs-2.4.0SNAPSHOT folder.

### codes folder

在该文件夹下包含了前端源代码，CRISP数据分析原代码以及数据库相关代码。这三部分代码分别存放在了SH，SD和YF三个文件夹下。

This folder contains the front-end source code, the original CRISP data analysis code and the database related code. These three sections of code are stored in three separate folders, SH, SD and YF.

### SDK folder

该文件夹包含了两个扩展Module的源代码。在Zhongyan Zuo文件夹下是Module 1的远吗，在Shuochen Dai文件夹下是Module 2的源码。

This folder contains the source code for the two extensions Module. In the folder Zhongyan Zuo/module1 is the source code for Module 1 and in the folder Shuochen Dai is the source code for Module 2.

### Team members

Shuochen Dai: SD

Yuxiao Fan: YF

Zhongyan Zuo: ZZ

Sunyi He: SH
