package org.openmrs.module.patientclassify;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openmrs.module.patientclassify.Entity.Status;
import org.openmrs.module.patientclassify.Entity.Util;
import org.openmrs.module.patientclassify.Service.GetStatusesByDate;
import org.openmrs.module.patientclassify.Service.JudgeStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

public class TestJudgeStatus {
	private JudgeStatus judgeStatus=new JudgeStatus();

	private Status status = new Status();

	@Test
	public void testTheCorrectenessOfCalculateRisk(){
		//Manual injection
		judgeStatus.util=new Util();
		judgeStatus.getStatusesByDate = new GetStatusesByDate();

		//Set status
		status.setName("John Doe");

		List<String> chr = new ArrayList<>();
		chr.add("hypertension");
		chr.add("COPD");
		chr.add("diabetes");
		status.setChronic(chr);

		List<String> sym = new ArrayList<>();
		sym.add("fever");
		status.setSysmtoms(sym);

		List<Status> statusList = new ArrayList<>();
		statusList.add(status);
		statusList = judgeStatus.CaculateRisk(statusList);


		//Test getRisk result
		Assert.assertEquals("F", statusList.get(0).getRisk());
	}
}
