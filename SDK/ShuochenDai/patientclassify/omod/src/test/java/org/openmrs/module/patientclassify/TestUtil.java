package org.openmrs.module.patientclassify;

import org.hamcrest.collection.IsMapContaining;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.matchers.Not;
import org.openmrs.module.patientclassify.Entity.Util;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.equalTo;

public class TestUtil {
	private Util util = new Util();

	@Test
	public void testUtilGetCorrect_ChronicList(){
		Assert.assertNotNull(util.getChronicList());
	}

	@Test
	public void testUtilGetCorrect_SymptomList(){
		Assert.assertNotNull(util.getSymtomList());
	}

	@Test
	public void testGetCorrectMap(){
		Map<String, Double>map = new HashMap<>();

		map.put("chills", 5.962646845044618);

		//Test the size of the Map
		Assert.assertThat(util.getweightsmap().size(), equalTo(81));

		//Test delete incorrect weights
		Assert.assertThat(map, new Not(IsMapContaining.hasEntry("Unnamed, 78", "0.0")));


	}
}
