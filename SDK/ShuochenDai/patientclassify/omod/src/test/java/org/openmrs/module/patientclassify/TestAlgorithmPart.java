package org.openmrs.module.patientclassify;

import org.junit.Assert;
import org.junit.Test;
import org.openmrs.module.patientclassify.classify.logicalClassify;
import org.openmrs.module.patientclassify.classify.logicalRegression;
import org.openmrs.module.patientclassify.readCSVData.getCSVData;
import org.openmrs.module.patientclassify.readCSVData.getData;

import java.io.IOException;

public class TestAlgorithmPart {
	
	@Test
	public void test() throws IOException {
		try{
			getCSVData getCSVdata = new getData();
			getCSVdata
					.setCSV("/Users/delldai/Desktop/LEEDS/Project/OpenMRS/SDK/ShuochenDai/patientclassify/omod/src/main/java/org/openmrs/module/patientclassify/readCSVData/metaData/features_final_1.csv");
			double[] weights = getCSVdata.getWeights();
			Assert.assertTrue(weights[0] == 5.962646845044618);
			System.out.println("Test Succeed");
		} catch (Exception e){
			System.out.println(e);
		}

	}

	@Test
	public void testLogicalClassifyData() throws IOException {
		try {
			logicalClassify logicalClassify = new logicalClassify();
			logicalClassify.setWeights(
							"/Users/delldai/Desktop/LEEDS/Project/OpenMRS/SDK/ShuochenDai/patientclassify/omod/src/main/java/org/openmrs/module/patientclassify/readCSVData/metaData/symptoms_final_1.csv");
			double[] weights = logicalClassify.getWeights();
			Assert.assertTrue(weights[0] == 5.962646845044618);
			System.out.println("Test Succeed");
		}catch (Exception e){
			System.out.println(e);
		}
	}
	
	@Test
	public void testLogicalClassify() throws IOException {
		try{
		int[] patientSymptoms = new int[] { 1, 0, 0, 0, 0, 0,
											0, 0, 0, 0, 0, 0,
											0, 0, 0, 0, 0, 0,
											0, 0, 0, 0, 0, 0,
											0, 0, 0, 0, 0, 0,
											0, 0, 0, 0, 0, 0,
											0, 0, 0, 0, 0, 0,
											0, 0, 0, 0, 0, 0,
											0, 0, 0, 0, 0, 0,
											0, 0, 0, 0, 0, 0,
											0, 0, 0, 0, 0, 0,
											0, 0, 0, 0, 1, 1,
											1, 1, 0, 0, 0, 0,
											1, 1, 1, 1};
		logicalRegression logicalRegression = new logicalClassify();
		double result = logicalRegression.patientPredict(patientSymptoms);
		Assert.assertTrue(0 == result);
		System.out.println("result: " + result);
		} catch (Exception e){
			System.out.println(e);
		}
	}
}
