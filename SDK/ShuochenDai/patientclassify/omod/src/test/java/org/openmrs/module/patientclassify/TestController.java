package org.openmrs.module.patientclassify;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import static org.junit.Assert.*;
import org.openmrs.api.ConceptService;
import org.openmrs.api.ObsService;
import org.openmrs.api.PersonService;
import org.openmrs.api.UserService;
import org.openmrs.api.context.Context;
import org.openmrs.module.patientclassify.api.PatientclassifyService;
import org.openmrs.module.patientclassify.web.controller.PatientclassifyController;
import org.openmrs.scheduler.web.controller.TaskHelper;
import org.openmrs.test.Verifies;
import org.openmrs.web.test.BaseModuleWebContextSensitiveTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;

public class TestController extends BaseModuleWebContextSensitiveTest {
	private static final String DATASET_CONFIG_XML = "org/openmrs/web/include/PortletControllerTest-bmi.xml";

	private static final long MAX_WAIT_TIME_IN_MILLISECONDS = 2048;

	private MockHttpServletRequest mockRequest;

	private TaskHelper taskHelper;

	@Autowired
	private PatientclassifyController patientclassifyController;

	private PatientclassifyService service;
	private UserService userService;
	private ObsService obsService;
	private ConceptService conceptService;
	private PersonService personService;

	@Before
	public void setUpProject1ControllerService() throws Exception{
		executeDataSet(DATASET_CONFIG_XML);

		userService = Context.getUserService();
		obsService = Context.getObsService();
		conceptService = Context.getConceptService();
		personService = Context.getPersonService();

		mockRequest = new MockHttpServletRequest();
		mockRequest.setMethod("POST");
		mockRequest.setParameter("action", "");
		mockRequest.setParameter("taskId","1");


	}

	@Test
	@Verifies(value = "should not reschedule a task that is not currently scheduled", method = "onGet(ChartFiled)")
	@Ignore
	public void testPatientClassifyControllerOnGet() throws Exception{
		String result = patientclassifyController.onGet();

		assertNull(result);
		assertNotSame("yes", result);
	}
}
