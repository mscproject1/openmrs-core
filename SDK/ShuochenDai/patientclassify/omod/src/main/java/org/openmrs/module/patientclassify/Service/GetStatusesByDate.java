package org.openmrs.module.patientclassify.Service;

import org.openmrs.Obs;
import org.openmrs.api.ConceptService;
import org.openmrs.api.PersonService;
import org.openmrs.module.patientclassify.Dao.GetPatients;
import org.openmrs.module.patientclassify.Entity.Status;
import org.openmrs.module.patientclassify.Entity.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.*;

@Service
public class GetStatusesByDate {
    @Autowired
    GetPatients getPatients;
    @Autowired
    ConceptService conceptService;
    @Autowired
    PersonService personService;
    @Autowired
    JudgeStatus judgeStatuses;
    @Autowired
    Util util;

    public List<Status> getPatientsStatusbyDate(String date) throws ParseException {
        List<Obs> obsList = getPatients.getPatientsByDate(date);
        Map<Integer, Status> maps = new HashMap<>();
        List<String> ch = util.getChronicList();
        List<String> sy = util.getSymtomList();
        List<Status> res = new ArrayList<>();
        System.out.println(ch.size());
        System.out.println(sy.size());
        for (Obs obs : obsList) {
            System.out.println(1);
            System.out.println(obs.getConcept().getName().getName().toLowerCase());
            if (!maps.containsKey(obs.getPersonId())) {
                System.out.println(2);
                Status status = new Status();
                status.setDate(date);
                List<String> list1 = new ArrayList<>();
                List<String> list2 = new ArrayList<>();
                status.setChronic(list1);
                status.setSysmtoms(list2);
                status.setName(obs.getPerson().getPersonName().getFullName());
                maps.put(obs.getPersonId(), status);
                if (ch.contains(obs.getConcept().getName().getName().toLowerCase())) {
                    System.out.println(3);
                    System.out.println(obs.getConcept().getName().getName().toLowerCase());
                    maps.get(obs.getPersonId()).getChronic().add(obs.getConcept().getName().getName().toLowerCase());
                } else if (sy.contains(obs.getConcept().getName().getName().toLowerCase())) {
                    System.out.println(4);
                    System.out.println(obs.getConcept().getName().getName().toLowerCase());

                    maps.get(obs.getPersonId()).getSysmtoms().add(obs.getConcept().getName().getName().toLowerCase());
                }

            } else {
                if (ch.contains(obs.getConcept().getName().getName().toLowerCase())) {
                    System.out.println(5);
                    System.out.println(obs.getConcept().getName().getName().toLowerCase());
                    maps.get(obs.getPersonId()).getChronic().add(obs.getConcept().getName().getName().toLowerCase());
                }
                if (sy.contains(obs.getConcept().getName().getName().toLowerCase())) {
                    System.out.println(6);
                    System.out.println(obs.getConcept().getName().getName().toLowerCase());
                    maps.get(obs.getPersonId()).getSysmtoms().add(obs.getConcept().getName().getName().toLowerCase());
                }
            }
        }
        for (Integer i : maps.keySet()) {
            res.add(maps.get(i));

        }
        res = judgeStatuses.CaculateRisk(res);
        return res;
    }

}
