package org.openmrs.module.patientclassify.readCSVData;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * This part of the code is used to obtain the weighting data needed for logistic regression
 * setCSV is used to get the contents of a csv file
 * getWeights is used to get all weights
 */

public class getData implements getCSVData{

	private String pathToCSV;
	private List<String> weights=new ArrayList<String>();

	@Override
	public void setCSV(String CSV) {
		this.pathToCSV = CSV;
	}

	@Override
	public double[] getWeights() throws IOException {
		String row;

		BufferedReader bufferedReader = new BufferedReader(new FileReader(this.pathToCSV));

		while ((row = bufferedReader.readLine()) != null){
			String[] data_tmp = row.split(",");
			weights.add(data_tmp[1]);
		}

		weights.remove(0);

		//Transform weights list to double array
		double[] double_weights = new double[weights.size()];

		for (int i=0; i<double_weights.length; i++){
			double_weights[i] = Double.parseDouble(weights.get(i));
		}

		return double_weights;
	}

	public double[] transformWeights(List<String> weights){
		double[] double_weights = new double[weights.size()];

		for (int i=0; i<double_weights.length; i++){
			double_weights[i] = Double.parseDouble(weights.get(i));
		}

		return double_weights;
	}


}
