package org.openmrs.module.patientclassify.classify;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.StringTokenizer;

public class SVMClassify implements SVM{
	private int exampleNum;
	private int exampleDim;
	private double[] weights = new double[]{6.962432845044618, 6.13439082109756,3.6804733270369145,3.548046408967687,3.028274650854037,3.0282287529699468,2.472611034638804,2.448605526166041,2.25435404404244,2.0760205089643304,1.984044004998669,1.8049924492999083,1.7024615913382977,1.9888173810379645,1.3989237094748624,1.364487209270825
			,1.2163869056042179,0.7119272678656735,0.6608045601599103,0.5092223166591859,0.7557495588932438,0.4176483858547437,0.276685018412277,0.2434177857407318,0.1947240519241297,0.0,0.0
			,0.0,7.90321769347,0.0,0.0,0.0,0.0,0.0,-0.04237862173446189,-0.0657832177780992,-0.0955577956190659,-0.097196604390777,-0.112198958507508,-0.1712685946969481,-0.1779938612088261,-0.4112149072288746,-0.4877519169174243,-0.4880911561241913,-0.5564243201219632,-0.6076340382498105,-0.8623535256292525,-1.0692509308934728,-1.087924964648267,-1.1162220777897125,-1.1814366141420416,-1.235507702196725,-1.235507702196725,-1.3415803513118103,-1.3455244563798945
			,-1.691213125343182,-1.381213125343182,-1.383657108661803,-1.6279681031828652 ,-1.6350704731557175,-1.6909711810541446,-1.805554715314311,-2.177442123951876,-2.3562834624855755,-2.398820386231704,-2.516803955522274,-2.891343946949524,-2.9487664491289087,-3.309981319614443,-3.338012337712561,-3.3915799621249305,-3.728827321339241,-3.8941734416682543,-3.959268315422804,-4.170927382844855};;
	private double lambda;
	private double lr = 0.001;//0.00001
	private final double threshold = 0.5;
	private double cost;
	private double[] grad;
	private double[] yp;
	public SVMClassify(double paramLambda)
	{

		lambda = paramLambda;

	}

	private void CostAndGrad(double[][] X,double[] y)
	{
		cost =0;
		for(int m=0;m<exampleNum;m++)
		{
			yp[m]=0;
			for(int d=0;d<exampleDim;d++)
			{
				yp[m]+=X[m][d]* weights[d];
			}

			if(y[m]*yp[m]-1<0)
			{
				cost += (1-y[m]*yp[m]);
			}

		}

		for(int d=0;d<exampleDim;d++)
		{
			cost += 0.5*lambda* weights[d]* weights[d];
		}

		for(int d=0;d<exampleDim;d++)
		{
			grad[d] = Math.abs(lambda* weights[d]);
			for(int m=0;m<exampleNum;m++)
			{
				if(y[m]*yp[m]-1<0)
				{
					grad[d]-= y[m]*X[m][d];
				}
			}
		}
	}

	private void update()
	{
		for(int d=0;d<exampleDim;d++)
		{
			weights[d] -= lr*grad[d];
		}
	}

	public void Train(double[][] X,double[] y,int maxIters)
	{
		exampleNum = X.length;
		if(exampleNum <=0)
		{
			System.out.println("num of example <=0!");
			return;
		}
		exampleDim = X[0].length;
		weights = new double[exampleDim];
		grad = new double[exampleDim];
		yp = new double[exampleNum];

		for(int iter=0;iter<maxIters;iter++)
		{

			CostAndGrad(X,y);
			System.out.println("cost:"+cost);
			if(cost< threshold)
			{
				break;
			}
			update();

		}
	}
	private int predict(double[] x)
	{
		double pre=0;
		for(int j=0;j<x.length;j++)
		{
			pre+=x[j]* weights[j];
		}
		if(pre >=0)//这个阈值一般位于-1到1
			return 1;
		else return -1;
	}

	public void Test(double[][] testX,double[] testY)
	{
		int error=0;
		for(int i=0;i<testX.length;i++)
		{
			if(predict(testX[i]) != testY[i])
			{
				error++;
			}
		}
		System.out.println("total:"+testX.length);
		System.out.println("error:"+error);
		System.out.println("error rate:"+((double)error/testX.length));
		System.out.println("acc rate:"+((double)(testX.length-error)/testX.length));
	}

	public static void loadData(double[][]X,double[] y,String trainFile) throws IOException
	{

		File file = new File(trainFile);
		RandomAccessFile raf = new RandomAccessFile(file,"r");
		StringTokenizer tokenizer,tokenizer2;

		int index=0;
		while(true)
		{
			String line = raf.readLine();

			if(line == null) break;
			tokenizer = new StringTokenizer(line," ");
			y[index] = Double.parseDouble(tokenizer.nextToken());
			//System.out.println(y[index]);
			while(tokenizer.hasMoreTokens())
			{
				tokenizer2 = new StringTokenizer(tokenizer.nextToken(),":");
				int k = Integer.parseInt(tokenizer2.nextToken());
				double v = Double.parseDouble(tokenizer2.nextToken());
				X[index][k] = v;
				//System.out.println(k);
				//System.out.println(v);
			}
			X[index][0] =1;
			index++;
		}
	}
}
