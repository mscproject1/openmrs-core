package org.openmrs.module.patientclassify.readCSVData;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface getCSVData {
	
	void setCSV(String pathToCSV) throws IOException;
	
	double[] getWeights() throws IOException;
}
