package org.openmrs.module.patientclassify.classify;

import java.io.IOException;

public interface logicalRegression {
	
	double classifyVector(double[] X);
	
	double[] getweights();
	
	double patientPredict(int[] patientSymptoms);
}
