package org.openmrs.module.patientclassify.Service;

import org.openmrs.module.patientclassify.Entity.Status;
import org.openmrs.module.patientclassify.Entity.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import static org.openmrs.module.patientclassify.classify.logicalClassify.sigmoid;

@Service
public class JudgeStatus {
    @Autowired
    public GetStatusesByDate getStatusesByDate;
    @Autowired
    public Util util;

    public List<Status> CaculateRisk(List<Status> list) {
        Map<String, Double> weightsmap = util.getweightsmap();
        for (Status status : list) {
            double index = 0.0;
            List<String> sys = status.getSysmtoms();
            List<String> chs = status.getChronic();
            for (String ch : chs) {
                if (weightsmap.containsKey(ch)) {
                    index = index + weightsmap.get(ch);
                }
            }
            for (String sy : sys) {
                if (weightsmap.containsKey(sy)) {
                    index = index + weightsmap.get(sy);
                }
            }
            if (sigmoid(index) > 0.5)
                status.setRisk("F");
            else
                status.setRisk("T");
        }
        return list;
    }
}
