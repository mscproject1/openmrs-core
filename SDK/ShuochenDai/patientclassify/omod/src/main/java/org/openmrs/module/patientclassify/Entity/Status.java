package org.openmrs.module.patientclassify.Entity;

import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Status {

    String date;
    String name;
    List<String> chronic;
    List<String> sysmtoms;
    String risk;

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public String getName() {
        return name;
    }

    public List<String> getChronic() {
        return chronic;
    }

    public List<String> getSysmtoms() {
        return sysmtoms;
    }

    public String getRisk() {
        return risk;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setChronic(List<String> chronic) {
        this.chronic = chronic;
    }

    public void setSysmtoms(List<String> sysmtoms) {
        this.sysmtoms = sysmtoms;
    }

    public void setRisk(String risk) {
        this.risk = risk;
    }
}
