package org.openmrs.module.patientclassify.Entity;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class Util {
    String[] strings = {"chills", "5.962646845044618", "myalgia", "4.034320659009756", "running nose", "3.6804733270369145", "respiratory symptoms", "3.548046408967687", "runny nose", "3.028274650854037", "body malaise", "3.0282287529699468", "lesions on chest radiographs", "2.472611034638804", "colds", "2.448605526166041", "chest discomfort", "2.25435404404244", "none", "2.0760205089643304", "sputum", "1.984044004998669", "transient fatigue", "1.8049924492999083", "sore throat", "1.7024615913382977", "asymptomatic", "1.4288171470379645", "afebrile", "1.1889237094748624", "hypertension", "1.084487209270825", "discomfort", "0.7163869059042179", "mialgia", "0.7119272678656735", "fever", "0.6608045601599103", "headache", "0.5092223166591859", "eye irritation", "0.4557495588932438", "conjunctivitis", "0.4176483858547437", "systemic weakness", "0.276685018412277", "shortness of breath", "0.2434177857407318", "diarrhea", "0.1947240519241297", "chest pain", "0.0", "multiple organ failure", "0.0", "cold chills", "0.0", "Unnamed, 78", "0.0", "little sputum", "0.0", "severe bronchopneumonia", "0.0", "weak", "0.0", "kidney failure", "0.0", "dysphagia", "0.0", "dizziness", "0.0", "age", "-0.0657832177780992", "cardiac arrhythmia", "-0.0955577956190659", "multiple electrolyte imbalance", "-0.097196604390777", "gastritis", "-0.112198958507508", "myocardial infarction", "-0.1712685946969481", "acute renal failure", "-0.1779938612088261", "sex", "-0.4112149072288746", "cardiogenic shock", "-0.4877519169174243", "cardiopulmonary arrest", "-0.4880911561241913", "hypoxia", "-0.5564243201219632", "sepsis", "-0.6076340382498105", "emesis", "-0.8623535256292525", "respiratory stress", "-1.0692509308934728", "heart failure", "-1.087924964648267", "acute respiratory distress", "-1.1162220777897125", "acute respiratory disease syndrome", "-1.1814366141420416", "obnubilation", "-1.235507702196725", "somnolence", "-1.235507702196725", "acute kidney injury", "-1.3415803513118103", "arrhythmia", "-1.3455244563798945", "congestive heart failure", "-1.381213125343182", "severe acute respiratory infection", "-1.381213125343182", "anorexia", "-1.383657108661803", "grasp", "-1.6279681031828652", "cough", "-1.6350704731557175", "acute coronary syndrome", "-1.6909711810541446", "myocardial dysfunction", "-1.805554715314311", "gasp", "-2.177442123951876", "sensation of chill", "-2.3562834624855755", "fatigue", "-2.398820386231704", "muscular soreness", "-2.516803955522274", "difficulty breathing", "-2.891343946949524", "septic shock", "-2.9487664491289087", "expectoration", "-3.309981319614443", "dyspnea", "-3.338012337712561", "pneumonia", "-3.3915799621249305", "acute respiratory disease", "-3.728827321339241", "primary myelofibrosis", "-3.8941734416682543", "severe pneumonia", "-3.959268315422804", "chest distress", "-4.170927382844855", "malaise", "-4.373102529788287", "significant clinical suspicion", "-4.559603807949133", "acute respiratory failure", "-4.67329878584428", "acute myocardial infarction", "-4.95925481641352", "acute respiratory distress syndrome", "-6.742281263516396", "severe", "-7.888974490089728"};
    String[] chr = {"hypertension", "COPD", "diabetes", "unknown", "coronary heart disease", "prostate hypertrophy", "hepatitis B", "chronic bronchitis", "Tuberculosis", "chronic obstructive pulmonary disease", "chronic renal insufficiency", "cerebral infarction", "frequent ventricular premature beat (FVPB)", "coronary artery stenting", "encephalomalacia", "hip replacement", "Parkinson's disease for five years", "taking medicine of Madopar", "Parkinson's disease", "colon cancer surgery four years ago", "stenocardia", "coronary stenting", "hemorrhage of digestive tract", "Diabetes", "chronic kidney disease", "asthma", "valvular heart disease", "Hypertension", "hypertenstion", "upper git bleeding", "Chronic kidney disease", "Pre-renal azotemia", "cardiomyopathy", "colon cancer", "ischemic heart disease", "pre-renal azotemia", "benign prostatic hyperplasia", "bronchial asthma", "impaired fasting glucose", "dislipidemia", "renal disease", "atherosclerosis", "coronary artery disease", "atrial fibrillation", "cerebrovascular infarct", "cardiac disease", "hypothyroidism", "dyslipidemia", "prostate cancer", "tongue cancer", "cardiovascular disease", "benign prostatic hypertrophy", "cardiac dysrhythmia", "hyperthyroidism", "cerebrovascular accident infarct", "lung cancer"};
    String[] sym = {"fever", "fatigue", "systemic weakness", "runny nose", "pneumonia", "sore throat", "cough", "shortness of breath", "myalgia", "chills", "diarrhea", "conjunctivitis", "severe", "kidney failure ", "lesions on chest radiographs", "difficulty breathing", "sputum", "malaise", "running nose", "mialgia", "headache", "discomfort", "anorexia", "respiratory symptoms", "eye irritation", "transient fatigue", "gasp", "chest distress", "sensation of chill", "dizziness", "expectoration", "chest pain", "weak", "dyspnea", "muscular soreness", "chest discomfort", "primary myelofibrosis", "respiratory stress", "little sputum", "obnubilation", "somnolence", "emesis", "cold chills", "grasp", "acute respiratory distress syndrome", "severe pneumonia", "acute respiratory failure", "colds", "body malaise", "none", "dysphagia", "septic shock", "cardiogenic shock", "acute renal failure", "multiple organ failure", "cardiac arrhythmia", "heart failure", "myocardial infarction", "multiple electrolyte imbalance", "acute respiratory disease", "acute respiratory distress", "acute myocardial infarction", "acute coronary syndrome", "congestive heart failure", "severe acute respiratory infection", "sepsis", "acute kidney injury", "arrhythmia", "myocardial dysfunction", "gastritis", "cardiopulmonary arrest", "acute respiratory disease syndrome", "hypoxia", "asymptomatic", "afebrile", "significant clinical suspicion", "torpid evolution with respiratory distress", "severe bronchopneumonia"};
    Map<String, Double> res = new HashMap<>();
    List<String> ChronicList = new ArrayList<>();
    List<String> SymptomList = new ArrayList<>();

    public Util() {
        for (int i = 0; i < strings.length - 1; i = i + 2) {
            res.put(strings[i], Double.valueOf(strings[i + 1]));
        }
        for (String ch : chr) {
            ChronicList.add(ch);
        }
        for (String sy : sym) {
            SymptomList.add(sy);
        }
    }

    public Map<String, Double> getweightsmap() {

        return res;
    }

    public List<String> getChronicList() {

        return ChronicList;
    }

    public List<String> getSymtomList() {

        return SymptomList;
    }


}
