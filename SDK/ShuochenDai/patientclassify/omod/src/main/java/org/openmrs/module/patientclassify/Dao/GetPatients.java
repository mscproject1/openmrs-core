package org.openmrs.module.patientclassify.Dao;

import org.openmrs.Obs;
import org.openmrs.api.ConceptService;
import org.openmrs.api.ObsService;
import org.openmrs.util.OpenmrsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Repository
public class GetPatients {

    @Autowired
    ObsService obsService;
    @Autowired
    SimpleDateFormat simpleDateFormat;
    @Autowired
    ConceptService conceptService;

    public List<Obs> getPatientsByDate(String ds) throws ParseException {
        Date date = simpleDateFormat.parse("2020-02-04");
        Date fromDate = OpenmrsUtil.firstSecondOfDay(date);
        Date toDate = OpenmrsUtil.getLastMomentOfDay(date);
        List<Obs> obsList = obsService.getObservations(null, null, null, null, null, null, null, null,
                null, fromDate, toDate, false);
        return obsList;
    }


}
