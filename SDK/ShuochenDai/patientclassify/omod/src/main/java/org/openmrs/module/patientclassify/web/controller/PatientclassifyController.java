/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 * <p>
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.patientclassify.web.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.module.patientclassify.Entity.Status;
import org.openmrs.module.patientclassify.Service.GetStatusesByDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

/**
 * This class configured as controller using annotation and mapped with the URL of
 * 'module/patientclassify/patientclassifyLink.form'.
 */
@Controller("${rootrootArtifactId}.PatientclassifyController")

public class PatientclassifyController {

    /**
     * Logger for this class and subclasses
     */
    protected final Log log = LogFactory.getLog(getClass());
    @Autowired
    GetStatusesByDate getStatusesByDate;
    /**
     * Success form view name
     */
    private final String VIEW = "/module/patientclassify/patientclassify";

    /**
     * Initially called after the getUsers method to get the landing form name
     *
     * @return String form view name
     */
    @RequestMapping(value = "/patientclassify/module/patientclassify/patientclassify.form")
    @ResponseBody
    public String onGet() throws IOException, ParseException {
        List<Status> res = getStatusesByDate.getPatientsStatusbyDate("2020-03-20");
        for (Status status : res) {
            System.out.println(status.getName());
            System.out.println(status.getRisk());
            for (String sy : status.getSysmtoms())
                System.out.println("sy" + sy);
            for (String ch : status.getChronic())
                System.out.println("ch" + ch);
        }
        return "yes";
    }
}
