*****************************
Module Writing Requirements
*****************************

1. Create a folder with your name in the SDK folder and create the corresponding module module in that folder.

2. All module (.omod) files should be named in lower case

3. Test versions should include a "-SNAPSHOT" field after the name of a block

