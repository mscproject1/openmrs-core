package org.openmrs.module.project1;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openmrs.api.ConceptService;
import org.openmrs.api.ObsService;
import org.openmrs.api.PersonService;
import org.openmrs.api.UserService;
import org.openmrs.api.context.Context;
import org.openmrs.module.project1.api.Project1Service;
import org.openmrs.module.project1.web.controller.ChartFiled;
import org.openmrs.module.project1.web.controller.Project1Controller;
import org.openmrs.scheduler.web.controller.TaskHelper;
import org.openmrs.test.Verifies;
import org.openmrs.web.test.BaseModuleWebContextSensitiveTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public class ControllerTest extends BaseModuleWebContextSensitiveTest {
	private static final String DATASET_CONFIG_XML = "org/openmrs/web/include/PortletControllerTest-bmi.xml";

	private static final long MAX_WAIT_TIME_IN_MILLISECONDS = 2048;

	private MockHttpServletRequest mockRequest;

	private TaskHelper taskHelper;

	@Autowired
	private Project1Controller project1Controller;

	private Project1Service service;
	private UserService userService;
	private ObsService obsService;
	private ConceptService conceptService;
	private PersonService personService;
	private ChartFiled chartFiled = new ChartFiled();

	@Before
	public void setUpProject1ControllerService() throws Exception{
		executeDataSet(DATASET_CONFIG_XML);

		userService = Context.getUserService();
		obsService = Context.getObsService();
		conceptService = Context.getConceptService();
		personService = Context.getPersonService();

		mockRequest = new MockHttpServletRequest();
		mockRequest.setMethod("POST");
		mockRequest.setParameter("action", "");
		mockRequest.setParameter("taskId","1");

		chartFiled.setMed("Dexamethasone");
		chartFiled.setIndex("5092");
	}

	@Test
	@Verifies(value = "should not reschedule a task that is not currently scheduled", method = "onGet(ChartFiled)")
	public void testControllerResult() throws Exception{
		Map<String, Double> result = project1Controller.onGet(chartFiled);
		Double index = result.get("Dexamethasone");

		assertNotNull(result);
		assertSame(5092, index);
	}
}
