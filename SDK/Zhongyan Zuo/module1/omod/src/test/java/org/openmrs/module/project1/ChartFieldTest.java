package org.openmrs.module.project1;

import static org.junit.Assert.*;
import org.junit.Test;
import org.openmrs.module.project1.web.controller.ChartFiled;

public class ChartFieldTest {
	@Test
	public void ChartFieldTestCreat(){
		try {
			ChartFiled chartFiled = new ChartFiled();
			assertNotNull(chartFiled);
		}catch (Exception e){
			System.err.println(e);
		}
	}

	@Test
	public void TestChartFieldFunctions(){
		try{
			ChartFiled chartFiled = new ChartFiled();
			chartFiled.setIndex("1");
			chartFiled.setMed("testMed");

			assertEquals("1", chartFiled.getIndex());
			assertEquals("testMed", chartFiled.getMed());
		}catch (Exception e){
			System.err.println(e);
		}
	}
}
