package org.openmrs.module.project1.web.controller;

public class ChartFiled {
	
	String index;
	
	String med;
	
	public String getIndex() {
		return index;
	}
	
	public void setIndex(String index) {
		this.index = index;
	}
	
	public String getMed() {
		return med;
	}
	
	public void setMed(String med) {
		this.med = med;
	}
}
