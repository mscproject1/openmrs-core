package org.openmrs.module.project1;

import org.codehaus.jackson.map.util.JSONPObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.openmrs.api.ConceptService;
import org.openmrs.api.ObsService;
import org.openmrs.api.PersonService;
import org.openmrs.api.UserService;
import org.openmrs.api.context.Context;
import org.openmrs.module.project1.web.controller.ChartFiled;
import org.openmrs.module.project1.web.controller.Project1Controller;
import org.openmrs.test.BaseModuleContextSensitiveTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Map;

/**
 * If you want to run this test Class, please check the version of jdk in pom.xml, it should be jdk1.7.
 */
@WebAppConfiguration
public class Project1ControllerTest {
	private MockMvc mockMvc;

	@Before
	public void setUp() throws Exception{
		System.out.println("Start testing...");
		Project1Controller project1Controller = new Project1Controller();
		mockMvc = MockMvcBuilders.standaloneSetup(project1Controller).build();
	}

	@After
	public void tearDown() {
		System.out.println("Test finished...");
	}
	/**
	 * Check Project1Controller results
	 */

	@Test
	//@Ignore("Unignore if you want to make the Item class persistable, see also Item and liquibase.xml")
	public void testProject1Controller() throws Exception {

		ChartFiled chartFiled = new ChartFiled();
		chartFiled.setMed("Dexamethasone");
		chartFiled.setIndex("Blood oxygen concentration");

		try {
			MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/project1/module/project1/test1").param("i", "12"))
					.andExpect(MockMvcResultMatchers.status().isOk())
					.andDo(MockMvcResultHandlers.print())
					.andReturn();
			int status = mvcResult.getResponse().getStatus();
			System.out.println("请求状态码：" + status);
			String result = mvcResult.getResponse().getContentAsString();
			System.out.println("接口返回结果：" + result);

			// 判断接口返回json中success字段是否为true
			Assert.assertNotNull(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
