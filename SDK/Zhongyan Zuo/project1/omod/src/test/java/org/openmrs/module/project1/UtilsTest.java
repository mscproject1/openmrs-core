package org.openmrs.module.project1;

import org.junit.Test;
import org.openmrs.module.project1.Untils.Utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @Author Shuochen Dai
 */
public class UtilsTest {
	
	@Test
	public void testGetMedIdByName() {
		Utils utils = new Utils();
		
		assertEquals(12, utils.GetMedIdByName("Dexamethasone"));
		assertEquals(13, utils.GetMedIdByName("Tocilizumab"));
		assertEquals(14, utils.GetMedIdByName("Ibuprofen"));
	}
	
	@Test
	public void testgetIndexIdByName() {
		Utils utils = new Utils();
		
		assertEquals(5092, utils.getIndexIdByName("Blood oxygen concentration"));
		assertEquals(790, utils.getIndexIdByName("CREATININE"));
	}
}
