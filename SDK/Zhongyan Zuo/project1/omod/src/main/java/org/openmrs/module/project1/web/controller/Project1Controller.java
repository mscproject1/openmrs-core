/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.project1.web.controller;

import java.util.*;

import java.lang.String;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.module.project1.Untils.Utils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.Concept;
import org.openmrs.Obs;
import org.openmrs.Person;
import org.openmrs.User;
import org.openmrs.api.ConceptService;
import org.openmrs.api.ObsService;
import org.openmrs.api.PersonService;
import org.openmrs.api.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;

/**
 * This class configured as controller using annotation and mapped with the URL of
 * 'module/project1/project1Link.form'.
 */
@Controller("${rootrootArtifactId}.Project1Controller")
public class Project1Controller {
	
	/** Logger for this class and subclasses */
	protected final Log log = LogFactory.getLog(getClass());
	
	@Autowired
	UserService userService;
	
	@Autowired
	ObsService obsService;
	
	@Autowired
	ConceptService conceptService;
	
	@Autowired
	PersonService personService;
	
	@RequestMapping(value = "/project1/module/project1/project1.form")
	public Map<String, Double> onGet(@RequestBody ChartFiled chartFiled) {
		Concept concept= conceptService.getConcept(Utils.getIndexIdByName(chartFiled.getIndex()));
		List<Concept> ConceptList=new ArrayList<>();
		ConceptList.add(concept);
		List<Obs> ObsConceptList=obsService.getObservations(null,null,ConceptList,null,null,null,null,null,null,null,null,false);
		List<Person> PersonList=new ArrayList<>();
        List<Integer> PersonIdList=new ArrayList<>();
		for(Obs obs:ObsConceptList)
		{
			if(!PersonIdList.contains(obs.getPersonId())) {
				PersonIdList.add(obs.getPersonId());
				PersonList.add(personService.getPerson(obs.getPersonId()));
			}
		}
		List<Obs> ObsPersonList = obsService.getObservations(PersonList,null,null,null,null,null,null,null,null,null,null,false);
		Map<String,Double> result= new HashMap<>();
		Set<Integer> PeronIdSet =new HashSet<>();
		for(Obs obs:ObsPersonList)
		{
			if(obs.getValueDrug()!=null&&obs.getValueDrug().getDrugId()==Utils.GetMedIdByName(chartFiled.getMed()))
			{
              PeronIdSet.add(obs.getPersonId());
			}
		}
		Date date=null;
		int count=1,days=0;double SumIndex=0;

        for(Obs obs:ObsConceptList)
		{
			if(days>7)
			{
				break;
			}
         if(PeronIdSet.contains(obs.getPersonId()))
		 {
		 	System.out.println("--personId:"+obs.getPersonId()+"year:"+obs.getObsDatetime().getYear()+"Month:"+obs.getObsDatetime().getMonth()+"date:"+obs.getObsDatetime().getDate());
           if(date==null)
		   {
		   	date = obs.getObsDatetime();
		   	SumIndex=obs.getValueNumeric();
		   	days=days+1;
		   }
           else
           {
           if(date.getMonth()!=obs.getObsDatetime().getMonth()||date.getDate()!=obs.getObsDatetime().getDate())
		   {
		   	String s=((date.getMonth()+1)+"."+date.getDate());
		   	double d=SumIndex/count;
		   	result.put(s,d);
		   	date=obs.getObsDatetime();
		   	SumIndex=obs.getValueNumeric();
		   	count=1;
		   	days=days+1;
		   }
           else{
               SumIndex=SumIndex+obs.getValueNumeric();
               count=count+1;
		   }
		   }
		 }
		}
        result.put((date.getMonth()+1)+"."+date.getDate(),SumIndex/count);
		Set<Map.Entry<String,Double>> es=result.entrySet();
		for(Map.Entry<String,Double> e:es)
		{
			System.out.println("key:"+e.getKey()+"value:"+e.getValue());
		}

		return result;
	}
}
