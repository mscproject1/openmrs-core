package org.openmrs.module.project1.Untils;

public class Utils {
	
	public static int GetMedIdByName(String s) {
		if (s.equals("Dexamethasone"))
			return 12;
		else if (s.equals("Tocilizumab"))
			return 13;
		else if (s.equals("Ibuprofen"))
			return 14;
		
		return 15;
		
	}
	
	public static int getIndexIdByName(String s) {
		if (s.equals("Blood oxygen concentration"))
			return 5092;
		else
			return 790;
		
	}
	
}
