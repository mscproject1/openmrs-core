
<link rel="shortcut icon" type="image/png" href="images/fav.png">
<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<!-- themify icons CSS -->
<link rel="stylesheet" href="css/themify-icons.css">
<!-- Animations CSS -->
<link rel="stylesheet" href="css/animate.css">
<!-- Main CSS -->
<link rel="stylesheet" href="css/styles.css">
<link rel="stylesheet" href="css/green.css" id="style_theme">
<link rel="stylesheet" href="css/responsive.css">

<link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
<!-- 引入组件库 -->

<script src="js/modernizr.min.js"></script>

<style>
.colour1 {
    background: green;
}

.el-dropdown {
    vertical-align: top;


}
.el-dropdown + .el-dropdown {
    margin-left: 15px;
}
.el-icon-arrow-down {
    font-size: 12px;
}
</style>
</head>

<body>
<div id="app">
    <!-- Pre Loader -->
    <div class="loading">
        <div class="spinner">
            <div class="double-bounce1"></div>
            <div class="double-bounce2"></div>
        </div>
    </div>
    <!--/Pre Loader -->
    <!-- Color Changer -->
    <div class="theme-settings" id="switcher">
        <span class="theme-click">
            <span class="ti-settings"></span>
        </span>
        <span class="theme-color theme-default theme-active" data-color="green"></span>
        <span class="theme-color theme-blue" data-color="blue"></span>
        <span class="theme-color theme-red" data-color="red"></span>
        <span class="theme-color theme-violet" data-color="violet"></span>
        <span class="theme-color theme-yellow" data-color="yellow"></span>
    </div>
    <!-- /Color Changer -->
    <div class="wrapper">
        <!-- Page Content -->
        <div id="content">
            <!-- Top Navigation -->
            <div class="container top-brand">
                <nav class="navbar navbar-default">
                    <div class="navbar-header">
                        <div class="sidebar-header"> <img src="images/logo-dark.png" class="logo" alt="logo"></a>
                        </div>
                    </div>


                </nav>
            </div>
            <!-- /Top Navigation -->
            <!-- Menu -->
            <div class="container menu-nav">
                <nav class="navbar navbar-expand-lg lochana-bg text-white">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="ti-menu text-white"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">


                            <li class="nav-item dropdown active">
                                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
                                   aria-expanded="false"><span class="ti-layout-tab"></span> Function </a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="typography.html">Risk differentiation</a>
                                    <a class="dropdown-item" href="buttons1.html">Data analysis</a>

                                </div>
                            </li>
                            <!--
                        </ul>
                    </div>
                </nav>
            </div>


            <!-- Page Title -->
            <div class="container mt-0">
                <div class="row breadcrumb-bar">

                </div>
            </div>

            <!-- /BreadCumb -->
            <!-- Main Content -->
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="widget-area-2 lochana-box-shadow">
                            <!-- Item -->
                            <h3 class="widget-title">Data selection</h3>
                            <p class="margin-l20">Examining changes in recent indicators based on the selected drug.
                            </p>
                            <div class="lochana-widget">
                                <div>
                                    <el-radio-group v-model="radio">
                                        <el-radio label="Dexamethas">Dexamethas</el-radio>
                                        <el-radio label="Tocilizumab">Tocilizumab</el-radio>
                                        <el-radio label="Ibuprofen">Ibuprofen</el-radio>
                                        <el-radio label="Angiotensin">Angiotensin</el-radio>
                                    </el-radio-group>
                                </div>
                                <div>
                                    <el-radio-group v-model="radio1">
                                        <el-radio label="Blood oxygen concentration">Blood oxygen concentration</el-radio>
                                        <el-radio label="creatinine">creatinine</el-radio>
                                    </el-radio-group>
                                </div>
                                <button class="btn btn-primary" type="submit" @click="clickFunc">Button</button>

                            </div>
                            <!-- /Item -->
                        </div>
                        <div class="widget-area-2 lochana-box-shadow">
                            <!-- Item -->
                            <h3 class="widget-title">Data Display</h3>
                            <p class="margin-l20">
                                Clicking on the button above, you're going to see the effects of different drugs on different metrics.
                            </p>

                            <div id="main" v-show="visible" style="width: 600px;height:400px;"></div>
                            <!-- /Item -->
                        </div>

                        <!-- /Item -->

                    </div>
                </div>
            </div>



    <!-- /Page Content -->

%{--    <div id="main" style="width: 600px;height:400px;"></div>--}%
</div>
<!-- Back to Top -->
<a id="back-to-top" href="#" class="back-to-top">
    <span class="ti-angle-up"></span>
</a>
<!--</div>-->
</body>
<!-- Jquery Library-->


    <script src="/openmrs/moduleResources/project1/js/jquery-3.2.1.min.js"></script>
    <!-- Popper Library-->
    <script src="/openmrs/moduleResources/project1/js/popper.min.js"></script>
    <!-- Bootstrap Library-->
    <script src="/openmrs/moduleResources/project1/js/bootstrap.min.js"></script>
    <!-- Custom Script-->
    <script src="/openmrs/moduleResources/project1/js/custom.js"></script>

<!-- import Vue before Element -->
<script src="https://unpkg.com/vue/dist/vue.js"></script>
<!-- import JavaScript -->
<script src="https://unpkg.com/element-ui/lib/index.js"></script>
<script src="/openmrs/moduleResources/project1/js/axios.js"></script>
<script src="https://cdn.jsdelivr.net/npm/echarts@5.1.1/dist/echarts.min.js"></script>
<script>
    new Vue({
        el: '#app',
        data: function(){
            return {
                radio:'',
                radio1:'',
                visible:false,
                chartDom:'',
                myChart:'',

            };
        },
        mounted(){
            this.chartDom = document.getElementById('main');
            this.myChart = echarts.init(this.chartDom);
        },
        methods:{
            clickFunc(){
                const _self = this

                axios.post('module/project1/project1.form', {
                    med: this.radio,
                    index: this.radio1
                })
                    .then(function (response) {
                      var keyArr = []
                      var valArr = []
                      for (var key in response.data) {
                        keyArr.push(key)
                        valArr.push(response.data[key])
                      }
                      _self.visible = true;
                      var option= {
                          title:{
                              text:_self.radio,
                              x:'center',
                          },
                          xAxis: {
                              type: 'category',
                              data: keyArr
                          },
                          yAxis: {
                              type: 'value'
                          },
                          series: [{
                              data: valArr,
                              type: 'line'
                          }]
                      };
                      option && _self.myChart.setOption(option);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        }
    })
</script>
