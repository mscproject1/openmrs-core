**Set Environment**

- Sunyi He

- Shuochen Dai

- Yuxiao Fan

**Learn and Control**

- Zhongyan Zuo

**Module 1:**

- Front end: Sunyi He

- Back end: Zhongyan Zuo

- Test: Shuochen Dai

- Database: Yuxiao Fan

**Module 2:**
- Front end: Sunyi He

- Back end: Zhongyan Zuo

- Test: Shuochen Dai

- Algorithm: Shuochen Dai

- Database: Yuxiao Fan

**Database Design**

- Yuxiao Fan

**Cloud Server**

- Shuochen Dai
