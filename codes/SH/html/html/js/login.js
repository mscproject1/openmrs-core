var bonapp_signup = new Vue({
    el:'#root',
    data:{
         message: 'Hello Vue!',
         login:{
             email:'',
             password:'',

         },
         loginError:{
            emailText: 'please input your email',
            passwordText: 'please input your password',
            email:'',
            password:'',

        }

    },
    methods:{
        valid(type){
            var login = this.login;
            var loginError = this.loginError;
            switch(type) {
                case 1:
                     if (!login.email) {
                         loginError.email = loginError.emailText;
                     } else {
                         loginError.email = '';
                     }
                     break;
                case 2:
                    if (!login.password) {
                        loginError.password = loginError.passwordText;
                    } else {
                        loginError.password = '';
                    }
                    break;

                default:
                   break;
             }

        }
    }
});