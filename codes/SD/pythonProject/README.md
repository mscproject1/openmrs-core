# 1.项目结构

## dataAnalysis

包含了所有数据处理过程

### dataunderstanding

·获取数据的初始特征。


### datapreparation

·包含了所有数据处理的步骤。

### algorithm

·包含了有所使用到的分类算法。

·共两个算法：K特征选择和逻辑回归。

### evaluation

·评估算法处理后的结果
## meatadata

·原始数据

## results

·数据处理过程中以及最终处理结果数据

### effectiveFeatures

·具有影响因素的Features

### features

·处理过程中产生的数据

### finalResults

·最终数据