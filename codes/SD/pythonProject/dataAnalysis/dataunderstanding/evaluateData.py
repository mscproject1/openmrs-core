import pandas as pd
import numpy as np


"""
getInfo
"""
def getInfo(data, chenckprint):
    if chenckprint == True and data is not None:
        data.info()
    else:
        return data.info

"""
get Null data
"""
def getNull(data, checkprint):
    if checkprint is True and data is not None:
        print (data.isnull().sum().sort_values(ascending=False))
    else:
        return data.isnull().sum().sort_values(ascending=False)

"""
评估所有数据
"""
def evaluateNullInfoShape(data):

    #Check shape
    print("The shape of data:" + str(data.shape) + "\n")

    print(data.head(10))
    #Check info
    data.info()

    #Check Null
    print("\n" + data.isnull().sum().sort_values(ascending=False) + "\n")

if __name__ == '__main__':
    data = pd.read_csv('../../metadata/COVID19.csv')

    evaluateNullInfoShape(data)

    getInfo(data, True)
