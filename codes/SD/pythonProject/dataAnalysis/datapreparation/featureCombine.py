import numpy as np
import pandas as pd

"""
将outcome跟不同需要准备的特征值进行组合，用于生成需要进一步转化为训练数据原始数据
"""

"""
将age，基础病，性别与outcome进行组合
"""
def basicInformationOutcomeCombine(data):
    basicInformation = pd.DataFrame(data, columns=['age', 'sex', 'chronic_disease', 'outcome'])

    #basicInformation.dropna(axis=0, how='any', inplace=True)

    basicInformation.to_csv('../../results/features/basicInformation.csv', index=False)

"""
将症状与outcome进行组合
"""
def symptomsOutcomeCombine(data):
    symptomsoutcome = pd.DataFrame(data, columns=['symptoms', 'outcome'])

    symptomsoutcome.dropna(axis=0, how='any', inplace=True)

    symptomsoutcome.to_csv('../../results/features/symptomsOutcome.csv', index=False)

def allfeaturesOutcomeCombine(data):
    allFeatures = pd.DataFrame(data, columns=['age', 'sex', 'chronic_disease','symptoms', 'outcome'])

    allFeatures.to_csv('../../results/features/allFeatures.csv', index=False)

if __name__ == '__main__':
    data = pd.read_csv('../../results/uniformData_age.csv', encoding='utf-8', index_col=False)

    #symptomsOutcomeCombine(data)

    #basicInformationOutcomeCombine(data)

    allfeaturesOutcomeCombine(data)