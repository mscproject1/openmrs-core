import pandas as pd
import numpy as np

"""
将数据转化成onehot格式
"""
def onehotSymptoms(data_onehot):
    data_onehot.dropna(axis=0, how='any', inplace=True)

    #print(data_onehot.isnull().sum().sort_values(ascending=False))

    data_onehot_symptoms = pd.get_dummies(data_onehot['symptoms'])
    print(data_onehot_symptoms)

    #将death和dischare进行替换
    data_onehot['outcome'] = data_onehot['outcome'].replace('death', 0)
    data_onehot['outcome'] = data_onehot['outcome'].replace('discharge', 1)

    data_onehot_final = data_onehot_symptoms.join(data_onehot['outcome'])

    print(data_onehot_final.head())

    data_onehot_final.to_csv('../../results/features/symptomsOutcome_onehot.csv')

if __name__ == '__main__':
    #df = pd.read_csv('../../results/features/symptomsOutcomeMerge.csv')

    #去掉字符左侧空格
    """ df['symptoms'] = df['symptoms'].str.lstrip()
    
        #print(df.info)
    
        df.to_csv('../../results/features/symptomsOutcomeMerge.csv')
    
    df = pd.read_csv('../../results/features/symptomsOutcomeMerge.csv')
    """

    #onehotSymptoms(df)

    oneHotAllFeatures()
