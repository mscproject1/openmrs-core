import pandas as pd
import numpy as np

"""
Removal of non-essential data
"""
def remove_illegal_outcome(drop_outcome_null):
    clear_illeague = drop_outcome_null.drop(drop_outcome_null[drop_outcome_null['outcome'] == 'https://www.mspbs.gov.py/covid-19.php'].index)
    clear_illeague = clear_illeague.drop(clear_illeague[clear_illeague['outcome'] == 'Migrated'].index)
    clear_illeague = clear_illeague.drop(clear_illeague[clear_illeague['outcome'] == 'Migrated_Other'].index)
    clear_illeague = clear_illeague.drop(clear_illeague[clear_illeague['outcome'] == 'Under treatment'].index)
    clear_illeague = clear_illeague.drop(clear_illeague[clear_illeague['outcome'] == 'Receiving Treatment'].index)
    clear_illeague = clear_illeague.drop(clear_illeague[clear_illeague['outcome'] == 'not hospitalized'].index)
    #list_outcome_type = clear_illeague['outcome'].unique()
    return pd.DataFrame(clear_illeague)

"""
Remove null age data
"""
def clean_null_age():
    #age sex padded data
    data = pd.read_csv('../../results/formatData_age_sex.csv', index_col=False)

    #去除掉age为null的值
    data = data.dropna(subset=['age'])

    data.to_csv('../../results/features/Features_cleaned_age.csv', index=False)

if __name__ == '__main__':
    """null_data = pd.read_csv('../../results/COVID19_del_columns.csv', encoding='utf-8', index_col=False)

    print("Start cleaning the null and illegal data, the original shape: " + str(null_data.shape))

    """
    #去除outcome的空值
    """
    null_data = null_data[null_data['outcome'].notna()]

    legal_outcome = remove_illegal_outcome(null_data)

    print("Clean Finished, the new shape: " + str(legal_outcome.shape))

    print("Saving...")

    legal_outcome.to_csv('../../results/outcome_cleaned_data.csv')"""

    clean_null_age()