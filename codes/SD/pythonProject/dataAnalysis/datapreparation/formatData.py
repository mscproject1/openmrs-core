import numpy as np
import pandas as pd
import dataAnalysis.dataunderstanding.evaluateData as ea
import random
from random import choice,randint

"""
The content of the unified outcome can only contain DEATH and DISCHARGED
"""
def formatOutcome(data):
    DEATH_OUTCOME = ['critical condition, intubated as of 14.02.2020','death', 'severe','died','Death','dead',
                                'treated in an intensive care unit (14.02.2020)', 'Dead', 'Died', 'Deceased', 'Critical condition',
                                'severe illness', 'unstable', 'critical condition' ,'treated in an intensive care unit (14.02.2020)'
                     ]
    DISCHARGE_OUTCOME = ['Alive', 'discharge', 'discharged', 'Discharged from hospital', 'recovered', 'recovering at home 03.03.2020',
                         'released from quarantine', 'stable', 'stable condition', 'Stable', 'Recovered', 'Discharged']

    # 去除状况不确定的值
    data = data.drop(data[(data['outcome'] == "Hospitalized") | (data['outcome'] == "Hospitalised") | (data['outcome'] == "Symptoms only improved with cough. Currently hospitalized for follow-up.")].index)

    #逐行替换
    for indexs in data.index:
        if data.loc[indexs, 'outcome'] in DEATH_OUTCOME:
            data.loc[indexs, 'outcome'] = "death"
        elif data.loc[indexs, 'outcome'] in DISCHARGE_OUTCOME:
            data.loc[indexs, 'outcome'] = "discharge"


    data.to_csv('../../results/uniformData.csv', index=False)

"""
Randomly generating range ages into the age of the range to which they belong
"""
def formateAge():
    data_change_age = pd.read_csv('../../results/features/allFeatures.csv')

    for index, row in data_change_age.iterrows():
        if row['age'] == "19-77":
            data_change_age.loc[index, 'age'] = random.randint(19, 77)
        elif row['age'] == "15-88":
            data_change_age.loc[index, 'age'] = random.randint(15, 88)
        elif row['age'] == "20-29":
            data_change_age.loc[index, 'age'] = random.randint(20, 29)
        elif row['age'] == "20-57":
            data_change_age.loc[index, 'age'] = random.randint(20, 57)
        elif row['age'] == "22-80":
            data_change_age.loc[index, 'age'] = random.randint(22, 80)
        elif row['age'] == "38-68":
            data_change_age.loc[index, 'age'] = random.randint(38, 68)
        elif row['age'] == "40-49":
            data_change_age.loc[index, 'age'] = random.randint(40, 49)
        elif row['age'] == "70-79":
            data_change_age.loc[index, 'age'] = random.randint(19, 77)
        elif row['age'] == "90-99":
            data_change_age.loc[index, 'age'] = random.randint(90, 99)
        elif row['age'] == "80-":
            data_change_age.loc[index, 'age'] = random.randint(80,100)
        elif row['age'] == "21-72":
            data_change_age.loc[index, 'age'] = random.randint(21,72)

    data_change_age.to_csv('../../results/formatData_age.csv', index=False)

def formateSex():
    data = pd.read_csv('../../results/formatData_age.csv', index_col=False)

    #The gender ratio after clearing the empty outcome is 5:4(female : male)
    #data['sex'] = data['sex'].interpolate()
    data['sex'] = data['sex'].fillna(pd.Series(np.random.choice(['female','male'],
                                               p=[0.57, 0.43], size=len(data))))

    data.to_csv('../../results/formatData_age_sex.csv', index=False)

"""
Convert the column values of chronic and symptoms into multiple rows
"""
def convert():
    #要用excel将:，;替换成,
    data = pd.read_csv('../../results/features/Features_cleaned_age_1.csv', index_col=False)

    #先获取所有行的symptoms和chronic值并做成新DataFrame的第一行特征
    #First get the symptoms and chronic values of all rows and make them the first features of the new DataFrame
    data_transform_chronic_disease = data.drop(["chronic_disease"], axis=1).join(data["chronic_disease"].str.split(",", expand=True)
                                                                                 .stack().reset_index(level = 1,drop = True).rename("chronic_disease"))

    #将所有symptoms统一格式并去除掉左空格
    #Uniform formatting of all symptoms and removal of left spaces
    data_transform_chronic_disease['chronic_disease'] = data_transform_chronic_disease['chronic_disease'].str.lower()

    data_transform_chronic_disease['chronic_disease'] = data_transform_chronic_disease['chronic_disease'].str.lstrip()

    all_symptoms = data_transform_chronic_disease['chronic_disease'].unique()

    all_chronic_disease_df = pd.DataFrame()
    for i in range(1, len(all_symptoms)):
        all_chronic_disease_df.loc[0, i] = all_symptoms[i]

    all_chronic_disease_df.to_csv('all_chronic_disease_1.csv', index=False)

def fillNull():
    data = pd.read_csv('.csv', index_col=False)

    data = data.fillna(0)

    data.to_csv('Features_cleaned_age_1_fillnull.csv', index_label=False)

def mergeAlldata():
    meta_data = pd.read_csv('Features_cleaned_age_2_fillnull.csv')

    new_data = pd.read_csv('all_symptoms_fixed_2.csv')

    #读、并填充symptoms和chronic
    for index, row in meta_data.iterrows():

        if row['symptoms'] != '0':
            row_features = meta_data.loc[index, 'symptoms']
            row_features = row_features.split(',')
            for i in range(0, len(row_features)):
                feature = row_features[i].lstrip()
                if feature in new_data.columns:
                    new_data.loc[index, feature] = 1
        elif row['chronic_disease'] != '0':
            row_features = meta_data.loc[index, 'chronic_disease']
            row_features = row_features.split(',')
            for i in range(0, len(row_features)):
                feature = row_features[i].lstrip()
                if feature in new_data.columns:
                    new_data.loc[index, feature] = 1

    new_data = new_data.fillna(0)

    #转换性别和outcome
    new_data['outcome'] = new_data['outcome'].replace('death', 0)
    new_data['outcome'] = new_data['outcome'].replace('discharge', 1)

    new_data['sex'] = new_data['sex'].replace('female', 0)
    new_data['sex'] = new_data['sex'].replace('male', 1)

    new_data.to_csv('../../results/features/all_features_onehot_1.csv', index=False)


if __name__ == '__main__':
    #formateAge()

    #formateSex()

    #convert()
    #fillNull()
    mergeAlldata()