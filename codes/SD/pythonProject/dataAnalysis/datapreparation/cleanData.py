import numpy as np
import pandas as pd

data_covid = pd.read_csv('../../metadata/COVID19.csv')

data_covid_new = data_covid.drop(['city', 'province', 'country', 'latitude',	'longitude', 'geo_resolution',
                                  'lives_in_Wuhan', 'travel_history_dates', 'travel_history_location', 'reported_market_exposure',
                                  'additional_information', 'source', 'notes_for_discussion', 'location',
                                  'admin3', 'admin2', 'admin1', 'country_new', 'admin_id',
                                  'data_moderator_initials', 'travel_history_binary', 'date_onset_symptoms', 'date_admission_hospital',
                                  'date_confirmation', 'date_death_or_discharge', 'ID']
                                 , axis=1)

data_covid_new.to_csv('../../results/COVID19_del_columns.csv')