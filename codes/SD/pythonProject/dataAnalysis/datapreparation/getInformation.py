import pandas as pd
import numpy as np

def getSymptoms():
    data = pd.read_csv('Features_cleaned_age_2_fillnull.csv')

    symptoms = []

    for index, row in data.iterrows():
        if row['symptoms'] != 0:
            row_features = data.loc[index, 'symptoms']
            row_features = row_features.split(',')
            for i in range(0, len(row_features)):
                feature = row_features[i].lstrip()
                if feature not in symptoms:
                    symptoms.append(feature)

    return symptoms

def getChronic_disease():
    data = pd.read_csv('Features_cleaned_age_2_fillnull.csv')

    chronic_disease = []

    for index, row in data.iterrows():
        if row['chronic_disease'] != 0:
            row_features = data.loc[index, 'chronic_disease']
            row_features = row_features.split(',')
            for i in range(0, len(row_features)):
                feature = row_features[i].lstrip()
                if feature not in chronic_disease:
                    chronic_disease.append(feature)

    print(chronic_disease)
    #return symptoms



if __name__ == '__main__':
    getChronic_disease()