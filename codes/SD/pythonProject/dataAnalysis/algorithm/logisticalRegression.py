import pandas as pd
import numpy as np
from sklearn.feature_selection import SelectKBest, f_regression
from sklearn import linear_model, datasets
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import PolynomialFeatures
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix,recall_score, roc_auc_score,precision_recall_fscore_support
import seaborn as sns
import matplotlib.pyplot as plt
from dataAnalysis.algorithm import Kfeatures as k
from dataAnalysis.evaluation import evaluateWeights
from dataAnalysis.evaluation import drawPicture

def PolynomiaLogisticRegression(degree):
    return Pipeline([
        ('poly', PolynomialFeatures(degree=degree)),
        ('std_scale', StandardScaler()),
        ('log_reg', LogisticRegression())
    ])

def logisticRegression(df, c, i):
    data_sets = df.to_numpy()

    #df_features = data_sets[:, :80]

    df_features = k.getFeatures()

    df_outcomes = data_sets[:, -1]

    #Segmentation data
    X_train, X_test, Y_train, Y_test = train_test_split(df_features, df_outcomes, random_state=True, test_size=0.2)

    #Set logisticRegression parameters
    log_reg = LogisticRegression(C=c, penalty='l2', max_iter=i)

    #log_reg = PolynomiaLogisticRegression(degree=3)
    log_reg.fit(X_train, np.ravel(Y_train, order='C'))
    score = log_reg.score(X_test, Y_test)

    #calculate recall
    y_pred = log_reg.predict(X_train)
    recall_acc = recall_score(Y_train, y_pred)
    roc_LG = roc_auc_score(y_true=Y_train, y_score=y_pred)


    # evaluate weights
    #evaluateWeights.outputWeights(log_reg, df_features)

    #draw heatmap
    #drawPicture.drawHeatMap(Y_train, y_pred)

    return roc_LG

if __name__ == '__main__':
    df = pd.read_csv('all_features_onehot_2.csv', index_col=False)

    logisticRegression(df, 1)