from sklearn import svm
from sklearn.metrics import confusion_matrix,recall_score
from sklearn.model_selection import train_test_split
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from dataAnalysis.algorithm import Kfeatures as k
from dataAnalysis.evaluation import drawPicture
from dataAnalysis.evaluation import evalueateSVM
from sklearn.metrics import confusion_matrix,recall_score, roc_auc_score,precision_recall_fscore_support

def getFeatures():
    df = pd.read_csv('all_features_onehot_2.csv', index_col=False)
    features = k.selectFeatures(df)

    selected_features = pd.DataFrame()

    #print(df.reindex(columns=['fatigue']))
    for index, row in features.iterrows():
        feature_name = row['features']
        #selected_features = selected_features.join(df.reindex(columns=[feature_name]), axis=1)
        selected_features = pd.concat([selected_features, df.reindex(columns=[feature_name])], axis=1)

    return selected_features

def SVM(df, c, iter):
    data_sets = df.to_numpy()

    df_features = k.getFeatures()

    df_outcomes = data_sets[:, -1]

    # Segmentation data
    X_train, X_test, Y_train, Y_test = train_test_split(df_features, df_outcomes, random_state=True, test_size=0.4)

    #Train
    clf_linear = svm.SVC(C=c, kernel="linear", max_iter=iter)
    clf_poly = svm.SVC(C=c, kernel='poly', degree=0.3,max_iter=iter)
    clf_rbf = svm.SVC(C=c, kernel='rbf', gamma=1, max_iter=iter)
    clf_rbf2 = svm.SVC(C=c, kernel='rbf', gamma=0.1, max_iter=iter)

    clf_linear.fit(X_train, np.ravel(Y_train, order='C'))
    clf_poly.fit(X_train, np.ravel(Y_train, order='C'))
    clf_rbf.fit(X_train, np.ravel(Y_train, order='C'))
    clf_rbf2.fit(X_train, np.ravel(Y_train, order='C'))

    #Get Recall
    roc_linear = evalueateSVM.ROC_accuracy(clf_linear, X_train, Y_train)

    roc_poly = evalueateSVM.ROC_accuracy(clf_poly, X_train, Y_train)

    roc_rbf = evalueateSVM.ROC_accuracy(clf_rbf, X_train, Y_train)

    roc_rbf2 = evalueateSVM.ROC_accuracy(clf_rbf2, X_train, Y_train)

    #Draw Heatmap
    #drawPicture.drawHeatMap(Y_train, y_pred)

    return roc_linear, roc_poly, roc_rbf, roc_rbf2

if __name__ == '__main__':
    df = pd.read_csv("all_features_onehot_2.csv", index_col=False)

    SVM(df, 1, 10000)