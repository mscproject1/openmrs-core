import pandas as pd
import numpy as np
from sklearn.feature_selection import SelectKBest, f_regression
from sklearn import linear_model, datasets
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split

def getFeatures():
    df = pd.read_csv('all_features_onehot_2.csv', index_col=False)
    features = selectFeatures(df)

    selected_features = pd.DataFrame()

    #print(df.reindex(columns=['fatigue']))
    for index, row in features.iterrows():
        feature_name = row['features']
        #selected_features = selected_features.join(df.reindex(columns=[feature_name]), axis=1)
        selected_features = pd.concat([selected_features, df.reindex(columns=[feature_name])], axis=1)

    return selected_features

def selectFeatures(data):
    #分别获得特征和结果值
    #X轴
    df_features = data.drop(columns=['outcome'])

    #Y轴
    df_outcomes = data.loc[:, 'outcome':]

    #print('before transform:\n', df_features)

    #选取几个特征值
    sel = SelectKBest(score_func=f_regression,k=10)
    sel.fit(df_features, df_outcomes)

    #分别获取有效特征值，特征值分数以及所有特征值列
    scores = sel.pvalues_

    target_index = sel.get_support(True)

    columns = data.columns

    columns = columns.tolist()

    features_weights = {'features': [],
                        'scores': []}

    features_weights = pd.DataFrame(features_weights)

    #输出目标列

    for i in range(0 ,len(columns)-1):
        if i in target_index:
            #print(columns[i], '====', scores[i])
            # 添加数据到Dataframe
            new_features_weights = {'features': columns[i], 'scores': scores[i]}
            features_weights.loc[i + 1] = new_features_weights

    #print(features_weights)
    features_weights.to_csv('../../results/effectiveFeatures/Kfeatures_results', index_label=False)
    #df_final = df_features_selected.join(df_outcomes)
    #df_final.to_csv('../../results/features/selected_features.csv', index=False)

    return features_weights

if __name__ == '__main__':
    df = pd.read_csv('all_features_onehot_2.csv', index_col=False)

    #df = df.drop(axis=1, columns=['index'])

    selectFeatures(df)