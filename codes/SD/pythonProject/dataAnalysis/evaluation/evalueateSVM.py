import pandas as pd
import numpy as np
from sklearn.feature_selection import SelectKBest, f_regression
from sklearn import linear_model, datasets
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import PolynomialFeatures
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix,recall_score, roc_auc_score,precision_recall_fscore_support
import seaborn as sns
import matplotlib.pyplot as plt
from dataAnalysis.algorithm import SVM

def outputWeights(algorithm, df_features):
    weights = algorithm.coef_

    weights = weights.tolist()[0]

    columns = df_features.columns

    columns = columns.tolist()

    # 创建结果DataFrame
    features_weights = {'feature': [],
                        'target': []
                        }
    features_weights = pd.DataFrame(features_weights)

    for i in range(0, len(columns) - 1):
        # print(columns[i],'====',weights[i])
        # Add data to Dataframe
        new_features_weights = {'feature': columns[i], 'target': weights[i]}
        features_weights.loc[i + 1] = new_features_weights

    print(features_weights)

def ROC_accuracy(algorithm, X_train, Y_train):
    y_pred = algorithm.predict(X_train)
    recall_acc = recall_score(Y_train, y_pred)
    #print(str(algorithm) + str(recall_acc))
    #print(roc_auc_score(y_true=Y_train, y_score=y_pred))
    return str(roc_auc_score(y_true=Y_train, y_score=y_pred))

def findBestSVM():
    iters = [10, 100, 1000,5000, 10000, 15000]
    cs = [0.2, 0.5, 0.8, 1, 2]
    df = pd.read_csv("all_features_onehot_2.csv", index_col=False)
    linear = []
    poly = []
    rbf = []
    rbf2 = []

    for i in iters:
        for c in cs:
            roc_linear, roc_poly, roc_rbf, roc_rbf2 = SVM.SVM(df, c, i)
            linear.append(roc_linear)
            poly.append(roc_poly)
            rbf.append(roc_rbf)
            rbf2.append(roc_rbf2)

    print("Linear " + max(linear))
    print("Poly: " + max(poly))
    print("Rbf: " + max(rbf))
    print("Rbf2 " + max(rbf2))

if __name__ == '__main__':
    findBestSVM()