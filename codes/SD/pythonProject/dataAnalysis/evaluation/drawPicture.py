import pandas as pd
import numpy as np
from sklearn.feature_selection import SelectKBest, f_regression
from sklearn import linear_model, datasets
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import PolynomialFeatures
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix,recall_score, roc_auc_score,precision_recall_fscore_support
import seaborn as sns
import matplotlib.pyplot as plt

def drawHeatMap(Y_train, y_pred):
    # confusion metric
    sns.set()
    f, ax = plt.subplots()
    C2 = confusion_matrix(Y_train, y_pred, labels=[0, 1])
    sns.heatmap(C2, annot=True, ax=ax, cbar=True, fmt="d")  # draw heatmap

    ax.set_title('confusion matrix')  # titile
    ax.set_xlabel('test')  # x axis
    ax.set_ylabel('predict')  # y axis

    plt.show()