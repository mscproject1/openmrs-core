import pandas as pd
import numpy as np
from sklearn.feature_selection import SelectKBest, f_regression
from sklearn import linear_model, datasets
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import PolynomialFeatures
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix,recall_score, roc_auc_score,precision_recall_fscore_support
import seaborn as sns
import matplotlib.pyplot as plt

def outputWeights(algorithm, df_features):
    weights = algorithm.coef_

    weights = weights.tolist()[0]

    columns = df_features.columns

    columns = columns.tolist()

    # 创建结果DataFrame
    features_weights = {'feature': [],
                        'target': []
                        }
    features_weights = pd.DataFrame(features_weights)

    for i in range(0, len(columns) - 1):
        # print(columns[i],'====',weights[i])
        # 添加数据到Dataframe
        new_features_weights = {'feature': columns[i], 'target': weights[i]}
        features_weights.loc[i + 1] = new_features_weights

    print(features_weights)