import pandas as pd
import numpy as np
from sklearn.feature_selection import SelectKBest, f_regression
from sklearn import linear_model, datasets
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from dataAnalysis.algorithm.logisticalRegression import logisticRegression
import matplotlib.pyplot as plt

def findTheBest_random(df):
    score_best = 0
    x_axis = []
    y_axis = []
    #best：5000
    for i in range(1, 4000):
        x_axis.append(i)
        score_train = logisticalRegression.logisticRegression(df, i)
        y_axis.append(score_train)
        if score_train > score_best:
            result_i = i
            score_best = score_train

    print(result_i)
    print(score_best)

    line = plt.plot(x_axis, y_axis, 'r--', label='type1')
    plt.plot(x_axis, y_axis, 'ro--')
    plt.xlabel('Iteration numbers')
    plt.ylabel('Train best results')
    plt.legend()
    plt.show()

def findTheBestLog_C():
    iters = [10, 100, 1000,5000, 10000, 15000]
    cs = [0.2, 0.5, 0.8, 1, 2]
    df = pd.read_csv("all_features_onehot_2.csv", index_col=False)
    results = []

    for i in iters:
        for c in cs:
            result = logisticRegression(df, c, i)
            results.append(result)

    print(max(results))

if __name__ == '__main__':

    findTheBestLog_C()