import pandas as pd
import numpy as np

if __name__ == '__main__':
    df = pd.read_csv('../../results/effectiveFeatures/logisticalFeatures_results_1.csv', index_col=False)

    df.info()

    #将数据排序
    df_ascending = df.sort_values(ascending=False,by='target')

    #df_ascending = df_ascending['target'].to_dict()

    #去除空格
    #df_ascending['features']=df_ascending['features'].str.lstrip()

    df_ascending.to_csv('../../results/finalResults/features_final_1.csv', index=False)