import pandas as pd
import re


def get_num(s):
    pat = re.compile(r"\d{1,20}")
    res = pat.findall(s)
    if len(res) == 0:
        return ""
    else:
        return res[-1]

def process_1(f):
    data = pd.read_excel(f)
    #Delete the garbled code
    data["Address1"] = data["Address"].apply(lambda x:"".join([i for i in x if ord(i)<10000 and i != "/" and i != " "]))
    # Go to the last column

    s = data["Address1"].tolist()
    s1 = [i.split(",")[:-1] for i in s]
    s2 = [i.split(",")[-1] for i in s]

    s1 = [i+[""]*(10-len(i)) for i  in s1]
    data1 = pd.DataFrame(s1)
    
    data1["L"] = s2
    data["Last"] = data["Address1"].apply(lambda x:x.split(",")[-1])
    # Extracting numbers
    data["Num"] = data["Address1"].apply(get_num)
    return data1

def process_2(f):
    data = pd.read_excel(f)
    x,y = [],[]
    m = len(data.columns) - 1
    for row in data.iterrows():
        i = m
        while True:
            if isinstance(row[1][i], str):
                g = "".join([i for i in row[1][i] if ord(i)<10000 and i != "/" and i!=" "])
                x.append(g)
                y.append("")
                break
            i = i - 1

    data1 = data.values.tolist()

    data1 = [[j.strip() for j in i if isinstance(j,str)] for i in data1]
    data1 = [i[:-1] for i in data1]
    data1 = [i+[""]*(10-len(i)) for i in data1]
    data1 = pd.DataFrame(data1)
    data1['L'] = x
    return data1
data1 = process_1("./2.xls")
print(data1.columns)
data2 = process_1("./2.xls")
print(data2.columns)
data3 = process_1("./3.xls")
print(data3.columns)
data4 = process_1("./4.xls")
print(data4.columns)
data5 = process_1("./5.xls")
print(data5.columns)
data6 = process_2("./6.xls")
print(data6.columns)
data7 = process_1("./7.xls")
print(data7.columns)
data8 = process_2("./8.xls")
print(data8.columns)
a = pd.concat([data1, data2,data3,data4,data5,data6,data7,data8])
a.to_excel("./test.xls")
