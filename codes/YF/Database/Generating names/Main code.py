# coding: utf-8
import os
import string
import random
import time
import math
import unicodedata
import pandas as pd
import torch
import torch.nn as nn

all_type = [i.split(".")[0] for i in os.listdir("/content/African")]
n_type = len(all_type)

letters = string.ascii_letters + " .,;'-"
n_letters = len(letters) + 1

def unicode2ascii(s):
    return ''.join(
        c for c in unicodedata.normalize('NFD', s)
        if unicodedata.category(c) != 'Mn'
        and c in letters
    )

type_names = {}
for c in all_type:
    name = []
    for line in open(f"/content/African/{c}.csv", encoding="utf8").readlines():
        name.append(line.strip())
    type_names[c] = name


class RNN(nn.Module):
    def __init__(self, input_size, hidden_size, output_size):
        super(RNN, self).__init__()
        self.hidden_size = hidden_size
        self.i2h = nn.Linear(n_type + input_size + hidden_size, hidden_size)
        self.i2o = nn.Linear(n_type + input_size + hidden_size, output_size)
        self.o2o = nn.Linear(hidden_size + output_size, output_size)
        self.dropout = nn.Dropout(0.1)
        self.softmax = nn.LogSoftmax(dim=1)
    def forward(self, type, input, hidden):
        input_combined = torch.cat((type, input, hidden), 1)
        hidden = self.i2h(input_combined)
        output = self.i2o(input_combined)
        output_combined = torch.cat((hidden, output), 1)
        output = self.o2o(output_combined)
        output = self.dropout(output)
        output = self.softmax(output)
        return output, hidden

    def hidden_init(self):
        return torch.zeros(1, self.hidden_size)



def type_2_tensor(type):
    index = all_type.index(type)
    tensor = torch.zeros(1, n_type)
    tensor[0][index] = 1
    return tensor

def get_input_tensor(name):
    tensor = torch.zeros(len(name), 1, n_letters)
    for idx in range(len(name)):
        letter = name[idx]
        tensor[idx][0][letters.index(letter)] = 1
    return tensor

def get_target_tensor(name):
    letter_indexes = [letters.find(name[idx]) for idx in range(1, len(name))]
    letter_indexes.append(n_letters - 1)
    return torch.LongTensor(letter_indexes)

def get_train_data():
    c = random.sample(all_type, 1)[0]
    name = random.sample(type_names[c],1)[0]
    name = unicode2ascii(name)
    type_tensor = type_2_tensor(c)
    input_tensor = get_input_tensor(name)
    target_tensor = get_target_tensor(name)
    return type_tensor,input_tensor,target_tensor


def train(type_tensor, input_tensor, target_tensor):
    target_tensor.unsqueeze_(-1)
    hidden = model.hidden_init()
    model.zero_grad()
    loss = 0
    for i in range(input_tensor.size(0)):
        output, hidden = model(type_tensor, input_tensor[i], hidden)
        l = loss_func(output, target_tensor[i])
        loss += l
    loss.backward()
    optim.step()
    return output, loss.item() / input_tensor.size(0)


loss_func = nn.NLLLoss()
lr = 0.0005
model = RNN(n_letters, 256, n_letters)
optim = torch.optim.Adam(model.parameters(), lr=lr)
total_step = 100000
print_interval = 5000
total_loss = 0 
for _ in range(1, total_step + 1):
    type_tensor,input_tensor,target_tensor = get_train_data()
    output, loss = train(type_tensor,input_tensor,target_tensor)
    total_loss += loss
    if _ % print_interval == 0:
        print(f"step:{_}, {_ / total_step *100}%, loss:{round(loss, 4)}")


max_length = 1000
# Samples from categories and initials
def generate(type, start_letter='A'):
    with torch.no_grad():
        type_tensor = type_2_tensor(type)
        input = get_input_tensor(start_letter)
        hidden = model.hidden_init()
        output_name = start_letter
        for i in range(max_length):
            output, hidden = model(type_tensor, input[0], hidden)
            topv, topi = output.topk(1)
            topi = topi[0][0]
            if topi == n_letters - 1:
                break
            else:
                letter = letters[topi]
                output_name += letter
            input = get_input_tensor(letter)
        return output_name

generate('last_name', 'R')
def generates(category, start_letters, outname):
    First_ = []
    
    for start_letter in start_letters:
       print(generate(category, start_letter))
       for i in range(1,1731):
         if i == 1731:
          break
         else:
           First_.append(generate(category,start_letter ))
   
           i = i+1
       
    add1=pd.DataFrame(First_ , columns=[category])
    add1.to_csv(outname)
    return First_

generates("last_name","ABCDEFGHIJKLMNOPQRSTUVWXYZ", "1.csv")
generates("first_name","ABCDEFGHIJKLMNOPQRSTUVWXYZ", "2.csv")
