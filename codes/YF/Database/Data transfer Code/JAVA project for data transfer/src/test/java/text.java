import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @auther zhangyihao
 * @date 2021-05-20 13:47
 */
//@Component
public class text {

//    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

//    //每隔2秒执行一次
//    @Scheduled(fixedRate = 2000)
//    public void testTasks() {
//        System.out.println("11111111111111");
//    }

//    //每天3：05执行
//    @Scheduled(cron = "0 05 03 ? * *")
//    public void testTasks() {
//        System.out.println("定时任务执行时间：" + dateFormat.format(new Date()));
//    }

//    @Autowired
//    UserService us;
//
//    String key;
//    Person value;
//    @Scheduled(cron = "*/5 * * * * ?")
//    public void updatePerson(){
//        System.out.println(1);
//        Map<String,Person> map = new HashMap<String, Person>();
////        Person persons = us.selectPerson(i);
////        map.put("person",persons);
//        Iterator it=map.keySet().iterator();
//        while(it.hasNext()){
//           key=it.next().toString();
//           value=map.get(key);
//           if (value.getDeathDate() != null){
//               value.setDeathDate("2021-0"+Math.ceil(Math.random()*30)+"-"+Math.ceil(Math.random()*100)+" "+Math.ceil(Math.random()*24)+":"+Math.ceil(Math.random()*60));
//               us.updateperson(value);
//               System.out.println("改好!");
//           }else{
//               System.out.println("没改!");
//           }
//        }
//        System.out.println("11111111111111111111");
//    }
    /**
     * 输入经纬度返回地址
     * key lat(纬度),lng(经度)
     */
    @Async
    @Scheduled(cron = "*/5 * * * * ?")
    public void getposition() throws MalformedURLException {
//		String latitude,String longitude
        BufferedReader in = null;
//		URL tirc = new URL("http://api.map.baidu.com/geocoder?location="+ latitude+","+longitude+"&output=json&key="+"E4805d16520de693a3fe707cdc962045");
        URL tirc = new URL("http://api.map.baidu.com/geocoder?location="+ "-11.8394584"+","+"17.76084983"+"&output=json&key="+"E4805d16520de693a3fe707cdc962045");
        try {
            in = new BufferedReader(new InputStreamReader(tirc.openStream(),"UTF-8"));
            String res;
            StringBuilder sb = new StringBuilder("");
            while((res = in.readLine())!=null){
                sb.append(res.trim());
            }
            String str = sb.toString();
            //System.out.println(str);
            ObjectMapper mapper = new ObjectMapper();
            if(StringUtils.isNotEmpty(str)){
                JsonNode jsonNode = mapper.readTree(str);
                jsonNode.findValue("status").toString();
                JsonNode resultNode = jsonNode.findValue("result");
                JsonNode locationNode = resultNode.findValue("formatted_address");
                System.out.println(locationNode);
            }

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

}
