package com.home.springboot.mapper;

import com.home.springboot.entry.Admissions;
import com.home.springboot.entry.AdmissionsExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AdmissionsMapper {
    int countByExample(AdmissionsExample example);

    int deleteByExample(AdmissionsExample example);

    int deleteByPrimaryKey(String rowId);

    int insert(Admissions record);

    int insertSelective(Admissions record);

    List<Admissions> selectByExample(AdmissionsExample example);

    Admissions selectByPrimaryKey(String rowId);

    int updateByExampleSelective(@Param("record") Admissions record, @Param("example") AdmissionsExample example);

    int updateByExample(@Param("record") Admissions record, @Param("example") AdmissionsExample example);

    int updateByPrimaryKeySelective(Admissions record);

    int updateByPrimaryKey(Admissions record);

    Admissions selectAdmissions(Integer count);
}