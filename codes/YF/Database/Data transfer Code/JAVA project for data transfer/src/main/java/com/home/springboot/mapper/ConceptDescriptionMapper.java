package com.home.springboot.mapper;

import com.home.springboot.entry.ConceptDescription;
import com.home.springboot.entry.ConceptDescriptionExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ConceptDescriptionMapper {
    int countByExample(ConceptDescriptionExample example);

    int deleteByExample(ConceptDescriptionExample example);

    int insert(ConceptDescription record);

    int insertSelective(ConceptDescription record);

    List<ConceptDescription> selectByExample(ConceptDescriptionExample example);

    int updateByExampleSelective(@Param("record") ConceptDescription record, @Param("example") ConceptDescriptionExample example);

    int updateByExample(@Param("record") ConceptDescription record, @Param("example") ConceptDescriptionExample example);

    ConceptDescription selectConceptDescription(Integer count);

    int updateConceptDescription(@Param("description") String description, @Param("id") String id);
}