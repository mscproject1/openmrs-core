package com.home.springboot.entry;

public class Patient {
    private Integer patientId;

    private String creator;

    private String dateCreated;

    private String changedBy;

    private String dateChanged;

    private String voidedBy;

    private String dateVoided;

    private String voidReason;

    private String allergyStatus;

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated == null ? null : dateCreated.trim();
    }

    public String getChangedBy() {
        return changedBy;
    }

    public void setChangedBy(String changedBy) {
        this.changedBy = changedBy == null ? null : changedBy.trim();
    }

    public String getDateChanged() {
        return dateChanged;
    }

    public void setDateChanged(String dateChanged) {
        this.dateChanged = dateChanged == null ? null : dateChanged.trim();
    }

    public String getVoidedBy() {
        return voidedBy;
    }

    public void setVoidedBy(String voidedBy) {
        this.voidedBy = voidedBy == null ? null : voidedBy.trim();
    }

    public String getDateVoided() {
        return dateVoided;
    }

    public void setDateVoided(String dateVoided) {
        this.dateVoided = dateVoided == null ? null : dateVoided.trim();
    }

    public String getVoidReason() {
        return voidReason;
    }

    public void setVoidReason(String voidReason) {
        this.voidReason = voidReason == null ? null : voidReason.trim();
    }

    public String getAllergyStatus() {
        return allergyStatus;
    }

    public void setAllergyStatus(String allergyStatus) {
        this.allergyStatus = allergyStatus == null ? null : allergyStatus.trim();
    }
}