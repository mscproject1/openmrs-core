package com.home.springboot.mapper;

import com.home.springboot.entry.ConceptName;
import com.home.springboot.entry.ConceptNameExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ConceptNameMapper {
    int countByExample(ConceptNameExample example);

    int deleteByExample(ConceptNameExample example);

    int insert(ConceptName record);

    int insertSelective(ConceptName record);

    List<ConceptName> selectByExample(ConceptNameExample example);

    int updateByExampleSelective(@Param("record") ConceptName record, @Param("example") ConceptNameExample example);

    int updateByExample(@Param("record") ConceptName record, @Param("example") ConceptNameExample example);

    ConceptName selectConceptName(Integer count);

    int updateConceptName(@Param("name") String name, @Param("id") String id);
}
