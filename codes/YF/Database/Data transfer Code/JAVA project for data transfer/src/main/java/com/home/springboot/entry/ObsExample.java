package com.home.springboot.entry;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ObsExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ObsExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andObsIdIsNull() {
            addCriterion("obs_id is null");
            return (Criteria) this;
        }

        public Criteria andObsIdIsNotNull() {
            addCriterion("obs_id is not null");
            return (Criteria) this;
        }

        public Criteria andObsIdEqualTo(Integer value) {
            addCriterion("obs_id =", value, "obsId");
            return (Criteria) this;
        }

        public Criteria andObsIdNotEqualTo(Integer value) {
            addCriterion("obs_id <>", value, "obsId");
            return (Criteria) this;
        }

        public Criteria andObsIdGreaterThan(Integer value) {
            addCriterion("obs_id >", value, "obsId");
            return (Criteria) this;
        }

        public Criteria andObsIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("obs_id >=", value, "obsId");
            return (Criteria) this;
        }

        public Criteria andObsIdLessThan(Integer value) {
            addCriterion("obs_id <", value, "obsId");
            return (Criteria) this;
        }

        public Criteria andObsIdLessThanOrEqualTo(Integer value) {
            addCriterion("obs_id <=", value, "obsId");
            return (Criteria) this;
        }

        public Criteria andObsIdIn(List<Integer> values) {
            addCriterion("obs_id in", values, "obsId");
            return (Criteria) this;
        }

        public Criteria andObsIdNotIn(List<Integer> values) {
            addCriterion("obs_id not in", values, "obsId");
            return (Criteria) this;
        }

        public Criteria andObsIdBetween(Integer value1, Integer value2) {
            addCriterion("obs_id between", value1, value2, "obsId");
            return (Criteria) this;
        }

        public Criteria andObsIdNotBetween(Integer value1, Integer value2) {
            addCriterion("obs_id not between", value1, value2, "obsId");
            return (Criteria) this;
        }

        public Criteria andPersonIdIsNull() {
            addCriterion("person_id is null");
            return (Criteria) this;
        }

        public Criteria andPersonIdIsNotNull() {
            addCriterion("person_id is not null");
            return (Criteria) this;
        }

        public Criteria andPersonIdEqualTo(Integer value) {
            addCriterion("person_id =", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotEqualTo(Integer value) {
            addCriterion("person_id <>", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdGreaterThan(Integer value) {
            addCriterion("person_id >", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("person_id >=", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdLessThan(Integer value) {
            addCriterion("person_id <", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdLessThanOrEqualTo(Integer value) {
            addCriterion("person_id <=", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdIn(List<Integer> values) {
            addCriterion("person_id in", values, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotIn(List<Integer> values) {
            addCriterion("person_id not in", values, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdBetween(Integer value1, Integer value2) {
            addCriterion("person_id between", value1, value2, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotBetween(Integer value1, Integer value2) {
            addCriterion("person_id not between", value1, value2, "personId");
            return (Criteria) this;
        }

        public Criteria andConceptIdIsNull() {
            addCriterion("concept_id is null");
            return (Criteria) this;
        }

        public Criteria andConceptIdIsNotNull() {
            addCriterion("concept_id is not null");
            return (Criteria) this;
        }

        public Criteria andConceptIdEqualTo(Integer value) {
            addCriterion("concept_id =", value, "conceptId");
            return (Criteria) this;
        }

        public Criteria andConceptIdNotEqualTo(Integer value) {
            addCriterion("concept_id <>", value, "conceptId");
            return (Criteria) this;
        }

        public Criteria andConceptIdGreaterThan(Integer value) {
            addCriterion("concept_id >", value, "conceptId");
            return (Criteria) this;
        }

        public Criteria andConceptIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("concept_id >=", value, "conceptId");
            return (Criteria) this;
        }

        public Criteria andConceptIdLessThan(Integer value) {
            addCriterion("concept_id <", value, "conceptId");
            return (Criteria) this;
        }

        public Criteria andConceptIdLessThanOrEqualTo(Integer value) {
            addCriterion("concept_id <=", value, "conceptId");
            return (Criteria) this;
        }

        public Criteria andConceptIdIn(List<Integer> values) {
            addCriterion("concept_id in", values, "conceptId");
            return (Criteria) this;
        }

        public Criteria andConceptIdNotIn(List<Integer> values) {
            addCriterion("concept_id not in", values, "conceptId");
            return (Criteria) this;
        }

        public Criteria andConceptIdBetween(Integer value1, Integer value2) {
            addCriterion("concept_id between", value1, value2, "conceptId");
            return (Criteria) this;
        }

        public Criteria andConceptIdNotBetween(Integer value1, Integer value2) {
            addCriterion("concept_id not between", value1, value2, "conceptId");
            return (Criteria) this;
        }

        public Criteria andEncounterIdIsNull() {
            addCriterion("encounter_id is null");
            return (Criteria) this;
        }

        public Criteria andEncounterIdIsNotNull() {
            addCriterion("encounter_id is not null");
            return (Criteria) this;
        }

        public Criteria andEncounterIdEqualTo(Integer value) {
            addCriterion("encounter_id =", value, "encounterId");
            return (Criteria) this;
        }

        public Criteria andEncounterIdNotEqualTo(Integer value) {
            addCriterion("encounter_id <>", value, "encounterId");
            return (Criteria) this;
        }

        public Criteria andEncounterIdGreaterThan(Integer value) {
            addCriterion("encounter_id >", value, "encounterId");
            return (Criteria) this;
        }

        public Criteria andEncounterIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("encounter_id >=", value, "encounterId");
            return (Criteria) this;
        }

        public Criteria andEncounterIdLessThan(Integer value) {
            addCriterion("encounter_id <", value, "encounterId");
            return (Criteria) this;
        }

        public Criteria andEncounterIdLessThanOrEqualTo(Integer value) {
            addCriterion("encounter_id <=", value, "encounterId");
            return (Criteria) this;
        }

        public Criteria andEncounterIdIn(List<Integer> values) {
            addCriterion("encounter_id in", values, "encounterId");
            return (Criteria) this;
        }

        public Criteria andEncounterIdNotIn(List<Integer> values) {
            addCriterion("encounter_id not in", values, "encounterId");
            return (Criteria) this;
        }

        public Criteria andEncounterIdBetween(Integer value1, Integer value2) {
            addCriterion("encounter_id between", value1, value2, "encounterId");
            return (Criteria) this;
        }

        public Criteria andEncounterIdNotBetween(Integer value1, Integer value2) {
            addCriterion("encounter_id not between", value1, value2, "encounterId");
            return (Criteria) this;
        }

        public Criteria andOrderIdIsNull() {
            addCriterion("order_id is null");
            return (Criteria) this;
        }

        public Criteria andOrderIdIsNotNull() {
            addCriterion("order_id is not null");
            return (Criteria) this;
        }

        public Criteria andOrderIdEqualTo(Integer value) {
            addCriterion("order_id =", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotEqualTo(Integer value) {
            addCriterion("order_id <>", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdGreaterThan(Integer value) {
            addCriterion("order_id >", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("order_id >=", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdLessThan(Integer value) {
            addCriterion("order_id <", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdLessThanOrEqualTo(Integer value) {
            addCriterion("order_id <=", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdIn(List<Integer> values) {
            addCriterion("order_id in", values, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotIn(List<Integer> values) {
            addCriterion("order_id not in", values, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdBetween(Integer value1, Integer value2) {
            addCriterion("order_id between", value1, value2, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotBetween(Integer value1, Integer value2) {
            addCriterion("order_id not between", value1, value2, "orderId");
            return (Criteria) this;
        }

        public Criteria andObsDatetimeIsNull() {
            addCriterion("obs_datetime is null");
            return (Criteria) this;
        }

        public Criteria andObsDatetimeIsNotNull() {
            addCriterion("obs_datetime is not null");
            return (Criteria) this;
        }

        public Criteria andObsDatetimeEqualTo(Date value) {
            addCriterion("obs_datetime =", value, "obsDatetime");
            return (Criteria) this;
        }

        public Criteria andObsDatetimeNotEqualTo(Date value) {
            addCriterion("obs_datetime <>", value, "obsDatetime");
            return (Criteria) this;
        }

        public Criteria andObsDatetimeGreaterThan(Date value) {
            addCriterion("obs_datetime >", value, "obsDatetime");
            return (Criteria) this;
        }

        public Criteria andObsDatetimeGreaterThanOrEqualTo(Date value) {
            addCriterion("obs_datetime >=", value, "obsDatetime");
            return (Criteria) this;
        }

        public Criteria andObsDatetimeLessThan(Date value) {
            addCriterion("obs_datetime <", value, "obsDatetime");
            return (Criteria) this;
        }

        public Criteria andObsDatetimeLessThanOrEqualTo(Date value) {
            addCriterion("obs_datetime <=", value, "obsDatetime");
            return (Criteria) this;
        }

        public Criteria andObsDatetimeIn(List<Date> values) {
            addCriterion("obs_datetime in", values, "obsDatetime");
            return (Criteria) this;
        }

        public Criteria andObsDatetimeNotIn(List<Date> values) {
            addCriterion("obs_datetime not in", values, "obsDatetime");
            return (Criteria) this;
        }

        public Criteria andObsDatetimeBetween(Date value1, Date value2) {
            addCriterion("obs_datetime between", value1, value2, "obsDatetime");
            return (Criteria) this;
        }

        public Criteria andObsDatetimeNotBetween(Date value1, Date value2) {
            addCriterion("obs_datetime not between", value1, value2, "obsDatetime");
            return (Criteria) this;
        }

        public Criteria andLocationIdIsNull() {
            addCriterion("location_id is null");
            return (Criteria) this;
        }

        public Criteria andLocationIdIsNotNull() {
            addCriterion("location_id is not null");
            return (Criteria) this;
        }

        public Criteria andLocationIdEqualTo(Integer value) {
            addCriterion("location_id =", value, "locationId");
            return (Criteria) this;
        }

        public Criteria andLocationIdNotEqualTo(Integer value) {
            addCriterion("location_id <>", value, "locationId");
            return (Criteria) this;
        }

        public Criteria andLocationIdGreaterThan(Integer value) {
            addCriterion("location_id >", value, "locationId");
            return (Criteria) this;
        }

        public Criteria andLocationIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("location_id >=", value, "locationId");
            return (Criteria) this;
        }

        public Criteria andLocationIdLessThan(Integer value) {
            addCriterion("location_id <", value, "locationId");
            return (Criteria) this;
        }

        public Criteria andLocationIdLessThanOrEqualTo(Integer value) {
            addCriterion("location_id <=", value, "locationId");
            return (Criteria) this;
        }

        public Criteria andLocationIdIn(List<Integer> values) {
            addCriterion("location_id in", values, "locationId");
            return (Criteria) this;
        }

        public Criteria andLocationIdNotIn(List<Integer> values) {
            addCriterion("location_id not in", values, "locationId");
            return (Criteria) this;
        }

        public Criteria andLocationIdBetween(Integer value1, Integer value2) {
            addCriterion("location_id between", value1, value2, "locationId");
            return (Criteria) this;
        }

        public Criteria andLocationIdNotBetween(Integer value1, Integer value2) {
            addCriterion("location_id not between", value1, value2, "locationId");
            return (Criteria) this;
        }

        public Criteria andObsGroupIdIsNull() {
            addCriterion("obs_group_id is null");
            return (Criteria) this;
        }

        public Criteria andObsGroupIdIsNotNull() {
            addCriterion("obs_group_id is not null");
            return (Criteria) this;
        }

        public Criteria andObsGroupIdEqualTo(Integer value) {
            addCriterion("obs_group_id =", value, "obsGroupId");
            return (Criteria) this;
        }

        public Criteria andObsGroupIdNotEqualTo(Integer value) {
            addCriterion("obs_group_id <>", value, "obsGroupId");
            return (Criteria) this;
        }

        public Criteria andObsGroupIdGreaterThan(Integer value) {
            addCriterion("obs_group_id >", value, "obsGroupId");
            return (Criteria) this;
        }

        public Criteria andObsGroupIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("obs_group_id >=", value, "obsGroupId");
            return (Criteria) this;
        }

        public Criteria andObsGroupIdLessThan(Integer value) {
            addCriterion("obs_group_id <", value, "obsGroupId");
            return (Criteria) this;
        }

        public Criteria andObsGroupIdLessThanOrEqualTo(Integer value) {
            addCriterion("obs_group_id <=", value, "obsGroupId");
            return (Criteria) this;
        }

        public Criteria andObsGroupIdIn(List<Integer> values) {
            addCriterion("obs_group_id in", values, "obsGroupId");
            return (Criteria) this;
        }

        public Criteria andObsGroupIdNotIn(List<Integer> values) {
            addCriterion("obs_group_id not in", values, "obsGroupId");
            return (Criteria) this;
        }

        public Criteria andObsGroupIdBetween(Integer value1, Integer value2) {
            addCriterion("obs_group_id between", value1, value2, "obsGroupId");
            return (Criteria) this;
        }

        public Criteria andObsGroupIdNotBetween(Integer value1, Integer value2) {
            addCriterion("obs_group_id not between", value1, value2, "obsGroupId");
            return (Criteria) this;
        }

        public Criteria andAccessionNumberIsNull() {
            addCriterion("accession_number is null");
            return (Criteria) this;
        }

        public Criteria andAccessionNumberIsNotNull() {
            addCriterion("accession_number is not null");
            return (Criteria) this;
        }

        public Criteria andAccessionNumberEqualTo(String value) {
            addCriterion("accession_number =", value, "accessionNumber");
            return (Criteria) this;
        }

        public Criteria andAccessionNumberNotEqualTo(String value) {
            addCriterion("accession_number <>", value, "accessionNumber");
            return (Criteria) this;
        }

        public Criteria andAccessionNumberGreaterThan(String value) {
            addCriterion("accession_number >", value, "accessionNumber");
            return (Criteria) this;
        }

        public Criteria andAccessionNumberGreaterThanOrEqualTo(String value) {
            addCriterion("accession_number >=", value, "accessionNumber");
            return (Criteria) this;
        }

        public Criteria andAccessionNumberLessThan(String value) {
            addCriterion("accession_number <", value, "accessionNumber");
            return (Criteria) this;
        }

        public Criteria andAccessionNumberLessThanOrEqualTo(String value) {
            addCriterion("accession_number <=", value, "accessionNumber");
            return (Criteria) this;
        }

        public Criteria andAccessionNumberLike(String value) {
            addCriterion("accession_number like", value, "accessionNumber");
            return (Criteria) this;
        }

        public Criteria andAccessionNumberNotLike(String value) {
            addCriterion("accession_number not like", value, "accessionNumber");
            return (Criteria) this;
        }

        public Criteria andAccessionNumberIn(List<String> values) {
            addCriterion("accession_number in", values, "accessionNumber");
            return (Criteria) this;
        }

        public Criteria andAccessionNumberNotIn(List<String> values) {
            addCriterion("accession_number not in", values, "accessionNumber");
            return (Criteria) this;
        }

        public Criteria andAccessionNumberBetween(String value1, String value2) {
            addCriterion("accession_number between", value1, value2, "accessionNumber");
            return (Criteria) this;
        }

        public Criteria andAccessionNumberNotBetween(String value1, String value2) {
            addCriterion("accession_number not between", value1, value2, "accessionNumber");
            return (Criteria) this;
        }

        public Criteria andValueGroupIdIsNull() {
            addCriterion("value_group_id is null");
            return (Criteria) this;
        }

        public Criteria andValueGroupIdIsNotNull() {
            addCriterion("value_group_id is not null");
            return (Criteria) this;
        }

        public Criteria andValueGroupIdEqualTo(Integer value) {
            addCriterion("value_group_id =", value, "valueGroupId");
            return (Criteria) this;
        }

        public Criteria andValueGroupIdNotEqualTo(Integer value) {
            addCriterion("value_group_id <>", value, "valueGroupId");
            return (Criteria) this;
        }

        public Criteria andValueGroupIdGreaterThan(Integer value) {
            addCriterion("value_group_id >", value, "valueGroupId");
            return (Criteria) this;
        }

        public Criteria andValueGroupIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("value_group_id >=", value, "valueGroupId");
            return (Criteria) this;
        }

        public Criteria andValueGroupIdLessThan(Integer value) {
            addCriterion("value_group_id <", value, "valueGroupId");
            return (Criteria) this;
        }

        public Criteria andValueGroupIdLessThanOrEqualTo(Integer value) {
            addCriterion("value_group_id <=", value, "valueGroupId");
            return (Criteria) this;
        }

        public Criteria andValueGroupIdIn(List<Integer> values) {
            addCriterion("value_group_id in", values, "valueGroupId");
            return (Criteria) this;
        }

        public Criteria andValueGroupIdNotIn(List<Integer> values) {
            addCriterion("value_group_id not in", values, "valueGroupId");
            return (Criteria) this;
        }

        public Criteria andValueGroupIdBetween(Integer value1, Integer value2) {
            addCriterion("value_group_id between", value1, value2, "valueGroupId");
            return (Criteria) this;
        }

        public Criteria andValueGroupIdNotBetween(Integer value1, Integer value2) {
            addCriterion("value_group_id not between", value1, value2, "valueGroupId");
            return (Criteria) this;
        }

        public Criteria andValueCodedIsNull() {
            addCriterion("value_coded is null");
            return (Criteria) this;
        }

        public Criteria andValueCodedIsNotNull() {
            addCriterion("value_coded is not null");
            return (Criteria) this;
        }

        public Criteria andValueCodedEqualTo(Integer value) {
            addCriterion("value_coded =", value, "valueCoded");
            return (Criteria) this;
        }

        public Criteria andValueCodedNotEqualTo(Integer value) {
            addCriterion("value_coded <>", value, "valueCoded");
            return (Criteria) this;
        }

        public Criteria andValueCodedGreaterThan(Integer value) {
            addCriterion("value_coded >", value, "valueCoded");
            return (Criteria) this;
        }

        public Criteria andValueCodedGreaterThanOrEqualTo(Integer value) {
            addCriterion("value_coded >=", value, "valueCoded");
            return (Criteria) this;
        }

        public Criteria andValueCodedLessThan(Integer value) {
            addCriterion("value_coded <", value, "valueCoded");
            return (Criteria) this;
        }

        public Criteria andValueCodedLessThanOrEqualTo(Integer value) {
            addCriterion("value_coded <=", value, "valueCoded");
            return (Criteria) this;
        }

        public Criteria andValueCodedIn(List<Integer> values) {
            addCriterion("value_coded in", values, "valueCoded");
            return (Criteria) this;
        }

        public Criteria andValueCodedNotIn(List<Integer> values) {
            addCriterion("value_coded not in", values, "valueCoded");
            return (Criteria) this;
        }

        public Criteria andValueCodedBetween(Integer value1, Integer value2) {
            addCriterion("value_coded between", value1, value2, "valueCoded");
            return (Criteria) this;
        }

        public Criteria andValueCodedNotBetween(Integer value1, Integer value2) {
            addCriterion("value_coded not between", value1, value2, "valueCoded");
            return (Criteria) this;
        }

        public Criteria andValueCodedNameIdIsNull() {
            addCriterion("value_coded_name_id is null");
            return (Criteria) this;
        }

        public Criteria andValueCodedNameIdIsNotNull() {
            addCriterion("value_coded_name_id is not null");
            return (Criteria) this;
        }

        public Criteria andValueCodedNameIdEqualTo(Integer value) {
            addCriterion("value_coded_name_id =", value, "valueCodedNameId");
            return (Criteria) this;
        }

        public Criteria andValueCodedNameIdNotEqualTo(Integer value) {
            addCriterion("value_coded_name_id <>", value, "valueCodedNameId");
            return (Criteria) this;
        }

        public Criteria andValueCodedNameIdGreaterThan(Integer value) {
            addCriterion("value_coded_name_id >", value, "valueCodedNameId");
            return (Criteria) this;
        }

        public Criteria andValueCodedNameIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("value_coded_name_id >=", value, "valueCodedNameId");
            return (Criteria) this;
        }

        public Criteria andValueCodedNameIdLessThan(Integer value) {
            addCriterion("value_coded_name_id <", value, "valueCodedNameId");
            return (Criteria) this;
        }

        public Criteria andValueCodedNameIdLessThanOrEqualTo(Integer value) {
            addCriterion("value_coded_name_id <=", value, "valueCodedNameId");
            return (Criteria) this;
        }

        public Criteria andValueCodedNameIdIn(List<Integer> values) {
            addCriterion("value_coded_name_id in", values, "valueCodedNameId");
            return (Criteria) this;
        }

        public Criteria andValueCodedNameIdNotIn(List<Integer> values) {
            addCriterion("value_coded_name_id not in", values, "valueCodedNameId");
            return (Criteria) this;
        }

        public Criteria andValueCodedNameIdBetween(Integer value1, Integer value2) {
            addCriterion("value_coded_name_id between", value1, value2, "valueCodedNameId");
            return (Criteria) this;
        }

        public Criteria andValueCodedNameIdNotBetween(Integer value1, Integer value2) {
            addCriterion("value_coded_name_id not between", value1, value2, "valueCodedNameId");
            return (Criteria) this;
        }

        public Criteria andValueDrugIsNull() {
            addCriterion("value_drug is null");
            return (Criteria) this;
        }

        public Criteria andValueDrugIsNotNull() {
            addCriterion("value_drug is not null");
            return (Criteria) this;
        }

        public Criteria andValueDrugEqualTo(Integer value) {
            addCriterion("value_drug =", value, "valueDrug");
            return (Criteria) this;
        }

        public Criteria andValueDrugNotEqualTo(Integer value) {
            addCriterion("value_drug <>", value, "valueDrug");
            return (Criteria) this;
        }

        public Criteria andValueDrugGreaterThan(Integer value) {
            addCriterion("value_drug >", value, "valueDrug");
            return (Criteria) this;
        }

        public Criteria andValueDrugGreaterThanOrEqualTo(Integer value) {
            addCriterion("value_drug >=", value, "valueDrug");
            return (Criteria) this;
        }

        public Criteria andValueDrugLessThan(Integer value) {
            addCriterion("value_drug <", value, "valueDrug");
            return (Criteria) this;
        }

        public Criteria andValueDrugLessThanOrEqualTo(Integer value) {
            addCriterion("value_drug <=", value, "valueDrug");
            return (Criteria) this;
        }

        public Criteria andValueDrugIn(List<Integer> values) {
            addCriterion("value_drug in", values, "valueDrug");
            return (Criteria) this;
        }

        public Criteria andValueDrugNotIn(List<Integer> values) {
            addCriterion("value_drug not in", values, "valueDrug");
            return (Criteria) this;
        }

        public Criteria andValueDrugBetween(Integer value1, Integer value2) {
            addCriterion("value_drug between", value1, value2, "valueDrug");
            return (Criteria) this;
        }

        public Criteria andValueDrugNotBetween(Integer value1, Integer value2) {
            addCriterion("value_drug not between", value1, value2, "valueDrug");
            return (Criteria) this;
        }

        public Criteria andValueDatetimeIsNull() {
            addCriterion("value_datetime is null");
            return (Criteria) this;
        }

        public Criteria andValueDatetimeIsNotNull() {
            addCriterion("value_datetime is not null");
            return (Criteria) this;
        }

        public Criteria andValueDatetimeEqualTo(Date value) {
            addCriterion("value_datetime =", value, "valueDatetime");
            return (Criteria) this;
        }

        public Criteria andValueDatetimeNotEqualTo(Date value) {
            addCriterion("value_datetime <>", value, "valueDatetime");
            return (Criteria) this;
        }

        public Criteria andValueDatetimeGreaterThan(Date value) {
            addCriterion("value_datetime >", value, "valueDatetime");
            return (Criteria) this;
        }

        public Criteria andValueDatetimeGreaterThanOrEqualTo(Date value) {
            addCriterion("value_datetime >=", value, "valueDatetime");
            return (Criteria) this;
        }

        public Criteria andValueDatetimeLessThan(Date value) {
            addCriterion("value_datetime <", value, "valueDatetime");
            return (Criteria) this;
        }

        public Criteria andValueDatetimeLessThanOrEqualTo(Date value) {
            addCriterion("value_datetime <=", value, "valueDatetime");
            return (Criteria) this;
        }

        public Criteria andValueDatetimeIn(List<Date> values) {
            addCriterion("value_datetime in", values, "valueDatetime");
            return (Criteria) this;
        }

        public Criteria andValueDatetimeNotIn(List<Date> values) {
            addCriterion("value_datetime not in", values, "valueDatetime");
            return (Criteria) this;
        }

        public Criteria andValueDatetimeBetween(Date value1, Date value2) {
            addCriterion("value_datetime between", value1, value2, "valueDatetime");
            return (Criteria) this;
        }

        public Criteria andValueDatetimeNotBetween(Date value1, Date value2) {
            addCriterion("value_datetime not between", value1, value2, "valueDatetime");
            return (Criteria) this;
        }

        public Criteria andValueNumericIsNull() {
            addCriterion("value_numeric is null");
            return (Criteria) this;
        }

        public Criteria andValueNumericIsNotNull() {
            addCriterion("value_numeric is not null");
            return (Criteria) this;
        }

        public Criteria andValueNumericEqualTo(Double value) {
            addCriterion("value_numeric =", value, "valueNumeric");
            return (Criteria) this;
        }

        public Criteria andValueNumericNotEqualTo(Double value) {
            addCriterion("value_numeric <>", value, "valueNumeric");
            return (Criteria) this;
        }

        public Criteria andValueNumericGreaterThan(Double value) {
            addCriterion("value_numeric >", value, "valueNumeric");
            return (Criteria) this;
        }

        public Criteria andValueNumericGreaterThanOrEqualTo(Double value) {
            addCriterion("value_numeric >=", value, "valueNumeric");
            return (Criteria) this;
        }

        public Criteria andValueNumericLessThan(Double value) {
            addCriterion("value_numeric <", value, "valueNumeric");
            return (Criteria) this;
        }

        public Criteria andValueNumericLessThanOrEqualTo(Double value) {
            addCriterion("value_numeric <=", value, "valueNumeric");
            return (Criteria) this;
        }

        public Criteria andValueNumericIn(List<Double> values) {
            addCriterion("value_numeric in", values, "valueNumeric");
            return (Criteria) this;
        }

        public Criteria andValueNumericNotIn(List<Double> values) {
            addCriterion("value_numeric not in", values, "valueNumeric");
            return (Criteria) this;
        }

        public Criteria andValueNumericBetween(Double value1, Double value2) {
            addCriterion("value_numeric between", value1, value2, "valueNumeric");
            return (Criteria) this;
        }

        public Criteria andValueNumericNotBetween(Double value1, Double value2) {
            addCriterion("value_numeric not between", value1, value2, "valueNumeric");
            return (Criteria) this;
        }

        public Criteria andValueModifierIsNull() {
            addCriterion("value_modifier is null");
            return (Criteria) this;
        }

        public Criteria andValueModifierIsNotNull() {
            addCriterion("value_modifier is not null");
            return (Criteria) this;
        }

        public Criteria andValueModifierEqualTo(String value) {
            addCriterion("value_modifier =", value, "valueModifier");
            return (Criteria) this;
        }

        public Criteria andValueModifierNotEqualTo(String value) {
            addCriterion("value_modifier <>", value, "valueModifier");
            return (Criteria) this;
        }

        public Criteria andValueModifierGreaterThan(String value) {
            addCriterion("value_modifier >", value, "valueModifier");
            return (Criteria) this;
        }

        public Criteria andValueModifierGreaterThanOrEqualTo(String value) {
            addCriterion("value_modifier >=", value, "valueModifier");
            return (Criteria) this;
        }

        public Criteria andValueModifierLessThan(String value) {
            addCriterion("value_modifier <", value, "valueModifier");
            return (Criteria) this;
        }

        public Criteria andValueModifierLessThanOrEqualTo(String value) {
            addCriterion("value_modifier <=", value, "valueModifier");
            return (Criteria) this;
        }

        public Criteria andValueModifierLike(String value) {
            addCriterion("value_modifier like", value, "valueModifier");
            return (Criteria) this;
        }

        public Criteria andValueModifierNotLike(String value) {
            addCriterion("value_modifier not like", value, "valueModifier");
            return (Criteria) this;
        }

        public Criteria andValueModifierIn(List<String> values) {
            addCriterion("value_modifier in", values, "valueModifier");
            return (Criteria) this;
        }

        public Criteria andValueModifierNotIn(List<String> values) {
            addCriterion("value_modifier not in", values, "valueModifier");
            return (Criteria) this;
        }

        public Criteria andValueModifierBetween(String value1, String value2) {
            addCriterion("value_modifier between", value1, value2, "valueModifier");
            return (Criteria) this;
        }

        public Criteria andValueModifierNotBetween(String value1, String value2) {
            addCriterion("value_modifier not between", value1, value2, "valueModifier");
            return (Criteria) this;
        }

        public Criteria andValueComplexIsNull() {
            addCriterion("value_complex is null");
            return (Criteria) this;
        }

        public Criteria andValueComplexIsNotNull() {
            addCriterion("value_complex is not null");
            return (Criteria) this;
        }

        public Criteria andValueComplexEqualTo(String value) {
            addCriterion("value_complex =", value, "valueComplex");
            return (Criteria) this;
        }

        public Criteria andValueComplexNotEqualTo(String value) {
            addCriterion("value_complex <>", value, "valueComplex");
            return (Criteria) this;
        }

        public Criteria andValueComplexGreaterThan(String value) {
            addCriterion("value_complex >", value, "valueComplex");
            return (Criteria) this;
        }

        public Criteria andValueComplexGreaterThanOrEqualTo(String value) {
            addCriterion("value_complex >=", value, "valueComplex");
            return (Criteria) this;
        }

        public Criteria andValueComplexLessThan(String value) {
            addCriterion("value_complex <", value, "valueComplex");
            return (Criteria) this;
        }

        public Criteria andValueComplexLessThanOrEqualTo(String value) {
            addCriterion("value_complex <=", value, "valueComplex");
            return (Criteria) this;
        }

        public Criteria andValueComplexLike(String value) {
            addCriterion("value_complex like", value, "valueComplex");
            return (Criteria) this;
        }

        public Criteria andValueComplexNotLike(String value) {
            addCriterion("value_complex not like", value, "valueComplex");
            return (Criteria) this;
        }

        public Criteria andValueComplexIn(List<String> values) {
            addCriterion("value_complex in", values, "valueComplex");
            return (Criteria) this;
        }

        public Criteria andValueComplexNotIn(List<String> values) {
            addCriterion("value_complex not in", values, "valueComplex");
            return (Criteria) this;
        }

        public Criteria andValueComplexBetween(String value1, String value2) {
            addCriterion("value_complex between", value1, value2, "valueComplex");
            return (Criteria) this;
        }

        public Criteria andValueComplexNotBetween(String value1, String value2) {
            addCriterion("value_complex not between", value1, value2, "valueComplex");
            return (Criteria) this;
        }

        public Criteria andCommentsIsNull() {
            addCriterion("comments is null");
            return (Criteria) this;
        }

        public Criteria andCommentsIsNotNull() {
            addCriterion("comments is not null");
            return (Criteria) this;
        }

        public Criteria andCommentsEqualTo(String value) {
            addCriterion("comments =", value, "comments");
            return (Criteria) this;
        }

        public Criteria andCommentsNotEqualTo(String value) {
            addCriterion("comments <>", value, "comments");
            return (Criteria) this;
        }

        public Criteria andCommentsGreaterThan(String value) {
            addCriterion("comments >", value, "comments");
            return (Criteria) this;
        }

        public Criteria andCommentsGreaterThanOrEqualTo(String value) {
            addCriterion("comments >=", value, "comments");
            return (Criteria) this;
        }

        public Criteria andCommentsLessThan(String value) {
            addCriterion("comments <", value, "comments");
            return (Criteria) this;
        }

        public Criteria andCommentsLessThanOrEqualTo(String value) {
            addCriterion("comments <=", value, "comments");
            return (Criteria) this;
        }

        public Criteria andCommentsLike(String value) {
            addCriterion("comments like", value, "comments");
            return (Criteria) this;
        }

        public Criteria andCommentsNotLike(String value) {
            addCriterion("comments not like", value, "comments");
            return (Criteria) this;
        }

        public Criteria andCommentsIn(List<String> values) {
            addCriterion("comments in", values, "comments");
            return (Criteria) this;
        }

        public Criteria andCommentsNotIn(List<String> values) {
            addCriterion("comments not in", values, "comments");
            return (Criteria) this;
        }

        public Criteria andCommentsBetween(String value1, String value2) {
            addCriterion("comments between", value1, value2, "comments");
            return (Criteria) this;
        }

        public Criteria andCommentsNotBetween(String value1, String value2) {
            addCriterion("comments not between", value1, value2, "comments");
            return (Criteria) this;
        }

        public Criteria andCreatorIsNull() {
            addCriterion("creator is null");
            return (Criteria) this;
        }

        public Criteria andCreatorIsNotNull() {
            addCriterion("creator is not null");
            return (Criteria) this;
        }

        public Criteria andCreatorEqualTo(Integer value) {
            addCriterion("creator =", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotEqualTo(Integer value) {
            addCriterion("creator <>", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorGreaterThan(Integer value) {
            addCriterion("creator >", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorGreaterThanOrEqualTo(Integer value) {
            addCriterion("creator >=", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorLessThan(Integer value) {
            addCriterion("creator <", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorLessThanOrEqualTo(Integer value) {
            addCriterion("creator <=", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorIn(List<Integer> values) {
            addCriterion("creator in", values, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotIn(List<Integer> values) {
            addCriterion("creator not in", values, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorBetween(Integer value1, Integer value2) {
            addCriterion("creator between", value1, value2, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotBetween(Integer value1, Integer value2) {
            addCriterion("creator not between", value1, value2, "creator");
            return (Criteria) this;
        }

        public Criteria andDateCreatedIsNull() {
            addCriterion("date_created is null");
            return (Criteria) this;
        }

        public Criteria andDateCreatedIsNotNull() {
            addCriterion("date_created is not null");
            return (Criteria) this;
        }

        public Criteria andDateCreatedEqualTo(Date value) {
            addCriterion("date_created =", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedNotEqualTo(Date value) {
            addCriterion("date_created <>", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedGreaterThan(Date value) {
            addCriterion("date_created >", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedGreaterThanOrEqualTo(Date value) {
            addCriterion("date_created >=", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedLessThan(Date value) {
            addCriterion("date_created <", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedLessThanOrEqualTo(Date value) {
            addCriterion("date_created <=", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedIn(List<Date> values) {
            addCriterion("date_created in", values, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedNotIn(List<Date> values) {
            addCriterion("date_created not in", values, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedBetween(Date value1, Date value2) {
            addCriterion("date_created between", value1, value2, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedNotBetween(Date value1, Date value2) {
            addCriterion("date_created not between", value1, value2, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andVoidedIsNull() {
            addCriterion("voided is null");
            return (Criteria) this;
        }

        public Criteria andVoidedIsNotNull() {
            addCriterion("voided is not null");
            return (Criteria) this;
        }

        public Criteria andVoidedEqualTo(Boolean value) {
            addCriterion("voided =", value, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedNotEqualTo(Boolean value) {
            addCriterion("voided <>", value, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedGreaterThan(Boolean value) {
            addCriterion("voided >", value, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedGreaterThanOrEqualTo(Boolean value) {
            addCriterion("voided >=", value, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedLessThan(Boolean value) {
            addCriterion("voided <", value, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedLessThanOrEqualTo(Boolean value) {
            addCriterion("voided <=", value, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedIn(List<Boolean> values) {
            addCriterion("voided in", values, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedNotIn(List<Boolean> values) {
            addCriterion("voided not in", values, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedBetween(Boolean value1, Boolean value2) {
            addCriterion("voided between", value1, value2, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedNotBetween(Boolean value1, Boolean value2) {
            addCriterion("voided not between", value1, value2, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedByIsNull() {
            addCriterion("voided_by is null");
            return (Criteria) this;
        }

        public Criteria andVoidedByIsNotNull() {
            addCriterion("voided_by is not null");
            return (Criteria) this;
        }

        public Criteria andVoidedByEqualTo(Integer value) {
            addCriterion("voided_by =", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByNotEqualTo(Integer value) {
            addCriterion("voided_by <>", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByGreaterThan(Integer value) {
            addCriterion("voided_by >", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByGreaterThanOrEqualTo(Integer value) {
            addCriterion("voided_by >=", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByLessThan(Integer value) {
            addCriterion("voided_by <", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByLessThanOrEqualTo(Integer value) {
            addCriterion("voided_by <=", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByIn(List<Integer> values) {
            addCriterion("voided_by in", values, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByNotIn(List<Integer> values) {
            addCriterion("voided_by not in", values, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByBetween(Integer value1, Integer value2) {
            addCriterion("voided_by between", value1, value2, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByNotBetween(Integer value1, Integer value2) {
            addCriterion("voided_by not between", value1, value2, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andDateVoidedIsNull() {
            addCriterion("date_voided is null");
            return (Criteria) this;
        }

        public Criteria andDateVoidedIsNotNull() {
            addCriterion("date_voided is not null");
            return (Criteria) this;
        }

        public Criteria andDateVoidedEqualTo(Date value) {
            addCriterion("date_voided =", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedNotEqualTo(Date value) {
            addCriterion("date_voided <>", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedGreaterThan(Date value) {
            addCriterion("date_voided >", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedGreaterThanOrEqualTo(Date value) {
            addCriterion("date_voided >=", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedLessThan(Date value) {
            addCriterion("date_voided <", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedLessThanOrEqualTo(Date value) {
            addCriterion("date_voided <=", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedIn(List<Date> values) {
            addCriterion("date_voided in", values, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedNotIn(List<Date> values) {
            addCriterion("date_voided not in", values, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedBetween(Date value1, Date value2) {
            addCriterion("date_voided between", value1, value2, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedNotBetween(Date value1, Date value2) {
            addCriterion("date_voided not between", value1, value2, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andVoidReasonIsNull() {
            addCriterion("void_reason is null");
            return (Criteria) this;
        }

        public Criteria andVoidReasonIsNotNull() {
            addCriterion("void_reason is not null");
            return (Criteria) this;
        }

        public Criteria andVoidReasonEqualTo(String value) {
            addCriterion("void_reason =", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonNotEqualTo(String value) {
            addCriterion("void_reason <>", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonGreaterThan(String value) {
            addCriterion("void_reason >", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonGreaterThanOrEqualTo(String value) {
            addCriterion("void_reason >=", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonLessThan(String value) {
            addCriterion("void_reason <", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonLessThanOrEqualTo(String value) {
            addCriterion("void_reason <=", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonLike(String value) {
            addCriterion("void_reason like", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonNotLike(String value) {
            addCriterion("void_reason not like", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonIn(List<String> values) {
            addCriterion("void_reason in", values, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonNotIn(List<String> values) {
            addCriterion("void_reason not in", values, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonBetween(String value1, String value2) {
            addCriterion("void_reason between", value1, value2, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonNotBetween(String value1, String value2) {
            addCriterion("void_reason not between", value1, value2, "voidReason");
            return (Criteria) this;
        }

        public Criteria andUuidIsNull() {
            addCriterion("uuid is null");
            return (Criteria) this;
        }

        public Criteria andUuidIsNotNull() {
            addCriterion("uuid is not null");
            return (Criteria) this;
        }

        public Criteria andUuidEqualTo(String value) {
            addCriterion("uuid =", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotEqualTo(String value) {
            addCriterion("uuid <>", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidGreaterThan(String value) {
            addCriterion("uuid >", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidGreaterThanOrEqualTo(String value) {
            addCriterion("uuid >=", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLessThan(String value) {
            addCriterion("uuid <", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLessThanOrEqualTo(String value) {
            addCriterion("uuid <=", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLike(String value) {
            addCriterion("uuid like", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotLike(String value) {
            addCriterion("uuid not like", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidIn(List<String> values) {
            addCriterion("uuid in", values, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotIn(List<String> values) {
            addCriterion("uuid not in", values, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidBetween(String value1, String value2) {
            addCriterion("uuid between", value1, value2, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotBetween(String value1, String value2) {
            addCriterion("uuid not between", value1, value2, "uuid");
            return (Criteria) this;
        }

        public Criteria andPreviousVersionIsNull() {
            addCriterion("previous_version is null");
            return (Criteria) this;
        }

        public Criteria andPreviousVersionIsNotNull() {
            addCriterion("previous_version is not null");
            return (Criteria) this;
        }

        public Criteria andPreviousVersionEqualTo(Integer value) {
            addCriterion("previous_version =", value, "previousVersion");
            return (Criteria) this;
        }

        public Criteria andPreviousVersionNotEqualTo(Integer value) {
            addCriterion("previous_version <>", value, "previousVersion");
            return (Criteria) this;
        }

        public Criteria andPreviousVersionGreaterThan(Integer value) {
            addCriterion("previous_version >", value, "previousVersion");
            return (Criteria) this;
        }

        public Criteria andPreviousVersionGreaterThanOrEqualTo(Integer value) {
            addCriterion("previous_version >=", value, "previousVersion");
            return (Criteria) this;
        }

        public Criteria andPreviousVersionLessThan(Integer value) {
            addCriterion("previous_version <", value, "previousVersion");
            return (Criteria) this;
        }

        public Criteria andPreviousVersionLessThanOrEqualTo(Integer value) {
            addCriterion("previous_version <=", value, "previousVersion");
            return (Criteria) this;
        }

        public Criteria andPreviousVersionIn(List<Integer> values) {
            addCriterion("previous_version in", values, "previousVersion");
            return (Criteria) this;
        }

        public Criteria andPreviousVersionNotIn(List<Integer> values) {
            addCriterion("previous_version not in", values, "previousVersion");
            return (Criteria) this;
        }

        public Criteria andPreviousVersionBetween(Integer value1, Integer value2) {
            addCriterion("previous_version between", value1, value2, "previousVersion");
            return (Criteria) this;
        }

        public Criteria andPreviousVersionNotBetween(Integer value1, Integer value2) {
            addCriterion("previous_version not between", value1, value2, "previousVersion");
            return (Criteria) this;
        }

        public Criteria andFormNamespaceAndPathIsNull() {
            addCriterion("form_namespace_and_path is null");
            return (Criteria) this;
        }

        public Criteria andFormNamespaceAndPathIsNotNull() {
            addCriterion("form_namespace_and_path is not null");
            return (Criteria) this;
        }

        public Criteria andFormNamespaceAndPathEqualTo(String value) {
            addCriterion("form_namespace_and_path =", value, "formNamespaceAndPath");
            return (Criteria) this;
        }

        public Criteria andFormNamespaceAndPathNotEqualTo(String value) {
            addCriterion("form_namespace_and_path <>", value, "formNamespaceAndPath");
            return (Criteria) this;
        }

        public Criteria andFormNamespaceAndPathGreaterThan(String value) {
            addCriterion("form_namespace_and_path >", value, "formNamespaceAndPath");
            return (Criteria) this;
        }

        public Criteria andFormNamespaceAndPathGreaterThanOrEqualTo(String value) {
            addCriterion("form_namespace_and_path >=", value, "formNamespaceAndPath");
            return (Criteria) this;
        }

        public Criteria andFormNamespaceAndPathLessThan(String value) {
            addCriterion("form_namespace_and_path <", value, "formNamespaceAndPath");
            return (Criteria) this;
        }

        public Criteria andFormNamespaceAndPathLessThanOrEqualTo(String value) {
            addCriterion("form_namespace_and_path <=", value, "formNamespaceAndPath");
            return (Criteria) this;
        }

        public Criteria andFormNamespaceAndPathLike(String value) {
            addCriterion("form_namespace_and_path like", value, "formNamespaceAndPath");
            return (Criteria) this;
        }

        public Criteria andFormNamespaceAndPathNotLike(String value) {
            addCriterion("form_namespace_and_path not like", value, "formNamespaceAndPath");
            return (Criteria) this;
        }

        public Criteria andFormNamespaceAndPathIn(List<String> values) {
            addCriterion("form_namespace_and_path in", values, "formNamespaceAndPath");
            return (Criteria) this;
        }

        public Criteria andFormNamespaceAndPathNotIn(List<String> values) {
            addCriterion("form_namespace_and_path not in", values, "formNamespaceAndPath");
            return (Criteria) this;
        }

        public Criteria andFormNamespaceAndPathBetween(String value1, String value2) {
            addCriterion("form_namespace_and_path between", value1, value2, "formNamespaceAndPath");
            return (Criteria) this;
        }

        public Criteria andFormNamespaceAndPathNotBetween(String value1, String value2) {
            addCriterion("form_namespace_and_path not between", value1, value2, "formNamespaceAndPath");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(String value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(String value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(String value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(String value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(String value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(String value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLike(String value) {
            addCriterion("status like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotLike(String value) {
            addCriterion("status not like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<String> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<String> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(String value1, String value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(String value1, String value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andInterpretationIsNull() {
            addCriterion("interpretation is null");
            return (Criteria) this;
        }

        public Criteria andInterpretationIsNotNull() {
            addCriterion("interpretation is not null");
            return (Criteria) this;
        }

        public Criteria andInterpretationEqualTo(String value) {
            addCriterion("interpretation =", value, "interpretation");
            return (Criteria) this;
        }

        public Criteria andInterpretationNotEqualTo(String value) {
            addCriterion("interpretation <>", value, "interpretation");
            return (Criteria) this;
        }

        public Criteria andInterpretationGreaterThan(String value) {
            addCriterion("interpretation >", value, "interpretation");
            return (Criteria) this;
        }

        public Criteria andInterpretationGreaterThanOrEqualTo(String value) {
            addCriterion("interpretation >=", value, "interpretation");
            return (Criteria) this;
        }

        public Criteria andInterpretationLessThan(String value) {
            addCriterion("interpretation <", value, "interpretation");
            return (Criteria) this;
        }

        public Criteria andInterpretationLessThanOrEqualTo(String value) {
            addCriterion("interpretation <=", value, "interpretation");
            return (Criteria) this;
        }

        public Criteria andInterpretationLike(String value) {
            addCriterion("interpretation like", value, "interpretation");
            return (Criteria) this;
        }

        public Criteria andInterpretationNotLike(String value) {
            addCriterion("interpretation not like", value, "interpretation");
            return (Criteria) this;
        }

        public Criteria andInterpretationIn(List<String> values) {
            addCriterion("interpretation in", values, "interpretation");
            return (Criteria) this;
        }

        public Criteria andInterpretationNotIn(List<String> values) {
            addCriterion("interpretation not in", values, "interpretation");
            return (Criteria) this;
        }

        public Criteria andInterpretationBetween(String value1, String value2) {
            addCriterion("interpretation between", value1, value2, "interpretation");
            return (Criteria) this;
        }

        public Criteria andInterpretationNotBetween(String value1, String value2) {
            addCriterion("interpretation not between", value1, value2, "interpretation");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}