package com.home.springboot.entry;

import java.util.Date;

public class Obs {
    private Integer obsId;

    private Integer personId;

    private Integer conceptId;

    private Integer encounterId;

    private Integer orderId;

    private Date obsDatetime;

    private Integer locationId;

    private Integer obsGroupId;

    private String accessionNumber;

    private Integer valueGroupId;

    private Integer valueCoded;

    private Integer valueCodedNameId;

    private Integer valueDrug;

    private Date valueDatetime;

    private Double valueNumeric;

    private String valueModifier;

    private String valueComplex;

    private String comments;

    private Integer creator;

    private Date dateCreated;

    private Boolean voided;

    private Integer voidedBy;

    private Date dateVoided;

    private String voidReason;

    private String uuid;

    private Integer previousVersion;

    private String formNamespaceAndPath;

    private String status;

    private String interpretation;

    private String valueText;

    public Integer getObsId() {
        return obsId;
    }

    public void setObsId(Integer obsId) {
        this.obsId = obsId;
    }

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public Integer getConceptId() {
        return conceptId;
    }

    public void setConceptId(Integer conceptId) {
        this.conceptId = conceptId;
    }

    public Integer getEncounterId() {
        return encounterId;
    }

    public void setEncounterId(Integer encounterId) {
        this.encounterId = encounterId;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Date getObsDatetime() {
        return obsDatetime;
    }

    public void setObsDatetime(Date obsDatetime) {
        this.obsDatetime = obsDatetime;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public Integer getObsGroupId() {
        return obsGroupId;
    }

    public void setObsGroupId(Integer obsGroupId) {
        this.obsGroupId = obsGroupId;
    }

    public String getAccessionNumber() {
        return accessionNumber;
    }

    public void setAccessionNumber(String accessionNumber) {
        this.accessionNumber = accessionNumber == null ? null : accessionNumber.trim();
    }

    public Integer getValueGroupId() {
        return valueGroupId;
    }

    public void setValueGroupId(Integer valueGroupId) {
        this.valueGroupId = valueGroupId;
    }

    public Integer getValueCoded() {
        return valueCoded;
    }

    public void setValueCoded(Integer valueCoded) {
        this.valueCoded = valueCoded;
    }

    public Integer getValueCodedNameId() {
        return valueCodedNameId;
    }

    public void setValueCodedNameId(Integer valueCodedNameId) {
        this.valueCodedNameId = valueCodedNameId;
    }

    public Integer getValueDrug() {
        return valueDrug;
    }

    public void setValueDrug(Integer valueDrug) {
        this.valueDrug = valueDrug;
    }

    public Date getValueDatetime() {
        return valueDatetime;
    }

    public void setValueDatetime(Date valueDatetime) {
        this.valueDatetime = valueDatetime;
    }

    public Double getValueNumeric() {
        return valueNumeric;
    }

    public void setValueNumeric(Double valueNumeric) {
        this.valueNumeric = valueNumeric;
    }

    public String getValueModifier() {
        return valueModifier;
    }

    public void setValueModifier(String valueModifier) {
        this.valueModifier = valueModifier == null ? null : valueModifier.trim();
    }

    public String getValueComplex() {
        return valueComplex;
    }

    public void setValueComplex(String valueComplex) {
        this.valueComplex = valueComplex == null ? null : valueComplex.trim();
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments == null ? null : comments.trim();
    }

    public Integer getCreator() {
        return creator;
    }

    public void setCreator(Integer creator) {
        this.creator = creator;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Boolean getVoided() {
        return voided;
    }

    public void setVoided(Boolean voided) {
        this.voided = voided;
    }

    public Integer getVoidedBy() {
        return voidedBy;
    }

    public void setVoidedBy(Integer voidedBy) {
        this.voidedBy = voidedBy;
    }

    public Date getDateVoided() {
        return dateVoided;
    }

    public void setDateVoided(Date dateVoided) {
        this.dateVoided = dateVoided;
    }

    public String getVoidReason() {
        return voidReason;
    }

    public void setVoidReason(String voidReason) {
        this.voidReason = voidReason == null ? null : voidReason.trim();
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid == null ? null : uuid.trim();
    }

    public Integer getPreviousVersion() {
        return previousVersion;
    }

    public void setPreviousVersion(Integer previousVersion) {
        this.previousVersion = previousVersion;
    }

    public String getFormNamespaceAndPath() {
        return formNamespaceAndPath;
    }

    public void setFormNamespaceAndPath(String formNamespaceAndPath) {
        this.formNamespaceAndPath = formNamespaceAndPath == null ? null : formNamespaceAndPath.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getInterpretation() {
        return interpretation;
    }

    public void setInterpretation(String interpretation) {
        this.interpretation = interpretation == null ? null : interpretation.trim();
    }

    public String getValueText() {
        return valueText;
    }

    public void setValueText(String valueText) {
        this.valueText = valueText == null ? null : valueText.trim();
    }
}