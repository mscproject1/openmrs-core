package com.home.springboot.entry;

public class ConceptName {
    private String conceptNameId;

    private String conceptId;

    private String name;

    private String locale;

    private String localePreferred;

    private String creator;

    private String dateCreated;

    private String conceptNameType;

    private String voided;

    private String voidedBy;

    private String dateVoided;

    private String voidReason;

    private String uuid;

    private String dateChanged;

    private String changedBy;

    public String getConceptNameId() {
        return conceptNameId;
    }

    public void setConceptNameId(String conceptNameId) {
        this.conceptNameId = conceptNameId == null ? null : conceptNameId.trim();
    }

    public String getConceptId() {
        return conceptId;
    }

    public void setConceptId(String conceptId) {
        this.conceptId = conceptId == null ? null : conceptId.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale == null ? null : locale.trim();
    }

    public String getLocalePreferred() {
        return localePreferred;
    }

    public void setLocalePreferred(String localePreferred) {
        this.localePreferred = localePreferred == null ? null : localePreferred.trim();
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated == null ? null : dateCreated.trim();
    }

    public String getConceptNameType() {
        return conceptNameType;
    }

    public void setConceptNameType(String conceptNameType) {
        this.conceptNameType = conceptNameType == null ? null : conceptNameType.trim();
    }

    public String getVoided() {
        return voided;
    }

    public void setVoided(String voided) {
        this.voided = voided == null ? null : voided.trim();
    }

    public String getVoidedBy() {
        return voidedBy;
    }

    public void setVoidedBy(String voidedBy) {
        this.voidedBy = voidedBy == null ? null : voidedBy.trim();
    }

    public String getDateVoided() {
        return dateVoided;
    }

    public void setDateVoided(String dateVoided) {
        this.dateVoided = dateVoided == null ? null : dateVoided.trim();
    }

    public String getVoidReason() {
        return voidReason;
    }

    public void setVoidReason(String voidReason) {
        this.voidReason = voidReason == null ? null : voidReason.trim();
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid == null ? null : uuid.trim();
    }

    public String getDateChanged() {
        return dateChanged;
    }

    public void setDateChanged(String dateChanged) {
        this.dateChanged = dateChanged == null ? null : dateChanged.trim();
    }

    public String getChangedBy() {
        return changedBy;
    }

    public void setChangedBy(String changedBy) {
        this.changedBy = changedBy == null ? null : changedBy.trim();
    }
}