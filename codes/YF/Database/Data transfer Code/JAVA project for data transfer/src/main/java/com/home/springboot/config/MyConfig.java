package com.home.springboot.config;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MyConfig {
	@Bean
	public LocaleResolver localeResolver() {
		return new NativeLocaleResolver();
	}

	protected static class NativeLocaleResolver implements LocaleResolver {

		@Override
		public Locale resolveLocale(HttpServletRequest request) {
			String language = request.getParameter("language");
			Locale locale = Locale.getDefault();
			if (!StringUtils.isEmpty(language)) {
				String[] split = language.split("_");
				locale = new Locale(split[0], split[1]);
			}
			return locale;
		}

		@Override
		public void setLocale(HttpServletRequest request, HttpServletResponse response, Locale locale) {

		}
	}

	@Configuration
	public class MyConfig2 {
		private CorsConfiguration buildConfig() {
			CorsConfiguration corsConfiguration = new CorsConfiguration();
			corsConfiguration.addAllowedOrigin("*"); // Allow any domain name to be used
			corsConfiguration.addAllowedHeader("*"); // Allow any head
			corsConfiguration.addAllowedMethod("*"); // Allow any method (post, get, etc.)
			return corsConfiguration;
		}

		@Bean
		public CorsFilter corsFilter() {
			UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
			source.registerCorsConfiguration("/**", buildConfig()); // Configuring cross-domain settings for interfaces
			return new CorsFilter(source);
		}
	}

	@Bean // Registering components in containers
	public WebMvcConfigurer webMvcConfigurer() {
		WebMvcConfigurer configurer = new WebMvcConfigurer() {
			// Configure resource mapping paths
			@Override
			public void addResourceHandlers(ResourceHandlerRegistry registry) {
				/**
				 * Resource mapping paths addResourceHandler: Access mapping paths addResourceLocations: Absolute resource paths
				 */
				registry.addResourceHandler("/img/**").addResourceLocations("file:E:/java/study/JAVA/mgj/tp/");
			}

		};

		return configurer;
	} 
}
