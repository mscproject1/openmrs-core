package com.home.springboot.entry;

import java.util.ArrayList;
import java.util.List;

public class AdmissionsExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public AdmissionsExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andRowIdIsNull() {
            addCriterion("ROW_ID is null");
            return (Criteria) this;
        }

        public Criteria andRowIdIsNotNull() {
            addCriterion("ROW_ID is not null");
            return (Criteria) this;
        }

        public Criteria andRowIdEqualTo(String value) {
            addCriterion("ROW_ID =", value, "rowId");
            return (Criteria) this;
        }

        public Criteria andRowIdNotEqualTo(String value) {
            addCriterion("ROW_ID <>", value, "rowId");
            return (Criteria) this;
        }

        public Criteria andRowIdGreaterThan(String value) {
            addCriterion("ROW_ID >", value, "rowId");
            return (Criteria) this;
        }

        public Criteria andRowIdGreaterThanOrEqualTo(String value) {
            addCriterion("ROW_ID >=", value, "rowId");
            return (Criteria) this;
        }

        public Criteria andRowIdLessThan(String value) {
            addCriterion("ROW_ID <", value, "rowId");
            return (Criteria) this;
        }

        public Criteria andRowIdLessThanOrEqualTo(String value) {
            addCriterion("ROW_ID <=", value, "rowId");
            return (Criteria) this;
        }

        public Criteria andRowIdLike(String value) {
            addCriterion("ROW_ID like", value, "rowId");
            return (Criteria) this;
        }

        public Criteria andRowIdNotLike(String value) {
            addCriterion("ROW_ID not like", value, "rowId");
            return (Criteria) this;
        }

        public Criteria andRowIdIn(List<String> values) {
            addCriterion("ROW_ID in", values, "rowId");
            return (Criteria) this;
        }

        public Criteria andRowIdNotIn(List<String> values) {
            addCriterion("ROW_ID not in", values, "rowId");
            return (Criteria) this;
        }

        public Criteria andRowIdBetween(String value1, String value2) {
            addCriterion("ROW_ID between", value1, value2, "rowId");
            return (Criteria) this;
        }

        public Criteria andRowIdNotBetween(String value1, String value2) {
            addCriterion("ROW_ID not between", value1, value2, "rowId");
            return (Criteria) this;
        }

        public Criteria andSubjectIdIsNull() {
            addCriterion("SUBJECT_ID is null");
            return (Criteria) this;
        }

        public Criteria andSubjectIdIsNotNull() {
            addCriterion("SUBJECT_ID is not null");
            return (Criteria) this;
        }

        public Criteria andSubjectIdEqualTo(String value) {
            addCriterion("SUBJECT_ID =", value, "subjectId");
            return (Criteria) this;
        }

        public Criteria andSubjectIdNotEqualTo(String value) {
            addCriterion("SUBJECT_ID <>", value, "subjectId");
            return (Criteria) this;
        }

        public Criteria andSubjectIdGreaterThan(String value) {
            addCriterion("SUBJECT_ID >", value, "subjectId");
            return (Criteria) this;
        }

        public Criteria andSubjectIdGreaterThanOrEqualTo(String value) {
            addCriterion("SUBJECT_ID >=", value, "subjectId");
            return (Criteria) this;
        }

        public Criteria andSubjectIdLessThan(String value) {
            addCriterion("SUBJECT_ID <", value, "subjectId");
            return (Criteria) this;
        }

        public Criteria andSubjectIdLessThanOrEqualTo(String value) {
            addCriterion("SUBJECT_ID <=", value, "subjectId");
            return (Criteria) this;
        }

        public Criteria andSubjectIdLike(String value) {
            addCriterion("SUBJECT_ID like", value, "subjectId");
            return (Criteria) this;
        }

        public Criteria andSubjectIdNotLike(String value) {
            addCriterion("SUBJECT_ID not like", value, "subjectId");
            return (Criteria) this;
        }

        public Criteria andSubjectIdIn(List<String> values) {
            addCriterion("SUBJECT_ID in", values, "subjectId");
            return (Criteria) this;
        }

        public Criteria andSubjectIdNotIn(List<String> values) {
            addCriterion("SUBJECT_ID not in", values, "subjectId");
            return (Criteria) this;
        }

        public Criteria andSubjectIdBetween(String value1, String value2) {
            addCriterion("SUBJECT_ID between", value1, value2, "subjectId");
            return (Criteria) this;
        }

        public Criteria andSubjectIdNotBetween(String value1, String value2) {
            addCriterion("SUBJECT_ID not between", value1, value2, "subjectId");
            return (Criteria) this;
        }

        public Criteria andHadmIdIsNull() {
            addCriterion("HADM_ID is null");
            return (Criteria) this;
        }

        public Criteria andHadmIdIsNotNull() {
            addCriterion("HADM_ID is not null");
            return (Criteria) this;
        }

        public Criteria andHadmIdEqualTo(String value) {
            addCriterion("HADM_ID =", value, "hadmId");
            return (Criteria) this;
        }

        public Criteria andHadmIdNotEqualTo(String value) {
            addCriterion("HADM_ID <>", value, "hadmId");
            return (Criteria) this;
        }

        public Criteria andHadmIdGreaterThan(String value) {
            addCriterion("HADM_ID >", value, "hadmId");
            return (Criteria) this;
        }

        public Criteria andHadmIdGreaterThanOrEqualTo(String value) {
            addCriterion("HADM_ID >=", value, "hadmId");
            return (Criteria) this;
        }

        public Criteria andHadmIdLessThan(String value) {
            addCriterion("HADM_ID <", value, "hadmId");
            return (Criteria) this;
        }

        public Criteria andHadmIdLessThanOrEqualTo(String value) {
            addCriterion("HADM_ID <=", value, "hadmId");
            return (Criteria) this;
        }

        public Criteria andHadmIdLike(String value) {
            addCriterion("HADM_ID like", value, "hadmId");
            return (Criteria) this;
        }

        public Criteria andHadmIdNotLike(String value) {
            addCriterion("HADM_ID not like", value, "hadmId");
            return (Criteria) this;
        }

        public Criteria andHadmIdIn(List<String> values) {
            addCriterion("HADM_ID in", values, "hadmId");
            return (Criteria) this;
        }

        public Criteria andHadmIdNotIn(List<String> values) {
            addCriterion("HADM_ID not in", values, "hadmId");
            return (Criteria) this;
        }

        public Criteria andHadmIdBetween(String value1, String value2) {
            addCriterion("HADM_ID between", value1, value2, "hadmId");
            return (Criteria) this;
        }

        public Criteria andHadmIdNotBetween(String value1, String value2) {
            addCriterion("HADM_ID not between", value1, value2, "hadmId");
            return (Criteria) this;
        }

        public Criteria andAdmittimeIsNull() {
            addCriterion("ADMITTIME is null");
            return (Criteria) this;
        }

        public Criteria andAdmittimeIsNotNull() {
            addCriterion("ADMITTIME is not null");
            return (Criteria) this;
        }

        public Criteria andAdmittimeEqualTo(String value) {
            addCriterion("ADMITTIME =", value, "admittime");
            return (Criteria) this;
        }

        public Criteria andAdmittimeNotEqualTo(String value) {
            addCriterion("ADMITTIME <>", value, "admittime");
            return (Criteria) this;
        }

        public Criteria andAdmittimeGreaterThan(String value) {
            addCriterion("ADMITTIME >", value, "admittime");
            return (Criteria) this;
        }

        public Criteria andAdmittimeGreaterThanOrEqualTo(String value) {
            addCriterion("ADMITTIME >=", value, "admittime");
            return (Criteria) this;
        }

        public Criteria andAdmittimeLessThan(String value) {
            addCriterion("ADMITTIME <", value, "admittime");
            return (Criteria) this;
        }

        public Criteria andAdmittimeLessThanOrEqualTo(String value) {
            addCriterion("ADMITTIME <=", value, "admittime");
            return (Criteria) this;
        }

        public Criteria andAdmittimeLike(String value) {
            addCriterion("ADMITTIME like", value, "admittime");
            return (Criteria) this;
        }

        public Criteria andAdmittimeNotLike(String value) {
            addCriterion("ADMITTIME not like", value, "admittime");
            return (Criteria) this;
        }

        public Criteria andAdmittimeIn(List<String> values) {
            addCriterion("ADMITTIME in", values, "admittime");
            return (Criteria) this;
        }

        public Criteria andAdmittimeNotIn(List<String> values) {
            addCriterion("ADMITTIME not in", values, "admittime");
            return (Criteria) this;
        }

        public Criteria andAdmittimeBetween(String value1, String value2) {
            addCriterion("ADMITTIME between", value1, value2, "admittime");
            return (Criteria) this;
        }

        public Criteria andAdmittimeNotBetween(String value1, String value2) {
            addCriterion("ADMITTIME not between", value1, value2, "admittime");
            return (Criteria) this;
        }

        public Criteria andDischtimeIsNull() {
            addCriterion("DISCHTIME is null");
            return (Criteria) this;
        }

        public Criteria andDischtimeIsNotNull() {
            addCriterion("DISCHTIME is not null");
            return (Criteria) this;
        }

        public Criteria andDischtimeEqualTo(String value) {
            addCriterion("DISCHTIME =", value, "dischtime");
            return (Criteria) this;
        }

        public Criteria andDischtimeNotEqualTo(String value) {
            addCriterion("DISCHTIME <>", value, "dischtime");
            return (Criteria) this;
        }

        public Criteria andDischtimeGreaterThan(String value) {
            addCriterion("DISCHTIME >", value, "dischtime");
            return (Criteria) this;
        }

        public Criteria andDischtimeGreaterThanOrEqualTo(String value) {
            addCriterion("DISCHTIME >=", value, "dischtime");
            return (Criteria) this;
        }

        public Criteria andDischtimeLessThan(String value) {
            addCriterion("DISCHTIME <", value, "dischtime");
            return (Criteria) this;
        }

        public Criteria andDischtimeLessThanOrEqualTo(String value) {
            addCriterion("DISCHTIME <=", value, "dischtime");
            return (Criteria) this;
        }

        public Criteria andDischtimeLike(String value) {
            addCriterion("DISCHTIME like", value, "dischtime");
            return (Criteria) this;
        }

        public Criteria andDischtimeNotLike(String value) {
            addCriterion("DISCHTIME not like", value, "dischtime");
            return (Criteria) this;
        }

        public Criteria andDischtimeIn(List<String> values) {
            addCriterion("DISCHTIME in", values, "dischtime");
            return (Criteria) this;
        }

        public Criteria andDischtimeNotIn(List<String> values) {
            addCriterion("DISCHTIME not in", values, "dischtime");
            return (Criteria) this;
        }

        public Criteria andDischtimeBetween(String value1, String value2) {
            addCriterion("DISCHTIME between", value1, value2, "dischtime");
            return (Criteria) this;
        }

        public Criteria andDischtimeNotBetween(String value1, String value2) {
            addCriterion("DISCHTIME not between", value1, value2, "dischtime");
            return (Criteria) this;
        }

        public Criteria andDeathtimeIsNull() {
            addCriterion("DEATHTIME is null");
            return (Criteria) this;
        }

        public Criteria andDeathtimeIsNotNull() {
            addCriterion("DEATHTIME is not null");
            return (Criteria) this;
        }

        public Criteria andDeathtimeEqualTo(String value) {
            addCriterion("DEATHTIME =", value, "deathtime");
            return (Criteria) this;
        }

        public Criteria andDeathtimeNotEqualTo(String value) {
            addCriterion("DEATHTIME <>", value, "deathtime");
            return (Criteria) this;
        }

        public Criteria andDeathtimeGreaterThan(String value) {
            addCriterion("DEATHTIME >", value, "deathtime");
            return (Criteria) this;
        }

        public Criteria andDeathtimeGreaterThanOrEqualTo(String value) {
            addCriterion("DEATHTIME >=", value, "deathtime");
            return (Criteria) this;
        }

        public Criteria andDeathtimeLessThan(String value) {
            addCriterion("DEATHTIME <", value, "deathtime");
            return (Criteria) this;
        }

        public Criteria andDeathtimeLessThanOrEqualTo(String value) {
            addCriterion("DEATHTIME <=", value, "deathtime");
            return (Criteria) this;
        }

        public Criteria andDeathtimeLike(String value) {
            addCriterion("DEATHTIME like", value, "deathtime");
            return (Criteria) this;
        }

        public Criteria andDeathtimeNotLike(String value) {
            addCriterion("DEATHTIME not like", value, "deathtime");
            return (Criteria) this;
        }

        public Criteria andDeathtimeIn(List<String> values) {
            addCriterion("DEATHTIME in", values, "deathtime");
            return (Criteria) this;
        }

        public Criteria andDeathtimeNotIn(List<String> values) {
            addCriterion("DEATHTIME not in", values, "deathtime");
            return (Criteria) this;
        }

        public Criteria andDeathtimeBetween(String value1, String value2) {
            addCriterion("DEATHTIME between", value1, value2, "deathtime");
            return (Criteria) this;
        }

        public Criteria andDeathtimeNotBetween(String value1, String value2) {
            addCriterion("DEATHTIME not between", value1, value2, "deathtime");
            return (Criteria) this;
        }

        public Criteria andAdmissionTypeIsNull() {
            addCriterion("ADMISSION_TYPE is null");
            return (Criteria) this;
        }

        public Criteria andAdmissionTypeIsNotNull() {
            addCriterion("ADMISSION_TYPE is not null");
            return (Criteria) this;
        }

        public Criteria andAdmissionTypeEqualTo(String value) {
            addCriterion("ADMISSION_TYPE =", value, "admissionType");
            return (Criteria) this;
        }

        public Criteria andAdmissionTypeNotEqualTo(String value) {
            addCriterion("ADMISSION_TYPE <>", value, "admissionType");
            return (Criteria) this;
        }

        public Criteria andAdmissionTypeGreaterThan(String value) {
            addCriterion("ADMISSION_TYPE >", value, "admissionType");
            return (Criteria) this;
        }

        public Criteria andAdmissionTypeGreaterThanOrEqualTo(String value) {
            addCriterion("ADMISSION_TYPE >=", value, "admissionType");
            return (Criteria) this;
        }

        public Criteria andAdmissionTypeLessThan(String value) {
            addCriterion("ADMISSION_TYPE <", value, "admissionType");
            return (Criteria) this;
        }

        public Criteria andAdmissionTypeLessThanOrEqualTo(String value) {
            addCriterion("ADMISSION_TYPE <=", value, "admissionType");
            return (Criteria) this;
        }

        public Criteria andAdmissionTypeLike(String value) {
            addCriterion("ADMISSION_TYPE like", value, "admissionType");
            return (Criteria) this;
        }

        public Criteria andAdmissionTypeNotLike(String value) {
            addCriterion("ADMISSION_TYPE not like", value, "admissionType");
            return (Criteria) this;
        }

        public Criteria andAdmissionTypeIn(List<String> values) {
            addCriterion("ADMISSION_TYPE in", values, "admissionType");
            return (Criteria) this;
        }

        public Criteria andAdmissionTypeNotIn(List<String> values) {
            addCriterion("ADMISSION_TYPE not in", values, "admissionType");
            return (Criteria) this;
        }

        public Criteria andAdmissionTypeBetween(String value1, String value2) {
            addCriterion("ADMISSION_TYPE between", value1, value2, "admissionType");
            return (Criteria) this;
        }

        public Criteria andAdmissionTypeNotBetween(String value1, String value2) {
            addCriterion("ADMISSION_TYPE not between", value1, value2, "admissionType");
            return (Criteria) this;
        }

        public Criteria andAdmissionLocationIsNull() {
            addCriterion("ADMISSION_LOCATION is null");
            return (Criteria) this;
        }

        public Criteria andAdmissionLocationIsNotNull() {
            addCriterion("ADMISSION_LOCATION is not null");
            return (Criteria) this;
        }

        public Criteria andAdmissionLocationEqualTo(String value) {
            addCriterion("ADMISSION_LOCATION =", value, "admissionLocation");
            return (Criteria) this;
        }

        public Criteria andAdmissionLocationNotEqualTo(String value) {
            addCriterion("ADMISSION_LOCATION <>", value, "admissionLocation");
            return (Criteria) this;
        }

        public Criteria andAdmissionLocationGreaterThan(String value) {
            addCriterion("ADMISSION_LOCATION >", value, "admissionLocation");
            return (Criteria) this;
        }

        public Criteria andAdmissionLocationGreaterThanOrEqualTo(String value) {
            addCriterion("ADMISSION_LOCATION >=", value, "admissionLocation");
            return (Criteria) this;
        }

        public Criteria andAdmissionLocationLessThan(String value) {
            addCriterion("ADMISSION_LOCATION <", value, "admissionLocation");
            return (Criteria) this;
        }

        public Criteria andAdmissionLocationLessThanOrEqualTo(String value) {
            addCriterion("ADMISSION_LOCATION <=", value, "admissionLocation");
            return (Criteria) this;
        }

        public Criteria andAdmissionLocationLike(String value) {
            addCriterion("ADMISSION_LOCATION like", value, "admissionLocation");
            return (Criteria) this;
        }

        public Criteria andAdmissionLocationNotLike(String value) {
            addCriterion("ADMISSION_LOCATION not like", value, "admissionLocation");
            return (Criteria) this;
        }

        public Criteria andAdmissionLocationIn(List<String> values) {
            addCriterion("ADMISSION_LOCATION in", values, "admissionLocation");
            return (Criteria) this;
        }

        public Criteria andAdmissionLocationNotIn(List<String> values) {
            addCriterion("ADMISSION_LOCATION not in", values, "admissionLocation");
            return (Criteria) this;
        }

        public Criteria andAdmissionLocationBetween(String value1, String value2) {
            addCriterion("ADMISSION_LOCATION between", value1, value2, "admissionLocation");
            return (Criteria) this;
        }

        public Criteria andAdmissionLocationNotBetween(String value1, String value2) {
            addCriterion("ADMISSION_LOCATION not between", value1, value2, "admissionLocation");
            return (Criteria) this;
        }

        public Criteria andDischargeLocationIsNull() {
            addCriterion("DISCHARGE_LOCATION is null");
            return (Criteria) this;
        }

        public Criteria andDischargeLocationIsNotNull() {
            addCriterion("DISCHARGE_LOCATION is not null");
            return (Criteria) this;
        }

        public Criteria andDischargeLocationEqualTo(String value) {
            addCriterion("DISCHARGE_LOCATION =", value, "dischargeLocation");
            return (Criteria) this;
        }

        public Criteria andDischargeLocationNotEqualTo(String value) {
            addCriterion("DISCHARGE_LOCATION <>", value, "dischargeLocation");
            return (Criteria) this;
        }

        public Criteria andDischargeLocationGreaterThan(String value) {
            addCriterion("DISCHARGE_LOCATION >", value, "dischargeLocation");
            return (Criteria) this;
        }

        public Criteria andDischargeLocationGreaterThanOrEqualTo(String value) {
            addCriterion("DISCHARGE_LOCATION >=", value, "dischargeLocation");
            return (Criteria) this;
        }

        public Criteria andDischargeLocationLessThan(String value) {
            addCriterion("DISCHARGE_LOCATION <", value, "dischargeLocation");
            return (Criteria) this;
        }

        public Criteria andDischargeLocationLessThanOrEqualTo(String value) {
            addCriterion("DISCHARGE_LOCATION <=", value, "dischargeLocation");
            return (Criteria) this;
        }

        public Criteria andDischargeLocationLike(String value) {
            addCriterion("DISCHARGE_LOCATION like", value, "dischargeLocation");
            return (Criteria) this;
        }

        public Criteria andDischargeLocationNotLike(String value) {
            addCriterion("DISCHARGE_LOCATION not like", value, "dischargeLocation");
            return (Criteria) this;
        }

        public Criteria andDischargeLocationIn(List<String> values) {
            addCriterion("DISCHARGE_LOCATION in", values, "dischargeLocation");
            return (Criteria) this;
        }

        public Criteria andDischargeLocationNotIn(List<String> values) {
            addCriterion("DISCHARGE_LOCATION not in", values, "dischargeLocation");
            return (Criteria) this;
        }

        public Criteria andDischargeLocationBetween(String value1, String value2) {
            addCriterion("DISCHARGE_LOCATION between", value1, value2, "dischargeLocation");
            return (Criteria) this;
        }

        public Criteria andDischargeLocationNotBetween(String value1, String value2) {
            addCriterion("DISCHARGE_LOCATION not between", value1, value2, "dischargeLocation");
            return (Criteria) this;
        }

        public Criteria andInsuranceIsNull() {
            addCriterion("INSURANCE is null");
            return (Criteria) this;
        }

        public Criteria andInsuranceIsNotNull() {
            addCriterion("INSURANCE is not null");
            return (Criteria) this;
        }

        public Criteria andInsuranceEqualTo(String value) {
            addCriterion("INSURANCE =", value, "insurance");
            return (Criteria) this;
        }

        public Criteria andInsuranceNotEqualTo(String value) {
            addCriterion("INSURANCE <>", value, "insurance");
            return (Criteria) this;
        }

        public Criteria andInsuranceGreaterThan(String value) {
            addCriterion("INSURANCE >", value, "insurance");
            return (Criteria) this;
        }

        public Criteria andInsuranceGreaterThanOrEqualTo(String value) {
            addCriterion("INSURANCE >=", value, "insurance");
            return (Criteria) this;
        }

        public Criteria andInsuranceLessThan(String value) {
            addCriterion("INSURANCE <", value, "insurance");
            return (Criteria) this;
        }

        public Criteria andInsuranceLessThanOrEqualTo(String value) {
            addCriterion("INSURANCE <=", value, "insurance");
            return (Criteria) this;
        }

        public Criteria andInsuranceLike(String value) {
            addCriterion("INSURANCE like", value, "insurance");
            return (Criteria) this;
        }

        public Criteria andInsuranceNotLike(String value) {
            addCriterion("INSURANCE not like", value, "insurance");
            return (Criteria) this;
        }

        public Criteria andInsuranceIn(List<String> values) {
            addCriterion("INSURANCE in", values, "insurance");
            return (Criteria) this;
        }

        public Criteria andInsuranceNotIn(List<String> values) {
            addCriterion("INSURANCE not in", values, "insurance");
            return (Criteria) this;
        }

        public Criteria andInsuranceBetween(String value1, String value2) {
            addCriterion("INSURANCE between", value1, value2, "insurance");
            return (Criteria) this;
        }

        public Criteria andInsuranceNotBetween(String value1, String value2) {
            addCriterion("INSURANCE not between", value1, value2, "insurance");
            return (Criteria) this;
        }

        public Criteria andLanguageIsNull() {
            addCriterion("LANGUAGE is null");
            return (Criteria) this;
        }

        public Criteria andLanguageIsNotNull() {
            addCriterion("LANGUAGE is not null");
            return (Criteria) this;
        }

        public Criteria andLanguageEqualTo(String value) {
            addCriterion("LANGUAGE =", value, "language");
            return (Criteria) this;
        }

        public Criteria andLanguageNotEqualTo(String value) {
            addCriterion("LANGUAGE <>", value, "language");
            return (Criteria) this;
        }

        public Criteria andLanguageGreaterThan(String value) {
            addCriterion("LANGUAGE >", value, "language");
            return (Criteria) this;
        }

        public Criteria andLanguageGreaterThanOrEqualTo(String value) {
            addCriterion("LANGUAGE >=", value, "language");
            return (Criteria) this;
        }

        public Criteria andLanguageLessThan(String value) {
            addCriterion("LANGUAGE <", value, "language");
            return (Criteria) this;
        }

        public Criteria andLanguageLessThanOrEqualTo(String value) {
            addCriterion("LANGUAGE <=", value, "language");
            return (Criteria) this;
        }

        public Criteria andLanguageLike(String value) {
            addCriterion("LANGUAGE like", value, "language");
            return (Criteria) this;
        }

        public Criteria andLanguageNotLike(String value) {
            addCriterion("LANGUAGE not like", value, "language");
            return (Criteria) this;
        }

        public Criteria andLanguageIn(List<String> values) {
            addCriterion("LANGUAGE in", values, "language");
            return (Criteria) this;
        }

        public Criteria andLanguageNotIn(List<String> values) {
            addCriterion("LANGUAGE not in", values, "language");
            return (Criteria) this;
        }

        public Criteria andLanguageBetween(String value1, String value2) {
            addCriterion("LANGUAGE between", value1, value2, "language");
            return (Criteria) this;
        }

        public Criteria andLanguageNotBetween(String value1, String value2) {
            addCriterion("LANGUAGE not between", value1, value2, "language");
            return (Criteria) this;
        }

        public Criteria andReligionIsNull() {
            addCriterion("RELIGION is null");
            return (Criteria) this;
        }

        public Criteria andReligionIsNotNull() {
            addCriterion("RELIGION is not null");
            return (Criteria) this;
        }

        public Criteria andReligionEqualTo(String value) {
            addCriterion("RELIGION =", value, "religion");
            return (Criteria) this;
        }

        public Criteria andReligionNotEqualTo(String value) {
            addCriterion("RELIGION <>", value, "religion");
            return (Criteria) this;
        }

        public Criteria andReligionGreaterThan(String value) {
            addCriterion("RELIGION >", value, "religion");
            return (Criteria) this;
        }

        public Criteria andReligionGreaterThanOrEqualTo(String value) {
            addCriterion("RELIGION >=", value, "religion");
            return (Criteria) this;
        }

        public Criteria andReligionLessThan(String value) {
            addCriterion("RELIGION <", value, "religion");
            return (Criteria) this;
        }

        public Criteria andReligionLessThanOrEqualTo(String value) {
            addCriterion("RELIGION <=", value, "religion");
            return (Criteria) this;
        }

        public Criteria andReligionLike(String value) {
            addCriterion("RELIGION like", value, "religion");
            return (Criteria) this;
        }

        public Criteria andReligionNotLike(String value) {
            addCriterion("RELIGION not like", value, "religion");
            return (Criteria) this;
        }

        public Criteria andReligionIn(List<String> values) {
            addCriterion("RELIGION in", values, "religion");
            return (Criteria) this;
        }

        public Criteria andReligionNotIn(List<String> values) {
            addCriterion("RELIGION not in", values, "religion");
            return (Criteria) this;
        }

        public Criteria andReligionBetween(String value1, String value2) {
            addCriterion("RELIGION between", value1, value2, "religion");
            return (Criteria) this;
        }

        public Criteria andReligionNotBetween(String value1, String value2) {
            addCriterion("RELIGION not between", value1, value2, "religion");
            return (Criteria) this;
        }

        public Criteria andMaritalStatusIsNull() {
            addCriterion("MARITAL_STATUS is null");
            return (Criteria) this;
        }

        public Criteria andMaritalStatusIsNotNull() {
            addCriterion("MARITAL_STATUS is not null");
            return (Criteria) this;
        }

        public Criteria andMaritalStatusEqualTo(String value) {
            addCriterion("MARITAL_STATUS =", value, "maritalStatus");
            return (Criteria) this;
        }

        public Criteria andMaritalStatusNotEqualTo(String value) {
            addCriterion("MARITAL_STATUS <>", value, "maritalStatus");
            return (Criteria) this;
        }

        public Criteria andMaritalStatusGreaterThan(String value) {
            addCriterion("MARITAL_STATUS >", value, "maritalStatus");
            return (Criteria) this;
        }

        public Criteria andMaritalStatusGreaterThanOrEqualTo(String value) {
            addCriterion("MARITAL_STATUS >=", value, "maritalStatus");
            return (Criteria) this;
        }

        public Criteria andMaritalStatusLessThan(String value) {
            addCriterion("MARITAL_STATUS <", value, "maritalStatus");
            return (Criteria) this;
        }

        public Criteria andMaritalStatusLessThanOrEqualTo(String value) {
            addCriterion("MARITAL_STATUS <=", value, "maritalStatus");
            return (Criteria) this;
        }

        public Criteria andMaritalStatusLike(String value) {
            addCriterion("MARITAL_STATUS like", value, "maritalStatus");
            return (Criteria) this;
        }

        public Criteria andMaritalStatusNotLike(String value) {
            addCriterion("MARITAL_STATUS not like", value, "maritalStatus");
            return (Criteria) this;
        }

        public Criteria andMaritalStatusIn(List<String> values) {
            addCriterion("MARITAL_STATUS in", values, "maritalStatus");
            return (Criteria) this;
        }

        public Criteria andMaritalStatusNotIn(List<String> values) {
            addCriterion("MARITAL_STATUS not in", values, "maritalStatus");
            return (Criteria) this;
        }

        public Criteria andMaritalStatusBetween(String value1, String value2) {
            addCriterion("MARITAL_STATUS between", value1, value2, "maritalStatus");
            return (Criteria) this;
        }

        public Criteria andMaritalStatusNotBetween(String value1, String value2) {
            addCriterion("MARITAL_STATUS not between", value1, value2, "maritalStatus");
            return (Criteria) this;
        }

        public Criteria andEthnicityIsNull() {
            addCriterion("ETHNICITY is null");
            return (Criteria) this;
        }

        public Criteria andEthnicityIsNotNull() {
            addCriterion("ETHNICITY is not null");
            return (Criteria) this;
        }

        public Criteria andEthnicityEqualTo(String value) {
            addCriterion("ETHNICITY =", value, "ethnicity");
            return (Criteria) this;
        }

        public Criteria andEthnicityNotEqualTo(String value) {
            addCriterion("ETHNICITY <>", value, "ethnicity");
            return (Criteria) this;
        }

        public Criteria andEthnicityGreaterThan(String value) {
            addCriterion("ETHNICITY >", value, "ethnicity");
            return (Criteria) this;
        }

        public Criteria andEthnicityGreaterThanOrEqualTo(String value) {
            addCriterion("ETHNICITY >=", value, "ethnicity");
            return (Criteria) this;
        }

        public Criteria andEthnicityLessThan(String value) {
            addCriterion("ETHNICITY <", value, "ethnicity");
            return (Criteria) this;
        }

        public Criteria andEthnicityLessThanOrEqualTo(String value) {
            addCriterion("ETHNICITY <=", value, "ethnicity");
            return (Criteria) this;
        }

        public Criteria andEthnicityLike(String value) {
            addCriterion("ETHNICITY like", value, "ethnicity");
            return (Criteria) this;
        }

        public Criteria andEthnicityNotLike(String value) {
            addCriterion("ETHNICITY not like", value, "ethnicity");
            return (Criteria) this;
        }

        public Criteria andEthnicityIn(List<String> values) {
            addCriterion("ETHNICITY in", values, "ethnicity");
            return (Criteria) this;
        }

        public Criteria andEthnicityNotIn(List<String> values) {
            addCriterion("ETHNICITY not in", values, "ethnicity");
            return (Criteria) this;
        }

        public Criteria andEthnicityBetween(String value1, String value2) {
            addCriterion("ETHNICITY between", value1, value2, "ethnicity");
            return (Criteria) this;
        }

        public Criteria andEthnicityNotBetween(String value1, String value2) {
            addCriterion("ETHNICITY not between", value1, value2, "ethnicity");
            return (Criteria) this;
        }

        public Criteria andEdregtimeIsNull() {
            addCriterion("EDREGTIME is null");
            return (Criteria) this;
        }

        public Criteria andEdregtimeIsNotNull() {
            addCriterion("EDREGTIME is not null");
            return (Criteria) this;
        }

        public Criteria andEdregtimeEqualTo(String value) {
            addCriterion("EDREGTIME =", value, "edregtime");
            return (Criteria) this;
        }

        public Criteria andEdregtimeNotEqualTo(String value) {
            addCriterion("EDREGTIME <>", value, "edregtime");
            return (Criteria) this;
        }

        public Criteria andEdregtimeGreaterThan(String value) {
            addCriterion("EDREGTIME >", value, "edregtime");
            return (Criteria) this;
        }

        public Criteria andEdregtimeGreaterThanOrEqualTo(String value) {
            addCriterion("EDREGTIME >=", value, "edregtime");
            return (Criteria) this;
        }

        public Criteria andEdregtimeLessThan(String value) {
            addCriterion("EDREGTIME <", value, "edregtime");
            return (Criteria) this;
        }

        public Criteria andEdregtimeLessThanOrEqualTo(String value) {
            addCriterion("EDREGTIME <=", value, "edregtime");
            return (Criteria) this;
        }

        public Criteria andEdregtimeLike(String value) {
            addCriterion("EDREGTIME like", value, "edregtime");
            return (Criteria) this;
        }

        public Criteria andEdregtimeNotLike(String value) {
            addCriterion("EDREGTIME not like", value, "edregtime");
            return (Criteria) this;
        }

        public Criteria andEdregtimeIn(List<String> values) {
            addCriterion("EDREGTIME in", values, "edregtime");
            return (Criteria) this;
        }

        public Criteria andEdregtimeNotIn(List<String> values) {
            addCriterion("EDREGTIME not in", values, "edregtime");
            return (Criteria) this;
        }

        public Criteria andEdregtimeBetween(String value1, String value2) {
            addCriterion("EDREGTIME between", value1, value2, "edregtime");
            return (Criteria) this;
        }

        public Criteria andEdregtimeNotBetween(String value1, String value2) {
            addCriterion("EDREGTIME not between", value1, value2, "edregtime");
            return (Criteria) this;
        }

        public Criteria andEdouttimeIsNull() {
            addCriterion("EDOUTTIME is null");
            return (Criteria) this;
        }

        public Criteria andEdouttimeIsNotNull() {
            addCriterion("EDOUTTIME is not null");
            return (Criteria) this;
        }

        public Criteria andEdouttimeEqualTo(String value) {
            addCriterion("EDOUTTIME =", value, "edouttime");
            return (Criteria) this;
        }

        public Criteria andEdouttimeNotEqualTo(String value) {
            addCriterion("EDOUTTIME <>", value, "edouttime");
            return (Criteria) this;
        }

        public Criteria andEdouttimeGreaterThan(String value) {
            addCriterion("EDOUTTIME >", value, "edouttime");
            return (Criteria) this;
        }

        public Criteria andEdouttimeGreaterThanOrEqualTo(String value) {
            addCriterion("EDOUTTIME >=", value, "edouttime");
            return (Criteria) this;
        }

        public Criteria andEdouttimeLessThan(String value) {
            addCriterion("EDOUTTIME <", value, "edouttime");
            return (Criteria) this;
        }

        public Criteria andEdouttimeLessThanOrEqualTo(String value) {
            addCriterion("EDOUTTIME <=", value, "edouttime");
            return (Criteria) this;
        }

        public Criteria andEdouttimeLike(String value) {
            addCriterion("EDOUTTIME like", value, "edouttime");
            return (Criteria) this;
        }

        public Criteria andEdouttimeNotLike(String value) {
            addCriterion("EDOUTTIME not like", value, "edouttime");
            return (Criteria) this;
        }

        public Criteria andEdouttimeIn(List<String> values) {
            addCriterion("EDOUTTIME in", values, "edouttime");
            return (Criteria) this;
        }

        public Criteria andEdouttimeNotIn(List<String> values) {
            addCriterion("EDOUTTIME not in", values, "edouttime");
            return (Criteria) this;
        }

        public Criteria andEdouttimeBetween(String value1, String value2) {
            addCriterion("EDOUTTIME between", value1, value2, "edouttime");
            return (Criteria) this;
        }

        public Criteria andEdouttimeNotBetween(String value1, String value2) {
            addCriterion("EDOUTTIME not between", value1, value2, "edouttime");
            return (Criteria) this;
        }

        public Criteria andDiagnosisIsNull() {
            addCriterion("DIAGNOSIS is null");
            return (Criteria) this;
        }

        public Criteria andDiagnosisIsNotNull() {
            addCriterion("DIAGNOSIS is not null");
            return (Criteria) this;
        }

        public Criteria andDiagnosisEqualTo(String value) {
            addCriterion("DIAGNOSIS =", value, "diagnosis");
            return (Criteria) this;
        }

        public Criteria andDiagnosisNotEqualTo(String value) {
            addCriterion("DIAGNOSIS <>", value, "diagnosis");
            return (Criteria) this;
        }

        public Criteria andDiagnosisGreaterThan(String value) {
            addCriterion("DIAGNOSIS >", value, "diagnosis");
            return (Criteria) this;
        }

        public Criteria andDiagnosisGreaterThanOrEqualTo(String value) {
            addCriterion("DIAGNOSIS >=", value, "diagnosis");
            return (Criteria) this;
        }

        public Criteria andDiagnosisLessThan(String value) {
            addCriterion("DIAGNOSIS <", value, "diagnosis");
            return (Criteria) this;
        }

        public Criteria andDiagnosisLessThanOrEqualTo(String value) {
            addCriterion("DIAGNOSIS <=", value, "diagnosis");
            return (Criteria) this;
        }

        public Criteria andDiagnosisLike(String value) {
            addCriterion("DIAGNOSIS like", value, "diagnosis");
            return (Criteria) this;
        }

        public Criteria andDiagnosisNotLike(String value) {
            addCriterion("DIAGNOSIS not like", value, "diagnosis");
            return (Criteria) this;
        }

        public Criteria andDiagnosisIn(List<String> values) {
            addCriterion("DIAGNOSIS in", values, "diagnosis");
            return (Criteria) this;
        }

        public Criteria andDiagnosisNotIn(List<String> values) {
            addCriterion("DIAGNOSIS not in", values, "diagnosis");
            return (Criteria) this;
        }

        public Criteria andDiagnosisBetween(String value1, String value2) {
            addCriterion("DIAGNOSIS between", value1, value2, "diagnosis");
            return (Criteria) this;
        }

        public Criteria andDiagnosisNotBetween(String value1, String value2) {
            addCriterion("DIAGNOSIS not between", value1, value2, "diagnosis");
            return (Criteria) this;
        }

        public Criteria andHospitalExpireFlagIsNull() {
            addCriterion("HOSPITAL_EXPIRE_FLAG is null");
            return (Criteria) this;
        }

        public Criteria andHospitalExpireFlagIsNotNull() {
            addCriterion("HOSPITAL_EXPIRE_FLAG is not null");
            return (Criteria) this;
        }

        public Criteria andHospitalExpireFlagEqualTo(String value) {
            addCriterion("HOSPITAL_EXPIRE_FLAG =", value, "hospitalExpireFlag");
            return (Criteria) this;
        }

        public Criteria andHospitalExpireFlagNotEqualTo(String value) {
            addCriterion("HOSPITAL_EXPIRE_FLAG <>", value, "hospitalExpireFlag");
            return (Criteria) this;
        }

        public Criteria andHospitalExpireFlagGreaterThan(String value) {
            addCriterion("HOSPITAL_EXPIRE_FLAG >", value, "hospitalExpireFlag");
            return (Criteria) this;
        }

        public Criteria andHospitalExpireFlagGreaterThanOrEqualTo(String value) {
            addCriterion("HOSPITAL_EXPIRE_FLAG >=", value, "hospitalExpireFlag");
            return (Criteria) this;
        }

        public Criteria andHospitalExpireFlagLessThan(String value) {
            addCriterion("HOSPITAL_EXPIRE_FLAG <", value, "hospitalExpireFlag");
            return (Criteria) this;
        }

        public Criteria andHospitalExpireFlagLessThanOrEqualTo(String value) {
            addCriterion("HOSPITAL_EXPIRE_FLAG <=", value, "hospitalExpireFlag");
            return (Criteria) this;
        }

        public Criteria andHospitalExpireFlagLike(String value) {
            addCriterion("HOSPITAL_EXPIRE_FLAG like", value, "hospitalExpireFlag");
            return (Criteria) this;
        }

        public Criteria andHospitalExpireFlagNotLike(String value) {
            addCriterion("HOSPITAL_EXPIRE_FLAG not like", value, "hospitalExpireFlag");
            return (Criteria) this;
        }

        public Criteria andHospitalExpireFlagIn(List<String> values) {
            addCriterion("HOSPITAL_EXPIRE_FLAG in", values, "hospitalExpireFlag");
            return (Criteria) this;
        }

        public Criteria andHospitalExpireFlagNotIn(List<String> values) {
            addCriterion("HOSPITAL_EXPIRE_FLAG not in", values, "hospitalExpireFlag");
            return (Criteria) this;
        }

        public Criteria andHospitalExpireFlagBetween(String value1, String value2) {
            addCriterion("HOSPITAL_EXPIRE_FLAG between", value1, value2, "hospitalExpireFlag");
            return (Criteria) this;
        }

        public Criteria andHospitalExpireFlagNotBetween(String value1, String value2) {
            addCriterion("HOSPITAL_EXPIRE_FLAG not between", value1, value2, "hospitalExpireFlag");
            return (Criteria) this;
        }

        public Criteria andHasCharteventsDataIsNull() {
            addCriterion("HAS_CHARTEVENTS_DATA is null");
            return (Criteria) this;
        }

        public Criteria andHasCharteventsDataIsNotNull() {
            addCriterion("HAS_CHARTEVENTS_DATA is not null");
            return (Criteria) this;
        }

        public Criteria andHasCharteventsDataEqualTo(String value) {
            addCriterion("HAS_CHARTEVENTS_DATA =", value, "hasCharteventsData");
            return (Criteria) this;
        }

        public Criteria andHasCharteventsDataNotEqualTo(String value) {
            addCriterion("HAS_CHARTEVENTS_DATA <>", value, "hasCharteventsData");
            return (Criteria) this;
        }

        public Criteria andHasCharteventsDataGreaterThan(String value) {
            addCriterion("HAS_CHARTEVENTS_DATA >", value, "hasCharteventsData");
            return (Criteria) this;
        }

        public Criteria andHasCharteventsDataGreaterThanOrEqualTo(String value) {
            addCriterion("HAS_CHARTEVENTS_DATA >=", value, "hasCharteventsData");
            return (Criteria) this;
        }

        public Criteria andHasCharteventsDataLessThan(String value) {
            addCriterion("HAS_CHARTEVENTS_DATA <", value, "hasCharteventsData");
            return (Criteria) this;
        }

        public Criteria andHasCharteventsDataLessThanOrEqualTo(String value) {
            addCriterion("HAS_CHARTEVENTS_DATA <=", value, "hasCharteventsData");
            return (Criteria) this;
        }

        public Criteria andHasCharteventsDataLike(String value) {
            addCriterion("HAS_CHARTEVENTS_DATA like", value, "hasCharteventsData");
            return (Criteria) this;
        }

        public Criteria andHasCharteventsDataNotLike(String value) {
            addCriterion("HAS_CHARTEVENTS_DATA not like", value, "hasCharteventsData");
            return (Criteria) this;
        }

        public Criteria andHasCharteventsDataIn(List<String> values) {
            addCriterion("HAS_CHARTEVENTS_DATA in", values, "hasCharteventsData");
            return (Criteria) this;
        }

        public Criteria andHasCharteventsDataNotIn(List<String> values) {
            addCriterion("HAS_CHARTEVENTS_DATA not in", values, "hasCharteventsData");
            return (Criteria) this;
        }

        public Criteria andHasCharteventsDataBetween(String value1, String value2) {
            addCriterion("HAS_CHARTEVENTS_DATA between", value1, value2, "hasCharteventsData");
            return (Criteria) this;
        }

        public Criteria andHasCharteventsDataNotBetween(String value1, String value2) {
            addCriterion("HAS_CHARTEVENTS_DATA not between", value1, value2, "hasCharteventsData");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}