package com.home.springboot.entry;

import java.util.ArrayList;
import java.util.List;

public class VisitExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public VisitExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andVisitIdIsNull() {
            addCriterion("visit_id is null");
            return (Criteria) this;
        }

        public Criteria andVisitIdIsNotNull() {
            addCriterion("visit_id is not null");
            return (Criteria) this;
        }

        public Criteria andVisitIdEqualTo(String value) {
            addCriterion("visit_id =", value, "visitId");
            return (Criteria) this;
        }

        public Criteria andVisitIdNotEqualTo(String value) {
            addCriterion("visit_id <>", value, "visitId");
            return (Criteria) this;
        }

        public Criteria andVisitIdGreaterThan(String value) {
            addCriterion("visit_id >", value, "visitId");
            return (Criteria) this;
        }

        public Criteria andVisitIdGreaterThanOrEqualTo(String value) {
            addCriterion("visit_id >=", value, "visitId");
            return (Criteria) this;
        }

        public Criteria andVisitIdLessThan(String value) {
            addCriterion("visit_id <", value, "visitId");
            return (Criteria) this;
        }

        public Criteria andVisitIdLessThanOrEqualTo(String value) {
            addCriterion("visit_id <=", value, "visitId");
            return (Criteria) this;
        }

        public Criteria andVisitIdLike(String value) {
            addCriterion("visit_id like", value, "visitId");
            return (Criteria) this;
        }

        public Criteria andVisitIdNotLike(String value) {
            addCriterion("visit_id not like", value, "visitId");
            return (Criteria) this;
        }

        public Criteria andVisitIdIn(List<String> values) {
            addCriterion("visit_id in", values, "visitId");
            return (Criteria) this;
        }

        public Criteria andVisitIdNotIn(List<String> values) {
            addCriterion("visit_id not in", values, "visitId");
            return (Criteria) this;
        }

        public Criteria andVisitIdBetween(String value1, String value2) {
            addCriterion("visit_id between", value1, value2, "visitId");
            return (Criteria) this;
        }

        public Criteria andVisitIdNotBetween(String value1, String value2) {
            addCriterion("visit_id not between", value1, value2, "visitId");
            return (Criteria) this;
        }

        public Criteria andPatientIdIsNull() {
            addCriterion("patient_id is null");
            return (Criteria) this;
        }

        public Criteria andPatientIdIsNotNull() {
            addCriterion("patient_id is not null");
            return (Criteria) this;
        }

        public Criteria andPatientIdEqualTo(String value) {
            addCriterion("patient_id =", value, "patientId");
            return (Criteria) this;
        }

        public Criteria andPatientIdNotEqualTo(String value) {
            addCriterion("patient_id <>", value, "patientId");
            return (Criteria) this;
        }

        public Criteria andPatientIdGreaterThan(String value) {
            addCriterion("patient_id >", value, "patientId");
            return (Criteria) this;
        }

        public Criteria andPatientIdGreaterThanOrEqualTo(String value) {
            addCriterion("patient_id >=", value, "patientId");
            return (Criteria) this;
        }

        public Criteria andPatientIdLessThan(String value) {
            addCriterion("patient_id <", value, "patientId");
            return (Criteria) this;
        }

        public Criteria andPatientIdLessThanOrEqualTo(String value) {
            addCriterion("patient_id <=", value, "patientId");
            return (Criteria) this;
        }

        public Criteria andPatientIdLike(String value) {
            addCriterion("patient_id like", value, "patientId");
            return (Criteria) this;
        }

        public Criteria andPatientIdNotLike(String value) {
            addCriterion("patient_id not like", value, "patientId");
            return (Criteria) this;
        }

        public Criteria andPatientIdIn(List<String> values) {
            addCriterion("patient_id in", values, "patientId");
            return (Criteria) this;
        }

        public Criteria andPatientIdNotIn(List<String> values) {
            addCriterion("patient_id not in", values, "patientId");
            return (Criteria) this;
        }

        public Criteria andPatientIdBetween(String value1, String value2) {
            addCriterion("patient_id between", value1, value2, "patientId");
            return (Criteria) this;
        }

        public Criteria andPatientIdNotBetween(String value1, String value2) {
            addCriterion("patient_id not between", value1, value2, "patientId");
            return (Criteria) this;
        }

        public Criteria andVisitTypeIdIsNull() {
            addCriterion("visit_type_id is null");
            return (Criteria) this;
        }

        public Criteria andVisitTypeIdIsNotNull() {
            addCriterion("visit_type_id is not null");
            return (Criteria) this;
        }

        public Criteria andVisitTypeIdEqualTo(String value) {
            addCriterion("visit_type_id =", value, "visitTypeId");
            return (Criteria) this;
        }

        public Criteria andVisitTypeIdNotEqualTo(String value) {
            addCriterion("visit_type_id <>", value, "visitTypeId");
            return (Criteria) this;
        }

        public Criteria andVisitTypeIdGreaterThan(String value) {
            addCriterion("visit_type_id >", value, "visitTypeId");
            return (Criteria) this;
        }

        public Criteria andVisitTypeIdGreaterThanOrEqualTo(String value) {
            addCriterion("visit_type_id >=", value, "visitTypeId");
            return (Criteria) this;
        }

        public Criteria andVisitTypeIdLessThan(String value) {
            addCriterion("visit_type_id <", value, "visitTypeId");
            return (Criteria) this;
        }

        public Criteria andVisitTypeIdLessThanOrEqualTo(String value) {
            addCriterion("visit_type_id <=", value, "visitTypeId");
            return (Criteria) this;
        }

        public Criteria andVisitTypeIdLike(String value) {
            addCriterion("visit_type_id like", value, "visitTypeId");
            return (Criteria) this;
        }

        public Criteria andVisitTypeIdNotLike(String value) {
            addCriterion("visit_type_id not like", value, "visitTypeId");
            return (Criteria) this;
        }

        public Criteria andVisitTypeIdIn(List<String> values) {
            addCriterion("visit_type_id in", values, "visitTypeId");
            return (Criteria) this;
        }

        public Criteria andVisitTypeIdNotIn(List<String> values) {
            addCriterion("visit_type_id not in", values, "visitTypeId");
            return (Criteria) this;
        }

        public Criteria andVisitTypeIdBetween(String value1, String value2) {
            addCriterion("visit_type_id between", value1, value2, "visitTypeId");
            return (Criteria) this;
        }

        public Criteria andVisitTypeIdNotBetween(String value1, String value2) {
            addCriterion("visit_type_id not between", value1, value2, "visitTypeId");
            return (Criteria) this;
        }

        public Criteria andDateStartedIsNull() {
            addCriterion("date_started is null");
            return (Criteria) this;
        }

        public Criteria andDateStartedIsNotNull() {
            addCriterion("date_started is not null");
            return (Criteria) this;
        }

        public Criteria andDateStartedEqualTo(String value) {
            addCriterion("date_started =", value, "dateStarted");
            return (Criteria) this;
        }

        public Criteria andDateStartedNotEqualTo(String value) {
            addCriterion("date_started <>", value, "dateStarted");
            return (Criteria) this;
        }

        public Criteria andDateStartedGreaterThan(String value) {
            addCriterion("date_started >", value, "dateStarted");
            return (Criteria) this;
        }

        public Criteria andDateStartedGreaterThanOrEqualTo(String value) {
            addCriterion("date_started >=", value, "dateStarted");
            return (Criteria) this;
        }

        public Criteria andDateStartedLessThan(String value) {
            addCriterion("date_started <", value, "dateStarted");
            return (Criteria) this;
        }

        public Criteria andDateStartedLessThanOrEqualTo(String value) {
            addCriterion("date_started <=", value, "dateStarted");
            return (Criteria) this;
        }

        public Criteria andDateStartedLike(String value) {
            addCriterion("date_started like", value, "dateStarted");
            return (Criteria) this;
        }

        public Criteria andDateStartedNotLike(String value) {
            addCriterion("date_started not like", value, "dateStarted");
            return (Criteria) this;
        }

        public Criteria andDateStartedIn(List<String> values) {
            addCriterion("date_started in", values, "dateStarted");
            return (Criteria) this;
        }

        public Criteria andDateStartedNotIn(List<String> values) {
            addCriterion("date_started not in", values, "dateStarted");
            return (Criteria) this;
        }

        public Criteria andDateStartedBetween(String value1, String value2) {
            addCriterion("date_started between", value1, value2, "dateStarted");
            return (Criteria) this;
        }

        public Criteria andDateStartedNotBetween(String value1, String value2) {
            addCriterion("date_started not between", value1, value2, "dateStarted");
            return (Criteria) this;
        }

        public Criteria andDateStoppedIsNull() {
            addCriterion("date_stopped is null");
            return (Criteria) this;
        }

        public Criteria andDateStoppedIsNotNull() {
            addCriterion("date_stopped is not null");
            return (Criteria) this;
        }

        public Criteria andDateStoppedEqualTo(String value) {
            addCriterion("date_stopped =", value, "dateStopped");
            return (Criteria) this;
        }

        public Criteria andDateStoppedNotEqualTo(String value) {
            addCriterion("date_stopped <>", value, "dateStopped");
            return (Criteria) this;
        }

        public Criteria andDateStoppedGreaterThan(String value) {
            addCriterion("date_stopped >", value, "dateStopped");
            return (Criteria) this;
        }

        public Criteria andDateStoppedGreaterThanOrEqualTo(String value) {
            addCriterion("date_stopped >=", value, "dateStopped");
            return (Criteria) this;
        }

        public Criteria andDateStoppedLessThan(String value) {
            addCriterion("date_stopped <", value, "dateStopped");
            return (Criteria) this;
        }

        public Criteria andDateStoppedLessThanOrEqualTo(String value) {
            addCriterion("date_stopped <=", value, "dateStopped");
            return (Criteria) this;
        }

        public Criteria andDateStoppedLike(String value) {
            addCriterion("date_stopped like", value, "dateStopped");
            return (Criteria) this;
        }

        public Criteria andDateStoppedNotLike(String value) {
            addCriterion("date_stopped not like", value, "dateStopped");
            return (Criteria) this;
        }

        public Criteria andDateStoppedIn(List<String> values) {
            addCriterion("date_stopped in", values, "dateStopped");
            return (Criteria) this;
        }

        public Criteria andDateStoppedNotIn(List<String> values) {
            addCriterion("date_stopped not in", values, "dateStopped");
            return (Criteria) this;
        }

        public Criteria andDateStoppedBetween(String value1, String value2) {
            addCriterion("date_stopped between", value1, value2, "dateStopped");
            return (Criteria) this;
        }

        public Criteria andDateStoppedNotBetween(String value1, String value2) {
            addCriterion("date_stopped not between", value1, value2, "dateStopped");
            return (Criteria) this;
        }

        public Criteria andIndicationConceptIdIsNull() {
            addCriterion("indication_concept_id is null");
            return (Criteria) this;
        }

        public Criteria andIndicationConceptIdIsNotNull() {
            addCriterion("indication_concept_id is not null");
            return (Criteria) this;
        }

        public Criteria andIndicationConceptIdEqualTo(String value) {
            addCriterion("indication_concept_id =", value, "indicationConceptId");
            return (Criteria) this;
        }

        public Criteria andIndicationConceptIdNotEqualTo(String value) {
            addCriterion("indication_concept_id <>", value, "indicationConceptId");
            return (Criteria) this;
        }

        public Criteria andIndicationConceptIdGreaterThan(String value) {
            addCriterion("indication_concept_id >", value, "indicationConceptId");
            return (Criteria) this;
        }

        public Criteria andIndicationConceptIdGreaterThanOrEqualTo(String value) {
            addCriterion("indication_concept_id >=", value, "indicationConceptId");
            return (Criteria) this;
        }

        public Criteria andIndicationConceptIdLessThan(String value) {
            addCriterion("indication_concept_id <", value, "indicationConceptId");
            return (Criteria) this;
        }

        public Criteria andIndicationConceptIdLessThanOrEqualTo(String value) {
            addCriterion("indication_concept_id <=", value, "indicationConceptId");
            return (Criteria) this;
        }

        public Criteria andIndicationConceptIdLike(String value) {
            addCriterion("indication_concept_id like", value, "indicationConceptId");
            return (Criteria) this;
        }

        public Criteria andIndicationConceptIdNotLike(String value) {
            addCriterion("indication_concept_id not like", value, "indicationConceptId");
            return (Criteria) this;
        }

        public Criteria andIndicationConceptIdIn(List<String> values) {
            addCriterion("indication_concept_id in", values, "indicationConceptId");
            return (Criteria) this;
        }

        public Criteria andIndicationConceptIdNotIn(List<String> values) {
            addCriterion("indication_concept_id not in", values, "indicationConceptId");
            return (Criteria) this;
        }

        public Criteria andIndicationConceptIdBetween(String value1, String value2) {
            addCriterion("indication_concept_id between", value1, value2, "indicationConceptId");
            return (Criteria) this;
        }

        public Criteria andIndicationConceptIdNotBetween(String value1, String value2) {
            addCriterion("indication_concept_id not between", value1, value2, "indicationConceptId");
            return (Criteria) this;
        }

        public Criteria andLocationIdIsNull() {
            addCriterion("location_id is null");
            return (Criteria) this;
        }

        public Criteria andLocationIdIsNotNull() {
            addCriterion("location_id is not null");
            return (Criteria) this;
        }

        public Criteria andLocationIdEqualTo(String value) {
            addCriterion("location_id =", value, "locationId");
            return (Criteria) this;
        }

        public Criteria andLocationIdNotEqualTo(String value) {
            addCriterion("location_id <>", value, "locationId");
            return (Criteria) this;
        }

        public Criteria andLocationIdGreaterThan(String value) {
            addCriterion("location_id >", value, "locationId");
            return (Criteria) this;
        }

        public Criteria andLocationIdGreaterThanOrEqualTo(String value) {
            addCriterion("location_id >=", value, "locationId");
            return (Criteria) this;
        }

        public Criteria andLocationIdLessThan(String value) {
            addCriterion("location_id <", value, "locationId");
            return (Criteria) this;
        }

        public Criteria andLocationIdLessThanOrEqualTo(String value) {
            addCriterion("location_id <=", value, "locationId");
            return (Criteria) this;
        }

        public Criteria andLocationIdLike(String value) {
            addCriterion("location_id like", value, "locationId");
            return (Criteria) this;
        }

        public Criteria andLocationIdNotLike(String value) {
            addCriterion("location_id not like", value, "locationId");
            return (Criteria) this;
        }

        public Criteria andLocationIdIn(List<String> values) {
            addCriterion("location_id in", values, "locationId");
            return (Criteria) this;
        }

        public Criteria andLocationIdNotIn(List<String> values) {
            addCriterion("location_id not in", values, "locationId");
            return (Criteria) this;
        }

        public Criteria andLocationIdBetween(String value1, String value2) {
            addCriterion("location_id between", value1, value2, "locationId");
            return (Criteria) this;
        }

        public Criteria andLocationIdNotBetween(String value1, String value2) {
            addCriterion("location_id not between", value1, value2, "locationId");
            return (Criteria) this;
        }

        public Criteria andCreatorIsNull() {
            addCriterion("creator is null");
            return (Criteria) this;
        }

        public Criteria andCreatorIsNotNull() {
            addCriterion("creator is not null");
            return (Criteria) this;
        }

        public Criteria andCreatorEqualTo(String value) {
            addCriterion("creator =", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotEqualTo(String value) {
            addCriterion("creator <>", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorGreaterThan(String value) {
            addCriterion("creator >", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorGreaterThanOrEqualTo(String value) {
            addCriterion("creator >=", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorLessThan(String value) {
            addCriterion("creator <", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorLessThanOrEqualTo(String value) {
            addCriterion("creator <=", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorLike(String value) {
            addCriterion("creator like", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotLike(String value) {
            addCriterion("creator not like", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorIn(List<String> values) {
            addCriterion("creator in", values, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotIn(List<String> values) {
            addCriterion("creator not in", values, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorBetween(String value1, String value2) {
            addCriterion("creator between", value1, value2, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotBetween(String value1, String value2) {
            addCriterion("creator not between", value1, value2, "creator");
            return (Criteria) this;
        }

        public Criteria andDateCreatedIsNull() {
            addCriterion("date_created is null");
            return (Criteria) this;
        }

        public Criteria andDateCreatedIsNotNull() {
            addCriterion("date_created is not null");
            return (Criteria) this;
        }

        public Criteria andDateCreatedEqualTo(String value) {
            addCriterion("date_created =", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedNotEqualTo(String value) {
            addCriterion("date_created <>", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedGreaterThan(String value) {
            addCriterion("date_created >", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedGreaterThanOrEqualTo(String value) {
            addCriterion("date_created >=", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedLessThan(String value) {
            addCriterion("date_created <", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedLessThanOrEqualTo(String value) {
            addCriterion("date_created <=", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedLike(String value) {
            addCriterion("date_created like", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedNotLike(String value) {
            addCriterion("date_created not like", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedIn(List<String> values) {
            addCriterion("date_created in", values, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedNotIn(List<String> values) {
            addCriterion("date_created not in", values, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedBetween(String value1, String value2) {
            addCriterion("date_created between", value1, value2, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedNotBetween(String value1, String value2) {
            addCriterion("date_created not between", value1, value2, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andChangedByIsNull() {
            addCriterion("changed_by is null");
            return (Criteria) this;
        }

        public Criteria andChangedByIsNotNull() {
            addCriterion("changed_by is not null");
            return (Criteria) this;
        }

        public Criteria andChangedByEqualTo(String value) {
            addCriterion("changed_by =", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByNotEqualTo(String value) {
            addCriterion("changed_by <>", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByGreaterThan(String value) {
            addCriterion("changed_by >", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByGreaterThanOrEqualTo(String value) {
            addCriterion("changed_by >=", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByLessThan(String value) {
            addCriterion("changed_by <", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByLessThanOrEqualTo(String value) {
            addCriterion("changed_by <=", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByLike(String value) {
            addCriterion("changed_by like", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByNotLike(String value) {
            addCriterion("changed_by not like", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByIn(List<String> values) {
            addCriterion("changed_by in", values, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByNotIn(List<String> values) {
            addCriterion("changed_by not in", values, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByBetween(String value1, String value2) {
            addCriterion("changed_by between", value1, value2, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByNotBetween(String value1, String value2) {
            addCriterion("changed_by not between", value1, value2, "changedBy");
            return (Criteria) this;
        }

        public Criteria andDateChangedIsNull() {
            addCriterion("date_changed is null");
            return (Criteria) this;
        }

        public Criteria andDateChangedIsNotNull() {
            addCriterion("date_changed is not null");
            return (Criteria) this;
        }

        public Criteria andDateChangedEqualTo(String value) {
            addCriterion("date_changed =", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedNotEqualTo(String value) {
            addCriterion("date_changed <>", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedGreaterThan(String value) {
            addCriterion("date_changed >", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedGreaterThanOrEqualTo(String value) {
            addCriterion("date_changed >=", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedLessThan(String value) {
            addCriterion("date_changed <", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedLessThanOrEqualTo(String value) {
            addCriterion("date_changed <=", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedLike(String value) {
            addCriterion("date_changed like", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedNotLike(String value) {
            addCriterion("date_changed not like", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedIn(List<String> values) {
            addCriterion("date_changed in", values, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedNotIn(List<String> values) {
            addCriterion("date_changed not in", values, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedBetween(String value1, String value2) {
            addCriterion("date_changed between", value1, value2, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedNotBetween(String value1, String value2) {
            addCriterion("date_changed not between", value1, value2, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andVoidedIsNull() {
            addCriterion("voided is null");
            return (Criteria) this;
        }

        public Criteria andVoidedIsNotNull() {
            addCriterion("voided is not null");
            return (Criteria) this;
        }

        public Criteria andVoidedEqualTo(String value) {
            addCriterion("voided =", value, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedNotEqualTo(String value) {
            addCriterion("voided <>", value, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedGreaterThan(String value) {
            addCriterion("voided >", value, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedGreaterThanOrEqualTo(String value) {
            addCriterion("voided >=", value, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedLessThan(String value) {
            addCriterion("voided <", value, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedLessThanOrEqualTo(String value) {
            addCriterion("voided <=", value, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedLike(String value) {
            addCriterion("voided like", value, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedNotLike(String value) {
            addCriterion("voided not like", value, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedIn(List<String> values) {
            addCriterion("voided in", values, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedNotIn(List<String> values) {
            addCriterion("voided not in", values, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedBetween(String value1, String value2) {
            addCriterion("voided between", value1, value2, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedNotBetween(String value1, String value2) {
            addCriterion("voided not between", value1, value2, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedByIsNull() {
            addCriterion("voided_by is null");
            return (Criteria) this;
        }

        public Criteria andVoidedByIsNotNull() {
            addCriterion("voided_by is not null");
            return (Criteria) this;
        }

        public Criteria andVoidedByEqualTo(String value) {
            addCriterion("voided_by =", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByNotEqualTo(String value) {
            addCriterion("voided_by <>", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByGreaterThan(String value) {
            addCriterion("voided_by >", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByGreaterThanOrEqualTo(String value) {
            addCriterion("voided_by >=", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByLessThan(String value) {
            addCriterion("voided_by <", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByLessThanOrEqualTo(String value) {
            addCriterion("voided_by <=", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByLike(String value) {
            addCriterion("voided_by like", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByNotLike(String value) {
            addCriterion("voided_by not like", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByIn(List<String> values) {
            addCriterion("voided_by in", values, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByNotIn(List<String> values) {
            addCriterion("voided_by not in", values, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByBetween(String value1, String value2) {
            addCriterion("voided_by between", value1, value2, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByNotBetween(String value1, String value2) {
            addCriterion("voided_by not between", value1, value2, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andDateVoidedIsNull() {
            addCriterion("date_voided is null");
            return (Criteria) this;
        }

        public Criteria andDateVoidedIsNotNull() {
            addCriterion("date_voided is not null");
            return (Criteria) this;
        }

        public Criteria andDateVoidedEqualTo(String value) {
            addCriterion("date_voided =", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedNotEqualTo(String value) {
            addCriterion("date_voided <>", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedGreaterThan(String value) {
            addCriterion("date_voided >", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedGreaterThanOrEqualTo(String value) {
            addCriterion("date_voided >=", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedLessThan(String value) {
            addCriterion("date_voided <", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedLessThanOrEqualTo(String value) {
            addCriterion("date_voided <=", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedLike(String value) {
            addCriterion("date_voided like", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedNotLike(String value) {
            addCriterion("date_voided not like", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedIn(List<String> values) {
            addCriterion("date_voided in", values, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedNotIn(List<String> values) {
            addCriterion("date_voided not in", values, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedBetween(String value1, String value2) {
            addCriterion("date_voided between", value1, value2, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedNotBetween(String value1, String value2) {
            addCriterion("date_voided not between", value1, value2, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andVoidReasonIsNull() {
            addCriterion("void_reason is null");
            return (Criteria) this;
        }

        public Criteria andVoidReasonIsNotNull() {
            addCriterion("void_reason is not null");
            return (Criteria) this;
        }

        public Criteria andVoidReasonEqualTo(String value) {
            addCriterion("void_reason =", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonNotEqualTo(String value) {
            addCriterion("void_reason <>", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonGreaterThan(String value) {
            addCriterion("void_reason >", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonGreaterThanOrEqualTo(String value) {
            addCriterion("void_reason >=", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonLessThan(String value) {
            addCriterion("void_reason <", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonLessThanOrEqualTo(String value) {
            addCriterion("void_reason <=", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonLike(String value) {
            addCriterion("void_reason like", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonNotLike(String value) {
            addCriterion("void_reason not like", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonIn(List<String> values) {
            addCriterion("void_reason in", values, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonNotIn(List<String> values) {
            addCriterion("void_reason not in", values, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonBetween(String value1, String value2) {
            addCriterion("void_reason between", value1, value2, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonNotBetween(String value1, String value2) {
            addCriterion("void_reason not between", value1, value2, "voidReason");
            return (Criteria) this;
        }

        public Criteria andUuidIsNull() {
            addCriterion("uuid is null");
            return (Criteria) this;
        }

        public Criteria andUuidIsNotNull() {
            addCriterion("uuid is not null");
            return (Criteria) this;
        }

        public Criteria andUuidEqualTo(String value) {
            addCriterion("uuid =", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotEqualTo(String value) {
            addCriterion("uuid <>", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidGreaterThan(String value) {
            addCriterion("uuid >", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidGreaterThanOrEqualTo(String value) {
            addCriterion("uuid >=", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLessThan(String value) {
            addCriterion("uuid <", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLessThanOrEqualTo(String value) {
            addCriterion("uuid <=", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLike(String value) {
            addCriterion("uuid like", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotLike(String value) {
            addCriterion("uuid not like", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidIn(List<String> values) {
            addCriterion("uuid in", values, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotIn(List<String> values) {
            addCriterion("uuid not in", values, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidBetween(String value1, String value2) {
            addCriterion("uuid between", value1, value2, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotBetween(String value1, String value2) {
            addCriterion("uuid not between", value1, value2, "uuid");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}