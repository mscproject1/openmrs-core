package com.home.springboot.entry;

import java.util.ArrayList;
import java.util.List;

public class PersonExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PersonExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andPersonIdIsNull() {
            addCriterion("person_id is null");
            return (Criteria) this;
        }

        public Criteria andPersonIdIsNotNull() {
            addCriterion("person_id is not null");
            return (Criteria) this;
        }

        public Criteria andPersonIdEqualTo(String value) {
            addCriterion("person_id =", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotEqualTo(String value) {
            addCriterion("person_id <>", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdGreaterThan(String value) {
            addCriterion("person_id >", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdGreaterThanOrEqualTo(String value) {
            addCriterion("person_id >=", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdLessThan(String value) {
            addCriterion("person_id <", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdLessThanOrEqualTo(String value) {
            addCriterion("person_id <=", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdLike(String value) {
            addCriterion("person_id like", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotLike(String value) {
            addCriterion("person_id not like", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdIn(List<String> values) {
            addCriterion("person_id in", values, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotIn(List<String> values) {
            addCriterion("person_id not in", values, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdBetween(String value1, String value2) {
            addCriterion("person_id between", value1, value2, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotBetween(String value1, String value2) {
            addCriterion("person_id not between", value1, value2, "personId");
            return (Criteria) this;
        }

        public Criteria andGenderIsNull() {
            addCriterion("gender is null");
            return (Criteria) this;
        }

        public Criteria andGenderIsNotNull() {
            addCriterion("gender is not null");
            return (Criteria) this;
        }

        public Criteria andGenderEqualTo(String value) {
            addCriterion("gender =", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderNotEqualTo(String value) {
            addCriterion("gender <>", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderGreaterThan(String value) {
            addCriterion("gender >", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderGreaterThanOrEqualTo(String value) {
            addCriterion("gender >=", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderLessThan(String value) {
            addCriterion("gender <", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderLessThanOrEqualTo(String value) {
            addCriterion("gender <=", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderLike(String value) {
            addCriterion("gender like", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderNotLike(String value) {
            addCriterion("gender not like", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderIn(List<String> values) {
            addCriterion("gender in", values, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderNotIn(List<String> values) {
            addCriterion("gender not in", values, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderBetween(String value1, String value2) {
            addCriterion("gender between", value1, value2, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderNotBetween(String value1, String value2) {
            addCriterion("gender not between", value1, value2, "gender");
            return (Criteria) this;
        }

        public Criteria andBirthdateIsNull() {
            addCriterion("birthdate is null");
            return (Criteria) this;
        }

        public Criteria andBirthdateIsNotNull() {
            addCriterion("birthdate is not null");
            return (Criteria) this;
        }

        public Criteria andBirthdateEqualTo(String value) {
            addCriterion("birthdate =", value, "birthdate");
            return (Criteria) this;
        }

        public Criteria andBirthdateNotEqualTo(String value) {
            addCriterion("birthdate <>", value, "birthdate");
            return (Criteria) this;
        }

        public Criteria andBirthdateGreaterThan(String value) {
            addCriterion("birthdate >", value, "birthdate");
            return (Criteria) this;
        }

        public Criteria andBirthdateGreaterThanOrEqualTo(String value) {
            addCriterion("birthdate >=", value, "birthdate");
            return (Criteria) this;
        }

        public Criteria andBirthdateLessThan(String value) {
            addCriterion("birthdate <", value, "birthdate");
            return (Criteria) this;
        }

        public Criteria andBirthdateLessThanOrEqualTo(String value) {
            addCriterion("birthdate <=", value, "birthdate");
            return (Criteria) this;
        }

        public Criteria andBirthdateLike(String value) {
            addCriterion("birthdate like", value, "birthdate");
            return (Criteria) this;
        }

        public Criteria andBirthdateNotLike(String value) {
            addCriterion("birthdate not like", value, "birthdate");
            return (Criteria) this;
        }

        public Criteria andBirthdateIn(List<String> values) {
            addCriterion("birthdate in", values, "birthdate");
            return (Criteria) this;
        }

        public Criteria andBirthdateNotIn(List<String> values) {
            addCriterion("birthdate not in", values, "birthdate");
            return (Criteria) this;
        }

        public Criteria andBirthdateBetween(String value1, String value2) {
            addCriterion("birthdate between", value1, value2, "birthdate");
            return (Criteria) this;
        }

        public Criteria andBirthdateNotBetween(String value1, String value2) {
            addCriterion("birthdate not between", value1, value2, "birthdate");
            return (Criteria) this;
        }

        public Criteria andBirthdateEstimatedIsNull() {
            addCriterion("birthdate_estimated is null");
            return (Criteria) this;
        }

        public Criteria andBirthdateEstimatedIsNotNull() {
            addCriterion("birthdate_estimated is not null");
            return (Criteria) this;
        }

        public Criteria andBirthdateEstimatedEqualTo(String value) {
            addCriterion("birthdate_estimated =", value, "birthdateEstimated");
            return (Criteria) this;
        }

        public Criteria andBirthdateEstimatedNotEqualTo(String value) {
            addCriterion("birthdate_estimated <>", value, "birthdateEstimated");
            return (Criteria) this;
        }

        public Criteria andBirthdateEstimatedGreaterThan(String value) {
            addCriterion("birthdate_estimated >", value, "birthdateEstimated");
            return (Criteria) this;
        }

        public Criteria andBirthdateEstimatedGreaterThanOrEqualTo(String value) {
            addCriterion("birthdate_estimated >=", value, "birthdateEstimated");
            return (Criteria) this;
        }

        public Criteria andBirthdateEstimatedLessThan(String value) {
            addCriterion("birthdate_estimated <", value, "birthdateEstimated");
            return (Criteria) this;
        }

        public Criteria andBirthdateEstimatedLessThanOrEqualTo(String value) {
            addCriterion("birthdate_estimated <=", value, "birthdateEstimated");
            return (Criteria) this;
        }

        public Criteria andBirthdateEstimatedLike(String value) {
            addCriterion("birthdate_estimated like", value, "birthdateEstimated");
            return (Criteria) this;
        }

        public Criteria andBirthdateEstimatedNotLike(String value) {
            addCriterion("birthdate_estimated not like", value, "birthdateEstimated");
            return (Criteria) this;
        }

        public Criteria andBirthdateEstimatedIn(List<String> values) {
            addCriterion("birthdate_estimated in", values, "birthdateEstimated");
            return (Criteria) this;
        }

        public Criteria andBirthdateEstimatedNotIn(List<String> values) {
            addCriterion("birthdate_estimated not in", values, "birthdateEstimated");
            return (Criteria) this;
        }

        public Criteria andBirthdateEstimatedBetween(String value1, String value2) {
            addCriterion("birthdate_estimated between", value1, value2, "birthdateEstimated");
            return (Criteria) this;
        }

        public Criteria andBirthdateEstimatedNotBetween(String value1, String value2) {
            addCriterion("birthdate_estimated not between", value1, value2, "birthdateEstimated");
            return (Criteria) this;
        }

        public Criteria andDeadIsNull() {
            addCriterion("dead is null");
            return (Criteria) this;
        }

        public Criteria andDeadIsNotNull() {
            addCriterion("dead is not null");
            return (Criteria) this;
        }

        public Criteria andDeadEqualTo(String value) {
            addCriterion("dead =", value, "dead");
            return (Criteria) this;
        }

        public Criteria andDeadNotEqualTo(String value) {
            addCriterion("dead <>", value, "dead");
            return (Criteria) this;
        }

        public Criteria andDeadGreaterThan(String value) {
            addCriterion("dead >", value, "dead");
            return (Criteria) this;
        }

        public Criteria andDeadGreaterThanOrEqualTo(String value) {
            addCriterion("dead >=", value, "dead");
            return (Criteria) this;
        }

        public Criteria andDeadLessThan(String value) {
            addCriterion("dead <", value, "dead");
            return (Criteria) this;
        }

        public Criteria andDeadLessThanOrEqualTo(String value) {
            addCriterion("dead <=", value, "dead");
            return (Criteria) this;
        }

        public Criteria andDeadLike(String value) {
            addCriterion("dead like", value, "dead");
            return (Criteria) this;
        }

        public Criteria andDeadNotLike(String value) {
            addCriterion("dead not like", value, "dead");
            return (Criteria) this;
        }

        public Criteria andDeadIn(List<String> values) {
            addCriterion("dead in", values, "dead");
            return (Criteria) this;
        }

        public Criteria andDeadNotIn(List<String> values) {
            addCriterion("dead not in", values, "dead");
            return (Criteria) this;
        }

        public Criteria andDeadBetween(String value1, String value2) {
            addCriterion("dead between", value1, value2, "dead");
            return (Criteria) this;
        }

        public Criteria andDeadNotBetween(String value1, String value2) {
            addCriterion("dead not between", value1, value2, "dead");
            return (Criteria) this;
        }

        public Criteria andDeathDateIsNull() {
            addCriterion("death_date is null");
            return (Criteria) this;
        }

        public Criteria andDeathDateIsNotNull() {
            addCriterion("death_date is not null");
            return (Criteria) this;
        }

        public Criteria andDeathDateEqualTo(String value) {
            addCriterion("death_date =", value, "deathDate");
            return (Criteria) this;
        }

        public Criteria andDeathDateNotEqualTo(String value) {
            addCriterion("death_date <>", value, "deathDate");
            return (Criteria) this;
        }

        public Criteria andDeathDateGreaterThan(String value) {
            addCriterion("death_date >", value, "deathDate");
            return (Criteria) this;
        }

        public Criteria andDeathDateGreaterThanOrEqualTo(String value) {
            addCriterion("death_date >=", value, "deathDate");
            return (Criteria) this;
        }

        public Criteria andDeathDateLessThan(String value) {
            addCriterion("death_date <", value, "deathDate");
            return (Criteria) this;
        }

        public Criteria andDeathDateLessThanOrEqualTo(String value) {
            addCriterion("death_date <=", value, "deathDate");
            return (Criteria) this;
        }

        public Criteria andDeathDateLike(String value) {
            addCriterion("death_date like", value, "deathDate");
            return (Criteria) this;
        }

        public Criteria andDeathDateNotLike(String value) {
            addCriterion("death_date not like", value, "deathDate");
            return (Criteria) this;
        }

        public Criteria andDeathDateIn(List<String> values) {
            addCriterion("death_date in", values, "deathDate");
            return (Criteria) this;
        }

        public Criteria andDeathDateNotIn(List<String> values) {
            addCriterion("death_date not in", values, "deathDate");
            return (Criteria) this;
        }

        public Criteria andDeathDateBetween(String value1, String value2) {
            addCriterion("death_date between", value1, value2, "deathDate");
            return (Criteria) this;
        }

        public Criteria andDeathDateNotBetween(String value1, String value2) {
            addCriterion("death_date not between", value1, value2, "deathDate");
            return (Criteria) this;
        }

        public Criteria andCauseOfDeathIsNull() {
            addCriterion("cause_of_death is null");
            return (Criteria) this;
        }

        public Criteria andCauseOfDeathIsNotNull() {
            addCriterion("cause_of_death is not null");
            return (Criteria) this;
        }

        public Criteria andCauseOfDeathEqualTo(String value) {
            addCriterion("cause_of_death =", value, "causeOfDeath");
            return (Criteria) this;
        }

        public Criteria andCauseOfDeathNotEqualTo(String value) {
            addCriterion("cause_of_death <>", value, "causeOfDeath");
            return (Criteria) this;
        }

        public Criteria andCauseOfDeathGreaterThan(String value) {
            addCriterion("cause_of_death >", value, "causeOfDeath");
            return (Criteria) this;
        }

        public Criteria andCauseOfDeathGreaterThanOrEqualTo(String value) {
            addCriterion("cause_of_death >=", value, "causeOfDeath");
            return (Criteria) this;
        }

        public Criteria andCauseOfDeathLessThan(String value) {
            addCriterion("cause_of_death <", value, "causeOfDeath");
            return (Criteria) this;
        }

        public Criteria andCauseOfDeathLessThanOrEqualTo(String value) {
            addCriterion("cause_of_death <=", value, "causeOfDeath");
            return (Criteria) this;
        }

        public Criteria andCauseOfDeathLike(String value) {
            addCriterion("cause_of_death like", value, "causeOfDeath");
            return (Criteria) this;
        }

        public Criteria andCauseOfDeathNotLike(String value) {
            addCriterion("cause_of_death not like", value, "causeOfDeath");
            return (Criteria) this;
        }

        public Criteria andCauseOfDeathIn(List<String> values) {
            addCriterion("cause_of_death in", values, "causeOfDeath");
            return (Criteria) this;
        }

        public Criteria andCauseOfDeathNotIn(List<String> values) {
            addCriterion("cause_of_death not in", values, "causeOfDeath");
            return (Criteria) this;
        }

        public Criteria andCauseOfDeathBetween(String value1, String value2) {
            addCriterion("cause_of_death between", value1, value2, "causeOfDeath");
            return (Criteria) this;
        }

        public Criteria andCauseOfDeathNotBetween(String value1, String value2) {
            addCriterion("cause_of_death not between", value1, value2, "causeOfDeath");
            return (Criteria) this;
        }

        public Criteria andCreatorIsNull() {
            addCriterion("creator is null");
            return (Criteria) this;
        }

        public Criteria andCreatorIsNotNull() {
            addCriterion("creator is not null");
            return (Criteria) this;
        }

        public Criteria andCreatorEqualTo(String value) {
            addCriterion("creator =", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotEqualTo(String value) {
            addCriterion("creator <>", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorGreaterThan(String value) {
            addCriterion("creator >", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorGreaterThanOrEqualTo(String value) {
            addCriterion("creator >=", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorLessThan(String value) {
            addCriterion("creator <", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorLessThanOrEqualTo(String value) {
            addCriterion("creator <=", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorLike(String value) {
            addCriterion("creator like", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotLike(String value) {
            addCriterion("creator not like", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorIn(List<String> values) {
            addCriterion("creator in", values, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotIn(List<String> values) {
            addCriterion("creator not in", values, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorBetween(String value1, String value2) {
            addCriterion("creator between", value1, value2, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotBetween(String value1, String value2) {
            addCriterion("creator not between", value1, value2, "creator");
            return (Criteria) this;
        }

        public Criteria andDateCreatedIsNull() {
            addCriterion("date_created is null");
            return (Criteria) this;
        }

        public Criteria andDateCreatedIsNotNull() {
            addCriterion("date_created is not null");
            return (Criteria) this;
        }

        public Criteria andDateCreatedEqualTo(String value) {
            addCriterion("date_created =", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedNotEqualTo(String value) {
            addCriterion("date_created <>", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedGreaterThan(String value) {
            addCriterion("date_created >", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedGreaterThanOrEqualTo(String value) {
            addCriterion("date_created >=", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedLessThan(String value) {
            addCriterion("date_created <", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedLessThanOrEqualTo(String value) {
            addCriterion("date_created <=", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedLike(String value) {
            addCriterion("date_created like", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedNotLike(String value) {
            addCriterion("date_created not like", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedIn(List<String> values) {
            addCriterion("date_created in", values, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedNotIn(List<String> values) {
            addCriterion("date_created not in", values, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedBetween(String value1, String value2) {
            addCriterion("date_created between", value1, value2, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedNotBetween(String value1, String value2) {
            addCriterion("date_created not between", value1, value2, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andChangedByIsNull() {
            addCriterion("changed_by is null");
            return (Criteria) this;
        }

        public Criteria andChangedByIsNotNull() {
            addCriterion("changed_by is not null");
            return (Criteria) this;
        }

        public Criteria andChangedByEqualTo(String value) {
            addCriterion("changed_by =", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByNotEqualTo(String value) {
            addCriterion("changed_by <>", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByGreaterThan(String value) {
            addCriterion("changed_by >", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByGreaterThanOrEqualTo(String value) {
            addCriterion("changed_by >=", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByLessThan(String value) {
            addCriterion("changed_by <", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByLessThanOrEqualTo(String value) {
            addCriterion("changed_by <=", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByLike(String value) {
            addCriterion("changed_by like", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByNotLike(String value) {
            addCriterion("changed_by not like", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByIn(List<String> values) {
            addCriterion("changed_by in", values, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByNotIn(List<String> values) {
            addCriterion("changed_by not in", values, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByBetween(String value1, String value2) {
            addCriterion("changed_by between", value1, value2, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByNotBetween(String value1, String value2) {
            addCriterion("changed_by not between", value1, value2, "changedBy");
            return (Criteria) this;
        }

        public Criteria andDateChangedIsNull() {
            addCriterion("date_changed is null");
            return (Criteria) this;
        }

        public Criteria andDateChangedIsNotNull() {
            addCriterion("date_changed is not null");
            return (Criteria) this;
        }

        public Criteria andDateChangedEqualTo(String value) {
            addCriterion("date_changed =", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedNotEqualTo(String value) {
            addCriterion("date_changed <>", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedGreaterThan(String value) {
            addCriterion("date_changed >", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedGreaterThanOrEqualTo(String value) {
            addCriterion("date_changed >=", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedLessThan(String value) {
            addCriterion("date_changed <", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedLessThanOrEqualTo(String value) {
            addCriterion("date_changed <=", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedLike(String value) {
            addCriterion("date_changed like", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedNotLike(String value) {
            addCriterion("date_changed not like", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedIn(List<String> values) {
            addCriterion("date_changed in", values, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedNotIn(List<String> values) {
            addCriterion("date_changed not in", values, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedBetween(String value1, String value2) {
            addCriterion("date_changed between", value1, value2, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedNotBetween(String value1, String value2) {
            addCriterion("date_changed not between", value1, value2, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andVoidedIsNull() {
            addCriterion("voided is null");
            return (Criteria) this;
        }

        public Criteria andVoidedIsNotNull() {
            addCriterion("voided is not null");
            return (Criteria) this;
        }

        public Criteria andVoidedEqualTo(String value) {
            addCriterion("voided =", value, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedNotEqualTo(String value) {
            addCriterion("voided <>", value, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedGreaterThan(String value) {
            addCriterion("voided >", value, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedGreaterThanOrEqualTo(String value) {
            addCriterion("voided >=", value, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedLessThan(String value) {
            addCriterion("voided <", value, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedLessThanOrEqualTo(String value) {
            addCriterion("voided <=", value, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedLike(String value) {
            addCriterion("voided like", value, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedNotLike(String value) {
            addCriterion("voided not like", value, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedIn(List<String> values) {
            addCriterion("voided in", values, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedNotIn(List<String> values) {
            addCriterion("voided not in", values, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedBetween(String value1, String value2) {
            addCriterion("voided between", value1, value2, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedNotBetween(String value1, String value2) {
            addCriterion("voided not between", value1, value2, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedByIsNull() {
            addCriterion("voided_by is null");
            return (Criteria) this;
        }

        public Criteria andVoidedByIsNotNull() {
            addCriterion("voided_by is not null");
            return (Criteria) this;
        }

        public Criteria andVoidedByEqualTo(String value) {
            addCriterion("voided_by =", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByNotEqualTo(String value) {
            addCriterion("voided_by <>", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByGreaterThan(String value) {
            addCriterion("voided_by >", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByGreaterThanOrEqualTo(String value) {
            addCriterion("voided_by >=", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByLessThan(String value) {
            addCriterion("voided_by <", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByLessThanOrEqualTo(String value) {
            addCriterion("voided_by <=", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByLike(String value) {
            addCriterion("voided_by like", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByNotLike(String value) {
            addCriterion("voided_by not like", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByIn(List<String> values) {
            addCriterion("voided_by in", values, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByNotIn(List<String> values) {
            addCriterion("voided_by not in", values, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByBetween(String value1, String value2) {
            addCriterion("voided_by between", value1, value2, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByNotBetween(String value1, String value2) {
            addCriterion("voided_by not between", value1, value2, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andDateVoidedIsNull() {
            addCriterion("date_voided is null");
            return (Criteria) this;
        }

        public Criteria andDateVoidedIsNotNull() {
            addCriterion("date_voided is not null");
            return (Criteria) this;
        }

        public Criteria andDateVoidedEqualTo(String value) {
            addCriterion("date_voided =", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedNotEqualTo(String value) {
            addCriterion("date_voided <>", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedGreaterThan(String value) {
            addCriterion("date_voided >", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedGreaterThanOrEqualTo(String value) {
            addCriterion("date_voided >=", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedLessThan(String value) {
            addCriterion("date_voided <", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedLessThanOrEqualTo(String value) {
            addCriterion("date_voided <=", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedLike(String value) {
            addCriterion("date_voided like", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedNotLike(String value) {
            addCriterion("date_voided not like", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedIn(List<String> values) {
            addCriterion("date_voided in", values, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedNotIn(List<String> values) {
            addCriterion("date_voided not in", values, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedBetween(String value1, String value2) {
            addCriterion("date_voided between", value1, value2, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedNotBetween(String value1, String value2) {
            addCriterion("date_voided not between", value1, value2, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andVoidReasonIsNull() {
            addCriterion("void_reason is null");
            return (Criteria) this;
        }

        public Criteria andVoidReasonIsNotNull() {
            addCriterion("void_reason is not null");
            return (Criteria) this;
        }

        public Criteria andVoidReasonEqualTo(String value) {
            addCriterion("void_reason =", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonNotEqualTo(String value) {
            addCriterion("void_reason <>", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonGreaterThan(String value) {
            addCriterion("void_reason >", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonGreaterThanOrEqualTo(String value) {
            addCriterion("void_reason >=", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonLessThan(String value) {
            addCriterion("void_reason <", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonLessThanOrEqualTo(String value) {
            addCriterion("void_reason <=", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonLike(String value) {
            addCriterion("void_reason like", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonNotLike(String value) {
            addCriterion("void_reason not like", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonIn(List<String> values) {
            addCriterion("void_reason in", values, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonNotIn(List<String> values) {
            addCriterion("void_reason not in", values, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonBetween(String value1, String value2) {
            addCriterion("void_reason between", value1, value2, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonNotBetween(String value1, String value2) {
            addCriterion("void_reason not between", value1, value2, "voidReason");
            return (Criteria) this;
        }

        public Criteria andUuidIsNull() {
            addCriterion("uuid is null");
            return (Criteria) this;
        }

        public Criteria andUuidIsNotNull() {
            addCriterion("uuid is not null");
            return (Criteria) this;
        }

        public Criteria andUuidEqualTo(String value) {
            addCriterion("uuid =", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotEqualTo(String value) {
            addCriterion("uuid <>", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidGreaterThan(String value) {
            addCriterion("uuid >", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidGreaterThanOrEqualTo(String value) {
            addCriterion("uuid >=", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLessThan(String value) {
            addCriterion("uuid <", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLessThanOrEqualTo(String value) {
            addCriterion("uuid <=", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLike(String value) {
            addCriterion("uuid like", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotLike(String value) {
            addCriterion("uuid not like", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidIn(List<String> values) {
            addCriterion("uuid in", values, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotIn(List<String> values) {
            addCriterion("uuid not in", values, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidBetween(String value1, String value2) {
            addCriterion("uuid between", value1, value2, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotBetween(String value1, String value2) {
            addCriterion("uuid not between", value1, value2, "uuid");
            return (Criteria) this;
        }

        public Criteria andDeathdateEstimatedIsNull() {
            addCriterion("deathdate_estimated is null");
            return (Criteria) this;
        }

        public Criteria andDeathdateEstimatedIsNotNull() {
            addCriterion("deathdate_estimated is not null");
            return (Criteria) this;
        }

        public Criteria andDeathdateEstimatedEqualTo(String value) {
            addCriterion("deathdate_estimated =", value, "deathdateEstimated");
            return (Criteria) this;
        }

        public Criteria andDeathdateEstimatedNotEqualTo(String value) {
            addCriterion("deathdate_estimated <>", value, "deathdateEstimated");
            return (Criteria) this;
        }

        public Criteria andDeathdateEstimatedGreaterThan(String value) {
            addCriterion("deathdate_estimated >", value, "deathdateEstimated");
            return (Criteria) this;
        }

        public Criteria andDeathdateEstimatedGreaterThanOrEqualTo(String value) {
            addCriterion("deathdate_estimated >=", value, "deathdateEstimated");
            return (Criteria) this;
        }

        public Criteria andDeathdateEstimatedLessThan(String value) {
            addCriterion("deathdate_estimated <", value, "deathdateEstimated");
            return (Criteria) this;
        }

        public Criteria andDeathdateEstimatedLessThanOrEqualTo(String value) {
            addCriterion("deathdate_estimated <=", value, "deathdateEstimated");
            return (Criteria) this;
        }

        public Criteria andDeathdateEstimatedLike(String value) {
            addCriterion("deathdate_estimated like", value, "deathdateEstimated");
            return (Criteria) this;
        }

        public Criteria andDeathdateEstimatedNotLike(String value) {
            addCriterion("deathdate_estimated not like", value, "deathdateEstimated");
            return (Criteria) this;
        }

        public Criteria andDeathdateEstimatedIn(List<String> values) {
            addCriterion("deathdate_estimated in", values, "deathdateEstimated");
            return (Criteria) this;
        }

        public Criteria andDeathdateEstimatedNotIn(List<String> values) {
            addCriterion("deathdate_estimated not in", values, "deathdateEstimated");
            return (Criteria) this;
        }

        public Criteria andDeathdateEstimatedBetween(String value1, String value2) {
            addCriterion("deathdate_estimated between", value1, value2, "deathdateEstimated");
            return (Criteria) this;
        }

        public Criteria andDeathdateEstimatedNotBetween(String value1, String value2) {
            addCriterion("deathdate_estimated not between", value1, value2, "deathdateEstimated");
            return (Criteria) this;
        }

        public Criteria andBirthtimeIsNull() {
            addCriterion("birthtime is null");
            return (Criteria) this;
        }

        public Criteria andBirthtimeIsNotNull() {
            addCriterion("birthtime is not null");
            return (Criteria) this;
        }

        public Criteria andBirthtimeEqualTo(String value) {
            addCriterion("birthtime =", value, "birthtime");
            return (Criteria) this;
        }

        public Criteria andBirthtimeNotEqualTo(String value) {
            addCriterion("birthtime <>", value, "birthtime");
            return (Criteria) this;
        }

        public Criteria andBirthtimeGreaterThan(String value) {
            addCriterion("birthtime >", value, "birthtime");
            return (Criteria) this;
        }

        public Criteria andBirthtimeGreaterThanOrEqualTo(String value) {
            addCriterion("birthtime >=", value, "birthtime");
            return (Criteria) this;
        }

        public Criteria andBirthtimeLessThan(String value) {
            addCriterion("birthtime <", value, "birthtime");
            return (Criteria) this;
        }

        public Criteria andBirthtimeLessThanOrEqualTo(String value) {
            addCriterion("birthtime <=", value, "birthtime");
            return (Criteria) this;
        }

        public Criteria andBirthtimeLike(String value) {
            addCriterion("birthtime like", value, "birthtime");
            return (Criteria) this;
        }

        public Criteria andBirthtimeNotLike(String value) {
            addCriterion("birthtime not like", value, "birthtime");
            return (Criteria) this;
        }

        public Criteria andBirthtimeIn(List<String> values) {
            addCriterion("birthtime in", values, "birthtime");
            return (Criteria) this;
        }

        public Criteria andBirthtimeNotIn(List<String> values) {
            addCriterion("birthtime not in", values, "birthtime");
            return (Criteria) this;
        }

        public Criteria andBirthtimeBetween(String value1, String value2) {
            addCriterion("birthtime between", value1, value2, "birthtime");
            return (Criteria) this;
        }

        public Criteria andBirthtimeNotBetween(String value1, String value2) {
            addCriterion("birthtime not between", value1, value2, "birthtime");
            return (Criteria) this;
        }

        public Criteria andCauseOfDeathNonCodedIsNull() {
            addCriterion("cause_of_death_non_coded is null");
            return (Criteria) this;
        }

        public Criteria andCauseOfDeathNonCodedIsNotNull() {
            addCriterion("cause_of_death_non_coded is not null");
            return (Criteria) this;
        }

        public Criteria andCauseOfDeathNonCodedEqualTo(String value) {
            addCriterion("cause_of_death_non_coded =", value, "causeOfDeathNonCoded");
            return (Criteria) this;
        }

        public Criteria andCauseOfDeathNonCodedNotEqualTo(String value) {
            addCriterion("cause_of_death_non_coded <>", value, "causeOfDeathNonCoded");
            return (Criteria) this;
        }

        public Criteria andCauseOfDeathNonCodedGreaterThan(String value) {
            addCriterion("cause_of_death_non_coded >", value, "causeOfDeathNonCoded");
            return (Criteria) this;
        }

        public Criteria andCauseOfDeathNonCodedGreaterThanOrEqualTo(String value) {
            addCriterion("cause_of_death_non_coded >=", value, "causeOfDeathNonCoded");
            return (Criteria) this;
        }

        public Criteria andCauseOfDeathNonCodedLessThan(String value) {
            addCriterion("cause_of_death_non_coded <", value, "causeOfDeathNonCoded");
            return (Criteria) this;
        }

        public Criteria andCauseOfDeathNonCodedLessThanOrEqualTo(String value) {
            addCriterion("cause_of_death_non_coded <=", value, "causeOfDeathNonCoded");
            return (Criteria) this;
        }

        public Criteria andCauseOfDeathNonCodedLike(String value) {
            addCriterion("cause_of_death_non_coded like", value, "causeOfDeathNonCoded");
            return (Criteria) this;
        }

        public Criteria andCauseOfDeathNonCodedNotLike(String value) {
            addCriterion("cause_of_death_non_coded not like", value, "causeOfDeathNonCoded");
            return (Criteria) this;
        }

        public Criteria andCauseOfDeathNonCodedIn(List<String> values) {
            addCriterion("cause_of_death_non_coded in", values, "causeOfDeathNonCoded");
            return (Criteria) this;
        }

        public Criteria andCauseOfDeathNonCodedNotIn(List<String> values) {
            addCriterion("cause_of_death_non_coded not in", values, "causeOfDeathNonCoded");
            return (Criteria) this;
        }

        public Criteria andCauseOfDeathNonCodedBetween(String value1, String value2) {
            addCriterion("cause_of_death_non_coded between", value1, value2, "causeOfDeathNonCoded");
            return (Criteria) this;
        }

        public Criteria andCauseOfDeathNonCodedNotBetween(String value1, String value2) {
            addCriterion("cause_of_death_non_coded not between", value1, value2, "causeOfDeathNonCoded");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}