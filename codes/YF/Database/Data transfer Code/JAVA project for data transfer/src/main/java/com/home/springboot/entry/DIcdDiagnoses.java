package com.home.springboot.entry;

public class DIcdDiagnoses {
    private String rowId;

    private String icd9Code;

    private String shortTitle;

    private String longTitle;

    public String getRowId() {
        return rowId;
    }

    public void setRowId(String rowId) {
        this.rowId = rowId == null ? null : rowId.trim();
    }

    public String getIcd9Code() {
        return icd9Code;
    }

    public void setIcd9Code(String icd9Code) {
        this.icd9Code = icd9Code == null ? null : icd9Code.trim();
    }

    public String getShortTitle() {
        return shortTitle;
    }

    public void setShortTitle(String shortTitle) {
        this.shortTitle = shortTitle == null ? null : shortTitle.trim();
    }

    public String getLongTitle() {
        return longTitle;
    }

    public void setLongTitle(String longTitle) {
        this.longTitle = longTitle == null ? null : longTitle.trim();
    }
}