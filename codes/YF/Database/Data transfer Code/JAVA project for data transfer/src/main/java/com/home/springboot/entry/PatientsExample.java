package com.home.springboot.entry;

import java.util.ArrayList;
import java.util.List;

public class PatientsExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PatientsExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andRowIdIsNull() {
            addCriterion("ROW_ID is null");
            return (Criteria) this;
        }

        public Criteria andRowIdIsNotNull() {
            addCriterion("ROW_ID is not null");
            return (Criteria) this;
        }

        public Criteria andRowIdEqualTo(String value) {
            addCriterion("ROW_ID =", value, "rowId");
            return (Criteria) this;
        }

        public Criteria andRowIdNotEqualTo(String value) {
            addCriterion("ROW_ID <>", value, "rowId");
            return (Criteria) this;
        }

        public Criteria andRowIdGreaterThan(String value) {
            addCriterion("ROW_ID >", value, "rowId");
            return (Criteria) this;
        }

        public Criteria andRowIdGreaterThanOrEqualTo(String value) {
            addCriterion("ROW_ID >=", value, "rowId");
            return (Criteria) this;
        }

        public Criteria andRowIdLessThan(String value) {
            addCriterion("ROW_ID <", value, "rowId");
            return (Criteria) this;
        }

        public Criteria andRowIdLessThanOrEqualTo(String value) {
            addCriterion("ROW_ID <=", value, "rowId");
            return (Criteria) this;
        }

        public Criteria andRowIdLike(String value) {
            addCriterion("ROW_ID like", value, "rowId");
            return (Criteria) this;
        }

        public Criteria andRowIdNotLike(String value) {
            addCriterion("ROW_ID not like", value, "rowId");
            return (Criteria) this;
        }

        public Criteria andRowIdIn(List<String> values) {
            addCriterion("ROW_ID in", values, "rowId");
            return (Criteria) this;
        }

        public Criteria andRowIdNotIn(List<String> values) {
            addCriterion("ROW_ID not in", values, "rowId");
            return (Criteria) this;
        }

        public Criteria andRowIdBetween(String value1, String value2) {
            addCriterion("ROW_ID between", value1, value2, "rowId");
            return (Criteria) this;
        }

        public Criteria andRowIdNotBetween(String value1, String value2) {
            addCriterion("ROW_ID not between", value1, value2, "rowId");
            return (Criteria) this;
        }

        public Criteria andSubjectIdIsNull() {
            addCriterion("SUBJECT_ID is null");
            return (Criteria) this;
        }

        public Criteria andSubjectIdIsNotNull() {
            addCriterion("SUBJECT_ID is not null");
            return (Criteria) this;
        }

        public Criteria andSubjectIdEqualTo(String value) {
            addCriterion("SUBJECT_ID =", value, "subjectId");
            return (Criteria) this;
        }

        public Criteria andSubjectIdNotEqualTo(String value) {
            addCriterion("SUBJECT_ID <>", value, "subjectId");
            return (Criteria) this;
        }

        public Criteria andSubjectIdGreaterThan(String value) {
            addCriterion("SUBJECT_ID >", value, "subjectId");
            return (Criteria) this;
        }

        public Criteria andSubjectIdGreaterThanOrEqualTo(String value) {
            addCriterion("SUBJECT_ID >=", value, "subjectId");
            return (Criteria) this;
        }

        public Criteria andSubjectIdLessThan(String value) {
            addCriterion("SUBJECT_ID <", value, "subjectId");
            return (Criteria) this;
        }

        public Criteria andSubjectIdLessThanOrEqualTo(String value) {
            addCriterion("SUBJECT_ID <=", value, "subjectId");
            return (Criteria) this;
        }

        public Criteria andSubjectIdLike(String value) {
            addCriterion("SUBJECT_ID like", value, "subjectId");
            return (Criteria) this;
        }

        public Criteria andSubjectIdNotLike(String value) {
            addCriterion("SUBJECT_ID not like", value, "subjectId");
            return (Criteria) this;
        }

        public Criteria andSubjectIdIn(List<String> values) {
            addCriterion("SUBJECT_ID in", values, "subjectId");
            return (Criteria) this;
        }

        public Criteria andSubjectIdNotIn(List<String> values) {
            addCriterion("SUBJECT_ID not in", values, "subjectId");
            return (Criteria) this;
        }

        public Criteria andSubjectIdBetween(String value1, String value2) {
            addCriterion("SUBJECT_ID between", value1, value2, "subjectId");
            return (Criteria) this;
        }

        public Criteria andSubjectIdNotBetween(String value1, String value2) {
            addCriterion("SUBJECT_ID not between", value1, value2, "subjectId");
            return (Criteria) this;
        }

        public Criteria andGenderIsNull() {
            addCriterion("GENDER is null");
            return (Criteria) this;
        }

        public Criteria andGenderIsNotNull() {
            addCriterion("GENDER is not null");
            return (Criteria) this;
        }

        public Criteria andGenderEqualTo(String value) {
            addCriterion("GENDER =", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderNotEqualTo(String value) {
            addCriterion("GENDER <>", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderGreaterThan(String value) {
            addCriterion("GENDER >", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderGreaterThanOrEqualTo(String value) {
            addCriterion("GENDER >=", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderLessThan(String value) {
            addCriterion("GENDER <", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderLessThanOrEqualTo(String value) {
            addCriterion("GENDER <=", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderLike(String value) {
            addCriterion("GENDER like", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderNotLike(String value) {
            addCriterion("GENDER not like", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderIn(List<String> values) {
            addCriterion("GENDER in", values, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderNotIn(List<String> values) {
            addCriterion("GENDER not in", values, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderBetween(String value1, String value2) {
            addCriterion("GENDER between", value1, value2, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderNotBetween(String value1, String value2) {
            addCriterion("GENDER not between", value1, value2, "gender");
            return (Criteria) this;
        }

        public Criteria andDobIsNull() {
            addCriterion("DOB is null");
            return (Criteria) this;
        }

        public Criteria andDobIsNotNull() {
            addCriterion("DOB is not null");
            return (Criteria) this;
        }

        public Criteria andDobEqualTo(String value) {
            addCriterion("DOB =", value, "dob");
            return (Criteria) this;
        }

        public Criteria andDobNotEqualTo(String value) {
            addCriterion("DOB <>", value, "dob");
            return (Criteria) this;
        }

        public Criteria andDobGreaterThan(String value) {
            addCriterion("DOB >", value, "dob");
            return (Criteria) this;
        }

        public Criteria andDobGreaterThanOrEqualTo(String value) {
            addCriterion("DOB >=", value, "dob");
            return (Criteria) this;
        }

        public Criteria andDobLessThan(String value) {
            addCriterion("DOB <", value, "dob");
            return (Criteria) this;
        }

        public Criteria andDobLessThanOrEqualTo(String value) {
            addCriterion("DOB <=", value, "dob");
            return (Criteria) this;
        }

        public Criteria andDobLike(String value) {
            addCriterion("DOB like", value, "dob");
            return (Criteria) this;
        }

        public Criteria andDobNotLike(String value) {
            addCriterion("DOB not like", value, "dob");
            return (Criteria) this;
        }

        public Criteria andDobIn(List<String> values) {
            addCriterion("DOB in", values, "dob");
            return (Criteria) this;
        }

        public Criteria andDobNotIn(List<String> values) {
            addCriterion("DOB not in", values, "dob");
            return (Criteria) this;
        }

        public Criteria andDobBetween(String value1, String value2) {
            addCriterion("DOB between", value1, value2, "dob");
            return (Criteria) this;
        }

        public Criteria andDobNotBetween(String value1, String value2) {
            addCriterion("DOB not between", value1, value2, "dob");
            return (Criteria) this;
        }

        public Criteria andDodIsNull() {
            addCriterion("DOD is null");
            return (Criteria) this;
        }

        public Criteria andDodIsNotNull() {
            addCriterion("DOD is not null");
            return (Criteria) this;
        }

        public Criteria andDodEqualTo(String value) {
            addCriterion("DOD =", value, "dod");
            return (Criteria) this;
        }

        public Criteria andDodNotEqualTo(String value) {
            addCriterion("DOD <>", value, "dod");
            return (Criteria) this;
        }

        public Criteria andDodGreaterThan(String value) {
            addCriterion("DOD >", value, "dod");
            return (Criteria) this;
        }

        public Criteria andDodGreaterThanOrEqualTo(String value) {
            addCriterion("DOD >=", value, "dod");
            return (Criteria) this;
        }

        public Criteria andDodLessThan(String value) {
            addCriterion("DOD <", value, "dod");
            return (Criteria) this;
        }

        public Criteria andDodLessThanOrEqualTo(String value) {
            addCriterion("DOD <=", value, "dod");
            return (Criteria) this;
        }

        public Criteria andDodLike(String value) {
            addCriterion("DOD like", value, "dod");
            return (Criteria) this;
        }

        public Criteria andDodNotLike(String value) {
            addCriterion("DOD not like", value, "dod");
            return (Criteria) this;
        }

        public Criteria andDodIn(List<String> values) {
            addCriterion("DOD in", values, "dod");
            return (Criteria) this;
        }

        public Criteria andDodNotIn(List<String> values) {
            addCriterion("DOD not in", values, "dod");
            return (Criteria) this;
        }

        public Criteria andDodBetween(String value1, String value2) {
            addCriterion("DOD between", value1, value2, "dod");
            return (Criteria) this;
        }

        public Criteria andDodNotBetween(String value1, String value2) {
            addCriterion("DOD not between", value1, value2, "dod");
            return (Criteria) this;
        }

        public Criteria andDodHospIsNull() {
            addCriterion("DOD_HOSP is null");
            return (Criteria) this;
        }

        public Criteria andDodHospIsNotNull() {
            addCriterion("DOD_HOSP is not null");
            return (Criteria) this;
        }

        public Criteria andDodHospEqualTo(String value) {
            addCriterion("DOD_HOSP =", value, "dodHosp");
            return (Criteria) this;
        }

        public Criteria andDodHospNotEqualTo(String value) {
            addCriterion("DOD_HOSP <>", value, "dodHosp");
            return (Criteria) this;
        }

        public Criteria andDodHospGreaterThan(String value) {
            addCriterion("DOD_HOSP >", value, "dodHosp");
            return (Criteria) this;
        }

        public Criteria andDodHospGreaterThanOrEqualTo(String value) {
            addCriterion("DOD_HOSP >=", value, "dodHosp");
            return (Criteria) this;
        }

        public Criteria andDodHospLessThan(String value) {
            addCriterion("DOD_HOSP <", value, "dodHosp");
            return (Criteria) this;
        }

        public Criteria andDodHospLessThanOrEqualTo(String value) {
            addCriterion("DOD_HOSP <=", value, "dodHosp");
            return (Criteria) this;
        }

        public Criteria andDodHospLike(String value) {
            addCriterion("DOD_HOSP like", value, "dodHosp");
            return (Criteria) this;
        }

        public Criteria andDodHospNotLike(String value) {
            addCriterion("DOD_HOSP not like", value, "dodHosp");
            return (Criteria) this;
        }

        public Criteria andDodHospIn(List<String> values) {
            addCriterion("DOD_HOSP in", values, "dodHosp");
            return (Criteria) this;
        }

        public Criteria andDodHospNotIn(List<String> values) {
            addCriterion("DOD_HOSP not in", values, "dodHosp");
            return (Criteria) this;
        }

        public Criteria andDodHospBetween(String value1, String value2) {
            addCriterion("DOD_HOSP between", value1, value2, "dodHosp");
            return (Criteria) this;
        }

        public Criteria andDodHospNotBetween(String value1, String value2) {
            addCriterion("DOD_HOSP not between", value1, value2, "dodHosp");
            return (Criteria) this;
        }

        public Criteria andDodSsnIsNull() {
            addCriterion("DOD_SSN is null");
            return (Criteria) this;
        }

        public Criteria andDodSsnIsNotNull() {
            addCriterion("DOD_SSN is not null");
            return (Criteria) this;
        }

        public Criteria andDodSsnEqualTo(String value) {
            addCriterion("DOD_SSN =", value, "dodSsn");
            return (Criteria) this;
        }

        public Criteria andDodSsnNotEqualTo(String value) {
            addCriterion("DOD_SSN <>", value, "dodSsn");
            return (Criteria) this;
        }

        public Criteria andDodSsnGreaterThan(String value) {
            addCriterion("DOD_SSN >", value, "dodSsn");
            return (Criteria) this;
        }

        public Criteria andDodSsnGreaterThanOrEqualTo(String value) {
            addCriterion("DOD_SSN >=", value, "dodSsn");
            return (Criteria) this;
        }

        public Criteria andDodSsnLessThan(String value) {
            addCriterion("DOD_SSN <", value, "dodSsn");
            return (Criteria) this;
        }

        public Criteria andDodSsnLessThanOrEqualTo(String value) {
            addCriterion("DOD_SSN <=", value, "dodSsn");
            return (Criteria) this;
        }

        public Criteria andDodSsnLike(String value) {
            addCriterion("DOD_SSN like", value, "dodSsn");
            return (Criteria) this;
        }

        public Criteria andDodSsnNotLike(String value) {
            addCriterion("DOD_SSN not like", value, "dodSsn");
            return (Criteria) this;
        }

        public Criteria andDodSsnIn(List<String> values) {
            addCriterion("DOD_SSN in", values, "dodSsn");
            return (Criteria) this;
        }

        public Criteria andDodSsnNotIn(List<String> values) {
            addCriterion("DOD_SSN not in", values, "dodSsn");
            return (Criteria) this;
        }

        public Criteria andDodSsnBetween(String value1, String value2) {
            addCriterion("DOD_SSN between", value1, value2, "dodSsn");
            return (Criteria) this;
        }

        public Criteria andDodSsnNotBetween(String value1, String value2) {
            addCriterion("DOD_SSN not between", value1, value2, "dodSsn");
            return (Criteria) this;
        }

        public Criteria andExpireFlagIsNull() {
            addCriterion("EXPIRE_FLAG is null");
            return (Criteria) this;
        }

        public Criteria andExpireFlagIsNotNull() {
            addCriterion("EXPIRE_FLAG is not null");
            return (Criteria) this;
        }

        public Criteria andExpireFlagEqualTo(String value) {
            addCriterion("EXPIRE_FLAG =", value, "expireFlag");
            return (Criteria) this;
        }

        public Criteria andExpireFlagNotEqualTo(String value) {
            addCriterion("EXPIRE_FLAG <>", value, "expireFlag");
            return (Criteria) this;
        }

        public Criteria andExpireFlagGreaterThan(String value) {
            addCriterion("EXPIRE_FLAG >", value, "expireFlag");
            return (Criteria) this;
        }

        public Criteria andExpireFlagGreaterThanOrEqualTo(String value) {
            addCriterion("EXPIRE_FLAG >=", value, "expireFlag");
            return (Criteria) this;
        }

        public Criteria andExpireFlagLessThan(String value) {
            addCriterion("EXPIRE_FLAG <", value, "expireFlag");
            return (Criteria) this;
        }

        public Criteria andExpireFlagLessThanOrEqualTo(String value) {
            addCriterion("EXPIRE_FLAG <=", value, "expireFlag");
            return (Criteria) this;
        }

        public Criteria andExpireFlagLike(String value) {
            addCriterion("EXPIRE_FLAG like", value, "expireFlag");
            return (Criteria) this;
        }

        public Criteria andExpireFlagNotLike(String value) {
            addCriterion("EXPIRE_FLAG not like", value, "expireFlag");
            return (Criteria) this;
        }

        public Criteria andExpireFlagIn(List<String> values) {
            addCriterion("EXPIRE_FLAG in", values, "expireFlag");
            return (Criteria) this;
        }

        public Criteria andExpireFlagNotIn(List<String> values) {
            addCriterion("EXPIRE_FLAG not in", values, "expireFlag");
            return (Criteria) this;
        }

        public Criteria andExpireFlagBetween(String value1, String value2) {
            addCriterion("EXPIRE_FLAG between", value1, value2, "expireFlag");
            return (Criteria) this;
        }

        public Criteria andExpireFlagNotBetween(String value1, String value2) {
            addCriterion("EXPIRE_FLAG not between", value1, value2, "expireFlag");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}