package com.home.springboot.mapper;

import com.home.springboot.entry.Patients;
import com.home.springboot.entry.PatientsExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PatientsMapper {
    int countByExample(PatientsExample example);

    int deleteByExample(PatientsExample example);

    int insert(Patients record);

    int insertSelective(Patients record);

    List<Patients> selectByExample(PatientsExample example);

    int updateByExampleSelective(@Param("record") Patients record, @Param("example") PatientsExample example);

    int updateByExample(@Param("record") Patients record, @Param("example") PatientsExample example);

    Patients selectPatients(Integer count);
}