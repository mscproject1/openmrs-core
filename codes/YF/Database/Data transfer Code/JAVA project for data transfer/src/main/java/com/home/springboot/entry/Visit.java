package com.home.springboot.entry;

public class Visit {
    private String visitId;

    private String patientId;

    private String visitTypeId;

    private String dateStarted;

    private String dateStopped;

    private String indicationConceptId;

    private String locationId;

    private String creator;

    private String dateCreated;

    private String changedBy;

    private String dateChanged;

    private String voided;

    private String voidedBy;

    private String dateVoided;

    private String voidReason;

    private String uuid;

    public String getVisitId() {
        return visitId;
    }

    public void setVisitId(String visitId) {
        this.visitId = visitId == null ? null : visitId.trim();
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId == null ? null : patientId.trim();
    }

    public String getVisitTypeId() {
        return visitTypeId;
    }

    public void setVisitTypeId(String visitTypeId) {
        this.visitTypeId = visitTypeId == null ? null : visitTypeId.trim();
    }

    public String getDateStarted() {
        return dateStarted;
    }

    public void setDateStarted(String dateStarted) {
        this.dateStarted = dateStarted == null ? null : dateStarted.trim();
    }

    public String getDateStopped() {
        return dateStopped;
    }

    public void setDateStopped(String dateStopped) {
        this.dateStopped = dateStopped == null ? null : dateStopped.trim();
    }

    public String getIndicationConceptId() {
        return indicationConceptId;
    }

    public void setIndicationConceptId(String indicationConceptId) {
        this.indicationConceptId = indicationConceptId == null ? null : indicationConceptId.trim();
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId == null ? null : locationId.trim();
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated == null ? null : dateCreated.trim();
    }

    public String getChangedBy() {
        return changedBy;
    }

    public void setChangedBy(String changedBy) {
        this.changedBy = changedBy == null ? null : changedBy.trim();
    }

    public String getDateChanged() {
        return dateChanged;
    }

    public void setDateChanged(String dateChanged) {
        this.dateChanged = dateChanged == null ? null : dateChanged.trim();
    }

    public String getVoided() {
        return voided;
    }

    public void setVoided(String voided) {
        this.voided = voided == null ? null : voided.trim();
    }

    public String getVoidedBy() {
        return voidedBy;
    }

    public void setVoidedBy(String voidedBy) {
        this.voidedBy = voidedBy == null ? null : voidedBy.trim();
    }

    public String getDateVoided() {
        return dateVoided;
    }

    public void setDateVoided(String dateVoided) {
        this.dateVoided = dateVoided == null ? null : dateVoided.trim();
    }

    public String getVoidReason() {
        return voidReason;
    }

    public void setVoidReason(String voidReason) {
        this.voidReason = voidReason == null ? null : voidReason.trim();
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid == null ? null : uuid.trim();
    }
}