package com.home.springboot.service;

import com.home.springboot.entry.*;

public interface ObsService {
	//Query person data
	Person selectPerson(int count);

	// Modify person data
	int updatePerson(String personid, String deathdate, String personId);

	// Query data in the concept table
    Concept selectConcrpt(int count);

	//Querying patient data
	Patient selectPatient(int count);

	//Query obs data
	Obs selectObs(int count);

	//Modify obs data
	int updateObs(Obs obs);

	//concept data insert person
	int updateCtoP(String dateCreated, String personId);

	//Query admissions data
	Admissions selectAdmissions(int count);

	//Modify admissions data
	int updateByPrimaryKey(Admissions admissions);

	//Querying data in the patients table
	Patients selectPatients(int count);

	//Modify patient_id
	int updatePid(int pidnew, int pidold);

	//Modify person table data
	int updatePerson(String subjectId, String gender, String DOB, String DOD, String expireFlag, String personID);

	//Get d_icd_diagnoses table data
	DIcdDiagnoses selectDicddiagnoses(int count);

	//Get concept table data
	ConceptName selectConceptName(int count);

	//Query data from the conceptDescription table
	ConceptDescription selectConceptDescription(int count);

	//Modifying data in the concept table
	int updateConceptId(String Id, String id);

	//Modify data in the accept_name table
	int updateConceptName(String name, String id);

	//Modify data in the concept_description table
	int updateConceptDescription(String description, String id);

	//Querying visit table data
	Visit selectVisit(int count);

	//Modify visit table data
	int updateVisit(String patientId, String dateStarted, String dateStopped, String id);

}
