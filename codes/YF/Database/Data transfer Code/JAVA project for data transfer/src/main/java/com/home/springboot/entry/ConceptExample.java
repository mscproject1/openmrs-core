package com.home.springboot.entry;

import java.util.ArrayList;
import java.util.List;

public class ConceptExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ConceptExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andConceptIdIsNull() {
            addCriterion("concept_id is null");
            return (Criteria) this;
        }

        public Criteria andConceptIdIsNotNull() {
            addCriterion("concept_id is not null");
            return (Criteria) this;
        }

        public Criteria andConceptIdEqualTo(String value) {
            addCriterion("concept_id =", value, "conceptId");
            return (Criteria) this;
        }

        public Criteria andConceptIdNotEqualTo(String value) {
            addCriterion("concept_id <>", value, "conceptId");
            return (Criteria) this;
        }

        public Criteria andConceptIdGreaterThan(String value) {
            addCriterion("concept_id >", value, "conceptId");
            return (Criteria) this;
        }

        public Criteria andConceptIdGreaterThanOrEqualTo(String value) {
            addCriterion("concept_id >=", value, "conceptId");
            return (Criteria) this;
        }

        public Criteria andConceptIdLessThan(String value) {
            addCriterion("concept_id <", value, "conceptId");
            return (Criteria) this;
        }

        public Criteria andConceptIdLessThanOrEqualTo(String value) {
            addCriterion("concept_id <=", value, "conceptId");
            return (Criteria) this;
        }

        public Criteria andConceptIdLike(String value) {
            addCriterion("concept_id like", value, "conceptId");
            return (Criteria) this;
        }

        public Criteria andConceptIdNotLike(String value) {
            addCriterion("concept_id not like", value, "conceptId");
            return (Criteria) this;
        }

        public Criteria andConceptIdIn(List<String> values) {
            addCriterion("concept_id in", values, "conceptId");
            return (Criteria) this;
        }

        public Criteria andConceptIdNotIn(List<String> values) {
            addCriterion("concept_id not in", values, "conceptId");
            return (Criteria) this;
        }

        public Criteria andConceptIdBetween(String value1, String value2) {
            addCriterion("concept_id between", value1, value2, "conceptId");
            return (Criteria) this;
        }

        public Criteria andConceptIdNotBetween(String value1, String value2) {
            addCriterion("concept_id not between", value1, value2, "conceptId");
            return (Criteria) this;
        }

        public Criteria andRetiredIsNull() {
            addCriterion("retired is null");
            return (Criteria) this;
        }

        public Criteria andRetiredIsNotNull() {
            addCriterion("retired is not null");
            return (Criteria) this;
        }

        public Criteria andRetiredEqualTo(String value) {
            addCriterion("retired =", value, "retired");
            return (Criteria) this;
        }

        public Criteria andRetiredNotEqualTo(String value) {
            addCriterion("retired <>", value, "retired");
            return (Criteria) this;
        }

        public Criteria andRetiredGreaterThan(String value) {
            addCriterion("retired >", value, "retired");
            return (Criteria) this;
        }

        public Criteria andRetiredGreaterThanOrEqualTo(String value) {
            addCriterion("retired >=", value, "retired");
            return (Criteria) this;
        }

        public Criteria andRetiredLessThan(String value) {
            addCriterion("retired <", value, "retired");
            return (Criteria) this;
        }

        public Criteria andRetiredLessThanOrEqualTo(String value) {
            addCriterion("retired <=", value, "retired");
            return (Criteria) this;
        }

        public Criteria andRetiredLike(String value) {
            addCriterion("retired like", value, "retired");
            return (Criteria) this;
        }

        public Criteria andRetiredNotLike(String value) {
            addCriterion("retired not like", value, "retired");
            return (Criteria) this;
        }

        public Criteria andRetiredIn(List<String> values) {
            addCriterion("retired in", values, "retired");
            return (Criteria) this;
        }

        public Criteria andRetiredNotIn(List<String> values) {
            addCriterion("retired not in", values, "retired");
            return (Criteria) this;
        }

        public Criteria andRetiredBetween(String value1, String value2) {
            addCriterion("retired between", value1, value2, "retired");
            return (Criteria) this;
        }

        public Criteria andRetiredNotBetween(String value1, String value2) {
            addCriterion("retired not between", value1, value2, "retired");
            return (Criteria) this;
        }

        public Criteria andShortNameIsNull() {
            addCriterion("short_name is null");
            return (Criteria) this;
        }

        public Criteria andShortNameIsNotNull() {
            addCriterion("short_name is not null");
            return (Criteria) this;
        }

        public Criteria andShortNameEqualTo(String value) {
            addCriterion("short_name =", value, "shortName");
            return (Criteria) this;
        }

        public Criteria andShortNameNotEqualTo(String value) {
            addCriterion("short_name <>", value, "shortName");
            return (Criteria) this;
        }

        public Criteria andShortNameGreaterThan(String value) {
            addCriterion("short_name >", value, "shortName");
            return (Criteria) this;
        }

        public Criteria andShortNameGreaterThanOrEqualTo(String value) {
            addCriterion("short_name >=", value, "shortName");
            return (Criteria) this;
        }

        public Criteria andShortNameLessThan(String value) {
            addCriterion("short_name <", value, "shortName");
            return (Criteria) this;
        }

        public Criteria andShortNameLessThanOrEqualTo(String value) {
            addCriterion("short_name <=", value, "shortName");
            return (Criteria) this;
        }

        public Criteria andShortNameLike(String value) {
            addCriterion("short_name like", value, "shortName");
            return (Criteria) this;
        }

        public Criteria andShortNameNotLike(String value) {
            addCriterion("short_name not like", value, "shortName");
            return (Criteria) this;
        }

        public Criteria andShortNameIn(List<String> values) {
            addCriterion("short_name in", values, "shortName");
            return (Criteria) this;
        }

        public Criteria andShortNameNotIn(List<String> values) {
            addCriterion("short_name not in", values, "shortName");
            return (Criteria) this;
        }

        public Criteria andShortNameBetween(String value1, String value2) {
            addCriterion("short_name between", value1, value2, "shortName");
            return (Criteria) this;
        }

        public Criteria andShortNameNotBetween(String value1, String value2) {
            addCriterion("short_name not between", value1, value2, "shortName");
            return (Criteria) this;
        }

        public Criteria andDescriptionIsNull() {
            addCriterion("description is null");
            return (Criteria) this;
        }

        public Criteria andDescriptionIsNotNull() {
            addCriterion("description is not null");
            return (Criteria) this;
        }

        public Criteria andDescriptionEqualTo(String value) {
            addCriterion("description =", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotEqualTo(String value) {
            addCriterion("description <>", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionGreaterThan(String value) {
            addCriterion("description >", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionGreaterThanOrEqualTo(String value) {
            addCriterion("description >=", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLessThan(String value) {
            addCriterion("description <", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLessThanOrEqualTo(String value) {
            addCriterion("description <=", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLike(String value) {
            addCriterion("description like", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotLike(String value) {
            addCriterion("description not like", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionIn(List<String> values) {
            addCriterion("description in", values, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotIn(List<String> values) {
            addCriterion("description not in", values, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionBetween(String value1, String value2) {
            addCriterion("description between", value1, value2, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotBetween(String value1, String value2) {
            addCriterion("description not between", value1, value2, "description");
            return (Criteria) this;
        }

        public Criteria andFormTextIsNull() {
            addCriterion("form_text is null");
            return (Criteria) this;
        }

        public Criteria andFormTextIsNotNull() {
            addCriterion("form_text is not null");
            return (Criteria) this;
        }

        public Criteria andFormTextEqualTo(String value) {
            addCriterion("form_text =", value, "formText");
            return (Criteria) this;
        }

        public Criteria andFormTextNotEqualTo(String value) {
            addCriterion("form_text <>", value, "formText");
            return (Criteria) this;
        }

        public Criteria andFormTextGreaterThan(String value) {
            addCriterion("form_text >", value, "formText");
            return (Criteria) this;
        }

        public Criteria andFormTextGreaterThanOrEqualTo(String value) {
            addCriterion("form_text >=", value, "formText");
            return (Criteria) this;
        }

        public Criteria andFormTextLessThan(String value) {
            addCriterion("form_text <", value, "formText");
            return (Criteria) this;
        }

        public Criteria andFormTextLessThanOrEqualTo(String value) {
            addCriterion("form_text <=", value, "formText");
            return (Criteria) this;
        }

        public Criteria andFormTextLike(String value) {
            addCriterion("form_text like", value, "formText");
            return (Criteria) this;
        }

        public Criteria andFormTextNotLike(String value) {
            addCriterion("form_text not like", value, "formText");
            return (Criteria) this;
        }

        public Criteria andFormTextIn(List<String> values) {
            addCriterion("form_text in", values, "formText");
            return (Criteria) this;
        }

        public Criteria andFormTextNotIn(List<String> values) {
            addCriterion("form_text not in", values, "formText");
            return (Criteria) this;
        }

        public Criteria andFormTextBetween(String value1, String value2) {
            addCriterion("form_text between", value1, value2, "formText");
            return (Criteria) this;
        }

        public Criteria andFormTextNotBetween(String value1, String value2) {
            addCriterion("form_text not between", value1, value2, "formText");
            return (Criteria) this;
        }

        public Criteria andDatatypeIdIsNull() {
            addCriterion("datatype_id is null");
            return (Criteria) this;
        }

        public Criteria andDatatypeIdIsNotNull() {
            addCriterion("datatype_id is not null");
            return (Criteria) this;
        }

        public Criteria andDatatypeIdEqualTo(String value) {
            addCriterion("datatype_id =", value, "datatypeId");
            return (Criteria) this;
        }

        public Criteria andDatatypeIdNotEqualTo(String value) {
            addCriterion("datatype_id <>", value, "datatypeId");
            return (Criteria) this;
        }

        public Criteria andDatatypeIdGreaterThan(String value) {
            addCriterion("datatype_id >", value, "datatypeId");
            return (Criteria) this;
        }

        public Criteria andDatatypeIdGreaterThanOrEqualTo(String value) {
            addCriterion("datatype_id >=", value, "datatypeId");
            return (Criteria) this;
        }

        public Criteria andDatatypeIdLessThan(String value) {
            addCriterion("datatype_id <", value, "datatypeId");
            return (Criteria) this;
        }

        public Criteria andDatatypeIdLessThanOrEqualTo(String value) {
            addCriterion("datatype_id <=", value, "datatypeId");
            return (Criteria) this;
        }

        public Criteria andDatatypeIdLike(String value) {
            addCriterion("datatype_id like", value, "datatypeId");
            return (Criteria) this;
        }

        public Criteria andDatatypeIdNotLike(String value) {
            addCriterion("datatype_id not like", value, "datatypeId");
            return (Criteria) this;
        }

        public Criteria andDatatypeIdIn(List<String> values) {
            addCriterion("datatype_id in", values, "datatypeId");
            return (Criteria) this;
        }

        public Criteria andDatatypeIdNotIn(List<String> values) {
            addCriterion("datatype_id not in", values, "datatypeId");
            return (Criteria) this;
        }

        public Criteria andDatatypeIdBetween(String value1, String value2) {
            addCriterion("datatype_id between", value1, value2, "datatypeId");
            return (Criteria) this;
        }

        public Criteria andDatatypeIdNotBetween(String value1, String value2) {
            addCriterion("datatype_id not between", value1, value2, "datatypeId");
            return (Criteria) this;
        }

        public Criteria andClassIdIsNull() {
            addCriterion("class_id is null");
            return (Criteria) this;
        }

        public Criteria andClassIdIsNotNull() {
            addCriterion("class_id is not null");
            return (Criteria) this;
        }

        public Criteria andClassIdEqualTo(String value) {
            addCriterion("class_id =", value, "classId");
            return (Criteria) this;
        }

        public Criteria andClassIdNotEqualTo(String value) {
            addCriterion("class_id <>", value, "classId");
            return (Criteria) this;
        }

        public Criteria andClassIdGreaterThan(String value) {
            addCriterion("class_id >", value, "classId");
            return (Criteria) this;
        }

        public Criteria andClassIdGreaterThanOrEqualTo(String value) {
            addCriterion("class_id >=", value, "classId");
            return (Criteria) this;
        }

        public Criteria andClassIdLessThan(String value) {
            addCriterion("class_id <", value, "classId");
            return (Criteria) this;
        }

        public Criteria andClassIdLessThanOrEqualTo(String value) {
            addCriterion("class_id <=", value, "classId");
            return (Criteria) this;
        }

        public Criteria andClassIdLike(String value) {
            addCriterion("class_id like", value, "classId");
            return (Criteria) this;
        }

        public Criteria andClassIdNotLike(String value) {
            addCriterion("class_id not like", value, "classId");
            return (Criteria) this;
        }

        public Criteria andClassIdIn(List<String> values) {
            addCriterion("class_id in", values, "classId");
            return (Criteria) this;
        }

        public Criteria andClassIdNotIn(List<String> values) {
            addCriterion("class_id not in", values, "classId");
            return (Criteria) this;
        }

        public Criteria andClassIdBetween(String value1, String value2) {
            addCriterion("class_id between", value1, value2, "classId");
            return (Criteria) this;
        }

        public Criteria andClassIdNotBetween(String value1, String value2) {
            addCriterion("class_id not between", value1, value2, "classId");
            return (Criteria) this;
        }

        public Criteria andIsSetIsNull() {
            addCriterion("is_set is null");
            return (Criteria) this;
        }

        public Criteria andIsSetIsNotNull() {
            addCriterion("is_set is not null");
            return (Criteria) this;
        }

        public Criteria andIsSetEqualTo(String value) {
            addCriterion("is_set =", value, "isSet");
            return (Criteria) this;
        }

        public Criteria andIsSetNotEqualTo(String value) {
            addCriterion("is_set <>", value, "isSet");
            return (Criteria) this;
        }

        public Criteria andIsSetGreaterThan(String value) {
            addCriterion("is_set >", value, "isSet");
            return (Criteria) this;
        }

        public Criteria andIsSetGreaterThanOrEqualTo(String value) {
            addCriterion("is_set >=", value, "isSet");
            return (Criteria) this;
        }

        public Criteria andIsSetLessThan(String value) {
            addCriterion("is_set <", value, "isSet");
            return (Criteria) this;
        }

        public Criteria andIsSetLessThanOrEqualTo(String value) {
            addCriterion("is_set <=", value, "isSet");
            return (Criteria) this;
        }

        public Criteria andIsSetLike(String value) {
            addCriterion("is_set like", value, "isSet");
            return (Criteria) this;
        }

        public Criteria andIsSetNotLike(String value) {
            addCriterion("is_set not like", value, "isSet");
            return (Criteria) this;
        }

        public Criteria andIsSetIn(List<String> values) {
            addCriterion("is_set in", values, "isSet");
            return (Criteria) this;
        }

        public Criteria andIsSetNotIn(List<String> values) {
            addCriterion("is_set not in", values, "isSet");
            return (Criteria) this;
        }

        public Criteria andIsSetBetween(String value1, String value2) {
            addCriterion("is_set between", value1, value2, "isSet");
            return (Criteria) this;
        }

        public Criteria andIsSetNotBetween(String value1, String value2) {
            addCriterion("is_set not between", value1, value2, "isSet");
            return (Criteria) this;
        }

        public Criteria andCreatorIsNull() {
            addCriterion("creator is null");
            return (Criteria) this;
        }

        public Criteria andCreatorIsNotNull() {
            addCriterion("creator is not null");
            return (Criteria) this;
        }

        public Criteria andCreatorEqualTo(String value) {
            addCriterion("creator =", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotEqualTo(String value) {
            addCriterion("creator <>", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorGreaterThan(String value) {
            addCriterion("creator >", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorGreaterThanOrEqualTo(String value) {
            addCriterion("creator >=", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorLessThan(String value) {
            addCriterion("creator <", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorLessThanOrEqualTo(String value) {
            addCriterion("creator <=", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorLike(String value) {
            addCriterion("creator like", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotLike(String value) {
            addCriterion("creator not like", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorIn(List<String> values) {
            addCriterion("creator in", values, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotIn(List<String> values) {
            addCriterion("creator not in", values, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorBetween(String value1, String value2) {
            addCriterion("creator between", value1, value2, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotBetween(String value1, String value2) {
            addCriterion("creator not between", value1, value2, "creator");
            return (Criteria) this;
        }

        public Criteria andDateCreatedIsNull() {
            addCriterion("date_created is null");
            return (Criteria) this;
        }

        public Criteria andDateCreatedIsNotNull() {
            addCriterion("date_created is not null");
            return (Criteria) this;
        }

        public Criteria andDateCreatedEqualTo(String value) {
            addCriterion("date_created =", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedNotEqualTo(String value) {
            addCriterion("date_created <>", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedGreaterThan(String value) {
            addCriterion("date_created >", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedGreaterThanOrEqualTo(String value) {
            addCriterion("date_created >=", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedLessThan(String value) {
            addCriterion("date_created <", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedLessThanOrEqualTo(String value) {
            addCriterion("date_created <=", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedLike(String value) {
            addCriterion("date_created like", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedNotLike(String value) {
            addCriterion("date_created not like", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedIn(List<String> values) {
            addCriterion("date_created in", values, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedNotIn(List<String> values) {
            addCriterion("date_created not in", values, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedBetween(String value1, String value2) {
            addCriterion("date_created between", value1, value2, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedNotBetween(String value1, String value2) {
            addCriterion("date_created not between", value1, value2, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andVersionIsNull() {
            addCriterion("version is null");
            return (Criteria) this;
        }

        public Criteria andVersionIsNotNull() {
            addCriterion("version is not null");
            return (Criteria) this;
        }

        public Criteria andVersionEqualTo(String value) {
            addCriterion("version =", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionNotEqualTo(String value) {
            addCriterion("version <>", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionGreaterThan(String value) {
            addCriterion("version >", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionGreaterThanOrEqualTo(String value) {
            addCriterion("version >=", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionLessThan(String value) {
            addCriterion("version <", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionLessThanOrEqualTo(String value) {
            addCriterion("version <=", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionLike(String value) {
            addCriterion("version like", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionNotLike(String value) {
            addCriterion("version not like", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionIn(List<String> values) {
            addCriterion("version in", values, "version");
            return (Criteria) this;
        }

        public Criteria andVersionNotIn(List<String> values) {
            addCriterion("version not in", values, "version");
            return (Criteria) this;
        }

        public Criteria andVersionBetween(String value1, String value2) {
            addCriterion("version between", value1, value2, "version");
            return (Criteria) this;
        }

        public Criteria andVersionNotBetween(String value1, String value2) {
            addCriterion("version not between", value1, value2, "version");
            return (Criteria) this;
        }

        public Criteria andChangedByIsNull() {
            addCriterion("changed_by is null");
            return (Criteria) this;
        }

        public Criteria andChangedByIsNotNull() {
            addCriterion("changed_by is not null");
            return (Criteria) this;
        }

        public Criteria andChangedByEqualTo(String value) {
            addCriterion("changed_by =", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByNotEqualTo(String value) {
            addCriterion("changed_by <>", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByGreaterThan(String value) {
            addCriterion("changed_by >", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByGreaterThanOrEqualTo(String value) {
            addCriterion("changed_by >=", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByLessThan(String value) {
            addCriterion("changed_by <", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByLessThanOrEqualTo(String value) {
            addCriterion("changed_by <=", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByLike(String value) {
            addCriterion("changed_by like", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByNotLike(String value) {
            addCriterion("changed_by not like", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByIn(List<String> values) {
            addCriterion("changed_by in", values, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByNotIn(List<String> values) {
            addCriterion("changed_by not in", values, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByBetween(String value1, String value2) {
            addCriterion("changed_by between", value1, value2, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByNotBetween(String value1, String value2) {
            addCriterion("changed_by not between", value1, value2, "changedBy");
            return (Criteria) this;
        }

        public Criteria andDateChangedIsNull() {
            addCriterion("date_changed is null");
            return (Criteria) this;
        }

        public Criteria andDateChangedIsNotNull() {
            addCriterion("date_changed is not null");
            return (Criteria) this;
        }

        public Criteria andDateChangedEqualTo(String value) {
            addCriterion("date_changed =", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedNotEqualTo(String value) {
            addCriterion("date_changed <>", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedGreaterThan(String value) {
            addCriterion("date_changed >", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedGreaterThanOrEqualTo(String value) {
            addCriterion("date_changed >=", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedLessThan(String value) {
            addCriterion("date_changed <", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedLessThanOrEqualTo(String value) {
            addCriterion("date_changed <=", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedLike(String value) {
            addCriterion("date_changed like", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedNotLike(String value) {
            addCriterion("date_changed not like", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedIn(List<String> values) {
            addCriterion("date_changed in", values, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedNotIn(List<String> values) {
            addCriterion("date_changed not in", values, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedBetween(String value1, String value2) {
            addCriterion("date_changed between", value1, value2, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedNotBetween(String value1, String value2) {
            addCriterion("date_changed not between", value1, value2, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andRetiredByIsNull() {
            addCriterion("retired_by is null");
            return (Criteria) this;
        }

        public Criteria andRetiredByIsNotNull() {
            addCriterion("retired_by is not null");
            return (Criteria) this;
        }

        public Criteria andRetiredByEqualTo(String value) {
            addCriterion("retired_by =", value, "retiredBy");
            return (Criteria) this;
        }

        public Criteria andRetiredByNotEqualTo(String value) {
            addCriterion("retired_by <>", value, "retiredBy");
            return (Criteria) this;
        }

        public Criteria andRetiredByGreaterThan(String value) {
            addCriterion("retired_by >", value, "retiredBy");
            return (Criteria) this;
        }

        public Criteria andRetiredByGreaterThanOrEqualTo(String value) {
            addCriterion("retired_by >=", value, "retiredBy");
            return (Criteria) this;
        }

        public Criteria andRetiredByLessThan(String value) {
            addCriterion("retired_by <", value, "retiredBy");
            return (Criteria) this;
        }

        public Criteria andRetiredByLessThanOrEqualTo(String value) {
            addCriterion("retired_by <=", value, "retiredBy");
            return (Criteria) this;
        }

        public Criteria andRetiredByLike(String value) {
            addCriterion("retired_by like", value, "retiredBy");
            return (Criteria) this;
        }

        public Criteria andRetiredByNotLike(String value) {
            addCriterion("retired_by not like", value, "retiredBy");
            return (Criteria) this;
        }

        public Criteria andRetiredByIn(List<String> values) {
            addCriterion("retired_by in", values, "retiredBy");
            return (Criteria) this;
        }

        public Criteria andRetiredByNotIn(List<String> values) {
            addCriterion("retired_by not in", values, "retiredBy");
            return (Criteria) this;
        }

        public Criteria andRetiredByBetween(String value1, String value2) {
            addCriterion("retired_by between", value1, value2, "retiredBy");
            return (Criteria) this;
        }

        public Criteria andRetiredByNotBetween(String value1, String value2) {
            addCriterion("retired_by not between", value1, value2, "retiredBy");
            return (Criteria) this;
        }

        public Criteria andDateRetiredIsNull() {
            addCriterion("date_retired is null");
            return (Criteria) this;
        }

        public Criteria andDateRetiredIsNotNull() {
            addCriterion("date_retired is not null");
            return (Criteria) this;
        }

        public Criteria andDateRetiredEqualTo(String value) {
            addCriterion("date_retired =", value, "dateRetired");
            return (Criteria) this;
        }

        public Criteria andDateRetiredNotEqualTo(String value) {
            addCriterion("date_retired <>", value, "dateRetired");
            return (Criteria) this;
        }

        public Criteria andDateRetiredGreaterThan(String value) {
            addCriterion("date_retired >", value, "dateRetired");
            return (Criteria) this;
        }

        public Criteria andDateRetiredGreaterThanOrEqualTo(String value) {
            addCriterion("date_retired >=", value, "dateRetired");
            return (Criteria) this;
        }

        public Criteria andDateRetiredLessThan(String value) {
            addCriterion("date_retired <", value, "dateRetired");
            return (Criteria) this;
        }

        public Criteria andDateRetiredLessThanOrEqualTo(String value) {
            addCriterion("date_retired <=", value, "dateRetired");
            return (Criteria) this;
        }

        public Criteria andDateRetiredLike(String value) {
            addCriterion("date_retired like", value, "dateRetired");
            return (Criteria) this;
        }

        public Criteria andDateRetiredNotLike(String value) {
            addCriterion("date_retired not like", value, "dateRetired");
            return (Criteria) this;
        }

        public Criteria andDateRetiredIn(List<String> values) {
            addCriterion("date_retired in", values, "dateRetired");
            return (Criteria) this;
        }

        public Criteria andDateRetiredNotIn(List<String> values) {
            addCriterion("date_retired not in", values, "dateRetired");
            return (Criteria) this;
        }

        public Criteria andDateRetiredBetween(String value1, String value2) {
            addCriterion("date_retired between", value1, value2, "dateRetired");
            return (Criteria) this;
        }

        public Criteria andDateRetiredNotBetween(String value1, String value2) {
            addCriterion("date_retired not between", value1, value2, "dateRetired");
            return (Criteria) this;
        }

        public Criteria andRetireReasonIsNull() {
            addCriterion("retire_reason is null");
            return (Criteria) this;
        }

        public Criteria andRetireReasonIsNotNull() {
            addCriterion("retire_reason is not null");
            return (Criteria) this;
        }

        public Criteria andRetireReasonEqualTo(String value) {
            addCriterion("retire_reason =", value, "retireReason");
            return (Criteria) this;
        }

        public Criteria andRetireReasonNotEqualTo(String value) {
            addCriterion("retire_reason <>", value, "retireReason");
            return (Criteria) this;
        }

        public Criteria andRetireReasonGreaterThan(String value) {
            addCriterion("retire_reason >", value, "retireReason");
            return (Criteria) this;
        }

        public Criteria andRetireReasonGreaterThanOrEqualTo(String value) {
            addCriterion("retire_reason >=", value, "retireReason");
            return (Criteria) this;
        }

        public Criteria andRetireReasonLessThan(String value) {
            addCriterion("retire_reason <", value, "retireReason");
            return (Criteria) this;
        }

        public Criteria andRetireReasonLessThanOrEqualTo(String value) {
            addCriterion("retire_reason <=", value, "retireReason");
            return (Criteria) this;
        }

        public Criteria andRetireReasonLike(String value) {
            addCriterion("retire_reason like", value, "retireReason");
            return (Criteria) this;
        }

        public Criteria andRetireReasonNotLike(String value) {
            addCriterion("retire_reason not like", value, "retireReason");
            return (Criteria) this;
        }

        public Criteria andRetireReasonIn(List<String> values) {
            addCriterion("retire_reason in", values, "retireReason");
            return (Criteria) this;
        }

        public Criteria andRetireReasonNotIn(List<String> values) {
            addCriterion("retire_reason not in", values, "retireReason");
            return (Criteria) this;
        }

        public Criteria andRetireReasonBetween(String value1, String value2) {
            addCriterion("retire_reason between", value1, value2, "retireReason");
            return (Criteria) this;
        }

        public Criteria andRetireReasonNotBetween(String value1, String value2) {
            addCriterion("retire_reason not between", value1, value2, "retireReason");
            return (Criteria) this;
        }

        public Criteria andUuidIsNull() {
            addCriterion("uuid is null");
            return (Criteria) this;
        }

        public Criteria andUuidIsNotNull() {
            addCriterion("uuid is not null");
            return (Criteria) this;
        }

        public Criteria andUuidEqualTo(String value) {
            addCriterion("uuid =", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotEqualTo(String value) {
            addCriterion("uuid <>", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidGreaterThan(String value) {
            addCriterion("uuid >", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidGreaterThanOrEqualTo(String value) {
            addCriterion("uuid >=", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLessThan(String value) {
            addCriterion("uuid <", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLessThanOrEqualTo(String value) {
            addCriterion("uuid <=", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLike(String value) {
            addCriterion("uuid like", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotLike(String value) {
            addCriterion("uuid not like", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidIn(List<String> values) {
            addCriterion("uuid in", values, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotIn(List<String> values) {
            addCriterion("uuid not in", values, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidBetween(String value1, String value2) {
            addCriterion("uuid between", value1, value2, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotBetween(String value1, String value2) {
            addCriterion("uuid not between", value1, value2, "uuid");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}