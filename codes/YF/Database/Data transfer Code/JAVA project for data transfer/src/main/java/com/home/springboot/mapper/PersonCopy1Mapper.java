package com.home.springboot.mapper;

import com.home.springboot.entry.PersonCopy1;
import com.home.springboot.entry.PersonCopy1Example;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PersonCopy1Mapper {
    int countByExample(PersonCopy1Example example);

    int deleteByExample(PersonCopy1Example example);

    int deleteByPrimaryKey(Integer personId);

    int insert(PersonCopy1 record);

    int insertSelective(PersonCopy1 record);

    List<PersonCopy1> selectByExample(PersonCopy1Example example);

    PersonCopy1 selectByPrimaryKey(Integer personId);

    int updateByExampleSelective(@Param("record") PersonCopy1 record, @Param("example") PersonCopy1Example example);

    int updateByExample(@Param("record") PersonCopy1 record, @Param("example") PersonCopy1Example example);

    int updateByPrimaryKeySelective(PersonCopy1 record);

    int updateByPrimaryKey(PersonCopy1 record);
}