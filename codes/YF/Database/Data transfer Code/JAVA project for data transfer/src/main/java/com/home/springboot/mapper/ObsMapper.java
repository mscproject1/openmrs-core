package com.home.springboot.mapper;

import com.home.springboot.entry.Obs;
import com.home.springboot.entry.ObsExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ObsMapper {
    int countByExample(ObsExample example);

    int deleteByExample(ObsExample example);

    int deleteByPrimaryKey(Integer obsId);

    int insert(Obs record);

    int insertSelective(Obs record);

    List<Obs> selectByExampleWithBLOBs(ObsExample example);

    List<Obs> selectByExample(ObsExample example);

    Obs selectByPrimaryKey(Integer obsId);

    int updateByExampleSelective(@Param("record") Obs record, @Param("example") ObsExample example);

    int updateByExampleWithBLOBs(@Param("record") Obs record, @Param("example") ObsExample example);

    int updateByExample(@Param("record") Obs record, @Param("example") ObsExample example);

    int updateByPrimaryKeySelective(Obs record);

    int updateByPrimaryKeyWithBLOBs(Obs record);

    int updateByPrimaryKey(Obs record);

    Obs selectObs(Integer count);
}