package com.home.springboot.entry;

import java.util.ArrayList;
import java.util.List;

public class ConceptNameExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ConceptNameExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andConceptNameIdIsNull() {
            addCriterion("concept_name_id is null");
            return (Criteria) this;
        }

        public Criteria andConceptNameIdIsNotNull() {
            addCriterion("concept_name_id is not null");
            return (Criteria) this;
        }

        public Criteria andConceptNameIdEqualTo(String value) {
            addCriterion("concept_name_id =", value, "conceptNameId");
            return (Criteria) this;
        }

        public Criteria andConceptNameIdNotEqualTo(String value) {
            addCriterion("concept_name_id <>", value, "conceptNameId");
            return (Criteria) this;
        }

        public Criteria andConceptNameIdGreaterThan(String value) {
            addCriterion("concept_name_id >", value, "conceptNameId");
            return (Criteria) this;
        }

        public Criteria andConceptNameIdGreaterThanOrEqualTo(String value) {
            addCriterion("concept_name_id >=", value, "conceptNameId");
            return (Criteria) this;
        }

        public Criteria andConceptNameIdLessThan(String value) {
            addCriterion("concept_name_id <", value, "conceptNameId");
            return (Criteria) this;
        }

        public Criteria andConceptNameIdLessThanOrEqualTo(String value) {
            addCriterion("concept_name_id <=", value, "conceptNameId");
            return (Criteria) this;
        }

        public Criteria andConceptNameIdLike(String value) {
            addCriterion("concept_name_id like", value, "conceptNameId");
            return (Criteria) this;
        }

        public Criteria andConceptNameIdNotLike(String value) {
            addCriterion("concept_name_id not like", value, "conceptNameId");
            return (Criteria) this;
        }

        public Criteria andConceptNameIdIn(List<String> values) {
            addCriterion("concept_name_id in", values, "conceptNameId");
            return (Criteria) this;
        }

        public Criteria andConceptNameIdNotIn(List<String> values) {
            addCriterion("concept_name_id not in", values, "conceptNameId");
            return (Criteria) this;
        }

        public Criteria andConceptNameIdBetween(String value1, String value2) {
            addCriterion("concept_name_id between", value1, value2, "conceptNameId");
            return (Criteria) this;
        }

        public Criteria andConceptNameIdNotBetween(String value1, String value2) {
            addCriterion("concept_name_id not between", value1, value2, "conceptNameId");
            return (Criteria) this;
        }

        public Criteria andConceptIdIsNull() {
            addCriterion("concept_id is null");
            return (Criteria) this;
        }

        public Criteria andConceptIdIsNotNull() {
            addCriterion("concept_id is not null");
            return (Criteria) this;
        }

        public Criteria andConceptIdEqualTo(String value) {
            addCriterion("concept_id =", value, "conceptId");
            return (Criteria) this;
        }

        public Criteria andConceptIdNotEqualTo(String value) {
            addCriterion("concept_id <>", value, "conceptId");
            return (Criteria) this;
        }

        public Criteria andConceptIdGreaterThan(String value) {
            addCriterion("concept_id >", value, "conceptId");
            return (Criteria) this;
        }

        public Criteria andConceptIdGreaterThanOrEqualTo(String value) {
            addCriterion("concept_id >=", value, "conceptId");
            return (Criteria) this;
        }

        public Criteria andConceptIdLessThan(String value) {
            addCriterion("concept_id <", value, "conceptId");
            return (Criteria) this;
        }

        public Criteria andConceptIdLessThanOrEqualTo(String value) {
            addCriterion("concept_id <=", value, "conceptId");
            return (Criteria) this;
        }

        public Criteria andConceptIdLike(String value) {
            addCriterion("concept_id like", value, "conceptId");
            return (Criteria) this;
        }

        public Criteria andConceptIdNotLike(String value) {
            addCriterion("concept_id not like", value, "conceptId");
            return (Criteria) this;
        }

        public Criteria andConceptIdIn(List<String> values) {
            addCriterion("concept_id in", values, "conceptId");
            return (Criteria) this;
        }

        public Criteria andConceptIdNotIn(List<String> values) {
            addCriterion("concept_id not in", values, "conceptId");
            return (Criteria) this;
        }

        public Criteria andConceptIdBetween(String value1, String value2) {
            addCriterion("concept_id between", value1, value2, "conceptId");
            return (Criteria) this;
        }

        public Criteria andConceptIdNotBetween(String value1, String value2) {
            addCriterion("concept_id not between", value1, value2, "conceptId");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andLocaleIsNull() {
            addCriterion("locale is null");
            return (Criteria) this;
        }

        public Criteria andLocaleIsNotNull() {
            addCriterion("locale is not null");
            return (Criteria) this;
        }

        public Criteria andLocaleEqualTo(String value) {
            addCriterion("locale =", value, "locale");
            return (Criteria) this;
        }

        public Criteria andLocaleNotEqualTo(String value) {
            addCriterion("locale <>", value, "locale");
            return (Criteria) this;
        }

        public Criteria andLocaleGreaterThan(String value) {
            addCriterion("locale >", value, "locale");
            return (Criteria) this;
        }

        public Criteria andLocaleGreaterThanOrEqualTo(String value) {
            addCriterion("locale >=", value, "locale");
            return (Criteria) this;
        }

        public Criteria andLocaleLessThan(String value) {
            addCriterion("locale <", value, "locale");
            return (Criteria) this;
        }

        public Criteria andLocaleLessThanOrEqualTo(String value) {
            addCriterion("locale <=", value, "locale");
            return (Criteria) this;
        }

        public Criteria andLocaleLike(String value) {
            addCriterion("locale like", value, "locale");
            return (Criteria) this;
        }

        public Criteria andLocaleNotLike(String value) {
            addCriterion("locale not like", value, "locale");
            return (Criteria) this;
        }

        public Criteria andLocaleIn(List<String> values) {
            addCriterion("locale in", values, "locale");
            return (Criteria) this;
        }

        public Criteria andLocaleNotIn(List<String> values) {
            addCriterion("locale not in", values, "locale");
            return (Criteria) this;
        }

        public Criteria andLocaleBetween(String value1, String value2) {
            addCriterion("locale between", value1, value2, "locale");
            return (Criteria) this;
        }

        public Criteria andLocaleNotBetween(String value1, String value2) {
            addCriterion("locale not between", value1, value2, "locale");
            return (Criteria) this;
        }

        public Criteria andLocalePreferredIsNull() {
            addCriterion("locale_preferred is null");
            return (Criteria) this;
        }

        public Criteria andLocalePreferredIsNotNull() {
            addCriterion("locale_preferred is not null");
            return (Criteria) this;
        }

        public Criteria andLocalePreferredEqualTo(String value) {
            addCriterion("locale_preferred =", value, "localePreferred");
            return (Criteria) this;
        }

        public Criteria andLocalePreferredNotEqualTo(String value) {
            addCriterion("locale_preferred <>", value, "localePreferred");
            return (Criteria) this;
        }

        public Criteria andLocalePreferredGreaterThan(String value) {
            addCriterion("locale_preferred >", value, "localePreferred");
            return (Criteria) this;
        }

        public Criteria andLocalePreferredGreaterThanOrEqualTo(String value) {
            addCriterion("locale_preferred >=", value, "localePreferred");
            return (Criteria) this;
        }

        public Criteria andLocalePreferredLessThan(String value) {
            addCriterion("locale_preferred <", value, "localePreferred");
            return (Criteria) this;
        }

        public Criteria andLocalePreferredLessThanOrEqualTo(String value) {
            addCriterion("locale_preferred <=", value, "localePreferred");
            return (Criteria) this;
        }

        public Criteria andLocalePreferredLike(String value) {
            addCriterion("locale_preferred like", value, "localePreferred");
            return (Criteria) this;
        }

        public Criteria andLocalePreferredNotLike(String value) {
            addCriterion("locale_preferred not like", value, "localePreferred");
            return (Criteria) this;
        }

        public Criteria andLocalePreferredIn(List<String> values) {
            addCriterion("locale_preferred in", values, "localePreferred");
            return (Criteria) this;
        }

        public Criteria andLocalePreferredNotIn(List<String> values) {
            addCriterion("locale_preferred not in", values, "localePreferred");
            return (Criteria) this;
        }

        public Criteria andLocalePreferredBetween(String value1, String value2) {
            addCriterion("locale_preferred between", value1, value2, "localePreferred");
            return (Criteria) this;
        }

        public Criteria andLocalePreferredNotBetween(String value1, String value2) {
            addCriterion("locale_preferred not between", value1, value2, "localePreferred");
            return (Criteria) this;
        }

        public Criteria andCreatorIsNull() {
            addCriterion("creator is null");
            return (Criteria) this;
        }

        public Criteria andCreatorIsNotNull() {
            addCriterion("creator is not null");
            return (Criteria) this;
        }

        public Criteria andCreatorEqualTo(String value) {
            addCriterion("creator =", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotEqualTo(String value) {
            addCriterion("creator <>", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorGreaterThan(String value) {
            addCriterion("creator >", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorGreaterThanOrEqualTo(String value) {
            addCriterion("creator >=", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorLessThan(String value) {
            addCriterion("creator <", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorLessThanOrEqualTo(String value) {
            addCriterion("creator <=", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorLike(String value) {
            addCriterion("creator like", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotLike(String value) {
            addCriterion("creator not like", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorIn(List<String> values) {
            addCriterion("creator in", values, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotIn(List<String> values) {
            addCriterion("creator not in", values, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorBetween(String value1, String value2) {
            addCriterion("creator between", value1, value2, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotBetween(String value1, String value2) {
            addCriterion("creator not between", value1, value2, "creator");
            return (Criteria) this;
        }

        public Criteria andDateCreatedIsNull() {
            addCriterion("date_created is null");
            return (Criteria) this;
        }

        public Criteria andDateCreatedIsNotNull() {
            addCriterion("date_created is not null");
            return (Criteria) this;
        }

        public Criteria andDateCreatedEqualTo(String value) {
            addCriterion("date_created =", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedNotEqualTo(String value) {
            addCriterion("date_created <>", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedGreaterThan(String value) {
            addCriterion("date_created >", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedGreaterThanOrEqualTo(String value) {
            addCriterion("date_created >=", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedLessThan(String value) {
            addCriterion("date_created <", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedLessThanOrEqualTo(String value) {
            addCriterion("date_created <=", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedLike(String value) {
            addCriterion("date_created like", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedNotLike(String value) {
            addCriterion("date_created not like", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedIn(List<String> values) {
            addCriterion("date_created in", values, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedNotIn(List<String> values) {
            addCriterion("date_created not in", values, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedBetween(String value1, String value2) {
            addCriterion("date_created between", value1, value2, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedNotBetween(String value1, String value2) {
            addCriterion("date_created not between", value1, value2, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andConceptNameTypeIsNull() {
            addCriterion("concept_name_type is null");
            return (Criteria) this;
        }

        public Criteria andConceptNameTypeIsNotNull() {
            addCriterion("concept_name_type is not null");
            return (Criteria) this;
        }

        public Criteria andConceptNameTypeEqualTo(String value) {
            addCriterion("concept_name_type =", value, "conceptNameType");
            return (Criteria) this;
        }

        public Criteria andConceptNameTypeNotEqualTo(String value) {
            addCriterion("concept_name_type <>", value, "conceptNameType");
            return (Criteria) this;
        }

        public Criteria andConceptNameTypeGreaterThan(String value) {
            addCriterion("concept_name_type >", value, "conceptNameType");
            return (Criteria) this;
        }

        public Criteria andConceptNameTypeGreaterThanOrEqualTo(String value) {
            addCriterion("concept_name_type >=", value, "conceptNameType");
            return (Criteria) this;
        }

        public Criteria andConceptNameTypeLessThan(String value) {
            addCriterion("concept_name_type <", value, "conceptNameType");
            return (Criteria) this;
        }

        public Criteria andConceptNameTypeLessThanOrEqualTo(String value) {
            addCriterion("concept_name_type <=", value, "conceptNameType");
            return (Criteria) this;
        }

        public Criteria andConceptNameTypeLike(String value) {
            addCriterion("concept_name_type like", value, "conceptNameType");
            return (Criteria) this;
        }

        public Criteria andConceptNameTypeNotLike(String value) {
            addCriterion("concept_name_type not like", value, "conceptNameType");
            return (Criteria) this;
        }

        public Criteria andConceptNameTypeIn(List<String> values) {
            addCriterion("concept_name_type in", values, "conceptNameType");
            return (Criteria) this;
        }

        public Criteria andConceptNameTypeNotIn(List<String> values) {
            addCriterion("concept_name_type not in", values, "conceptNameType");
            return (Criteria) this;
        }

        public Criteria andConceptNameTypeBetween(String value1, String value2) {
            addCriterion("concept_name_type between", value1, value2, "conceptNameType");
            return (Criteria) this;
        }

        public Criteria andConceptNameTypeNotBetween(String value1, String value2) {
            addCriterion("concept_name_type not between", value1, value2, "conceptNameType");
            return (Criteria) this;
        }

        public Criteria andVoidedIsNull() {
            addCriterion("voided is null");
            return (Criteria) this;
        }

        public Criteria andVoidedIsNotNull() {
            addCriterion("voided is not null");
            return (Criteria) this;
        }

        public Criteria andVoidedEqualTo(String value) {
            addCriterion("voided =", value, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedNotEqualTo(String value) {
            addCriterion("voided <>", value, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedGreaterThan(String value) {
            addCriterion("voided >", value, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedGreaterThanOrEqualTo(String value) {
            addCriterion("voided >=", value, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedLessThan(String value) {
            addCriterion("voided <", value, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedLessThanOrEqualTo(String value) {
            addCriterion("voided <=", value, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedLike(String value) {
            addCriterion("voided like", value, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedNotLike(String value) {
            addCriterion("voided not like", value, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedIn(List<String> values) {
            addCriterion("voided in", values, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedNotIn(List<String> values) {
            addCriterion("voided not in", values, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedBetween(String value1, String value2) {
            addCriterion("voided between", value1, value2, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedNotBetween(String value1, String value2) {
            addCriterion("voided not between", value1, value2, "voided");
            return (Criteria) this;
        }

        public Criteria andVoidedByIsNull() {
            addCriterion("voided_by is null");
            return (Criteria) this;
        }

        public Criteria andVoidedByIsNotNull() {
            addCriterion("voided_by is not null");
            return (Criteria) this;
        }

        public Criteria andVoidedByEqualTo(String value) {
            addCriterion("voided_by =", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByNotEqualTo(String value) {
            addCriterion("voided_by <>", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByGreaterThan(String value) {
            addCriterion("voided_by >", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByGreaterThanOrEqualTo(String value) {
            addCriterion("voided_by >=", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByLessThan(String value) {
            addCriterion("voided_by <", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByLessThanOrEqualTo(String value) {
            addCriterion("voided_by <=", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByLike(String value) {
            addCriterion("voided_by like", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByNotLike(String value) {
            addCriterion("voided_by not like", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByIn(List<String> values) {
            addCriterion("voided_by in", values, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByNotIn(List<String> values) {
            addCriterion("voided_by not in", values, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByBetween(String value1, String value2) {
            addCriterion("voided_by between", value1, value2, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByNotBetween(String value1, String value2) {
            addCriterion("voided_by not between", value1, value2, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andDateVoidedIsNull() {
            addCriterion("date_voided is null");
            return (Criteria) this;
        }

        public Criteria andDateVoidedIsNotNull() {
            addCriterion("date_voided is not null");
            return (Criteria) this;
        }

        public Criteria andDateVoidedEqualTo(String value) {
            addCriterion("date_voided =", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedNotEqualTo(String value) {
            addCriterion("date_voided <>", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedGreaterThan(String value) {
            addCriterion("date_voided >", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedGreaterThanOrEqualTo(String value) {
            addCriterion("date_voided >=", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedLessThan(String value) {
            addCriterion("date_voided <", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedLessThanOrEqualTo(String value) {
            addCriterion("date_voided <=", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedLike(String value) {
            addCriterion("date_voided like", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedNotLike(String value) {
            addCriterion("date_voided not like", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedIn(List<String> values) {
            addCriterion("date_voided in", values, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedNotIn(List<String> values) {
            addCriterion("date_voided not in", values, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedBetween(String value1, String value2) {
            addCriterion("date_voided between", value1, value2, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedNotBetween(String value1, String value2) {
            addCriterion("date_voided not between", value1, value2, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andVoidReasonIsNull() {
            addCriterion("void_reason is null");
            return (Criteria) this;
        }

        public Criteria andVoidReasonIsNotNull() {
            addCriterion("void_reason is not null");
            return (Criteria) this;
        }

        public Criteria andVoidReasonEqualTo(String value) {
            addCriterion("void_reason =", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonNotEqualTo(String value) {
            addCriterion("void_reason <>", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonGreaterThan(String value) {
            addCriterion("void_reason >", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonGreaterThanOrEqualTo(String value) {
            addCriterion("void_reason >=", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonLessThan(String value) {
            addCriterion("void_reason <", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonLessThanOrEqualTo(String value) {
            addCriterion("void_reason <=", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonLike(String value) {
            addCriterion("void_reason like", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonNotLike(String value) {
            addCriterion("void_reason not like", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonIn(List<String> values) {
            addCriterion("void_reason in", values, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonNotIn(List<String> values) {
            addCriterion("void_reason not in", values, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonBetween(String value1, String value2) {
            addCriterion("void_reason between", value1, value2, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonNotBetween(String value1, String value2) {
            addCriterion("void_reason not between", value1, value2, "voidReason");
            return (Criteria) this;
        }

        public Criteria andUuidIsNull() {
            addCriterion("uuid is null");
            return (Criteria) this;
        }

        public Criteria andUuidIsNotNull() {
            addCriterion("uuid is not null");
            return (Criteria) this;
        }

        public Criteria andUuidEqualTo(String value) {
            addCriterion("uuid =", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotEqualTo(String value) {
            addCriterion("uuid <>", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidGreaterThan(String value) {
            addCriterion("uuid >", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidGreaterThanOrEqualTo(String value) {
            addCriterion("uuid >=", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLessThan(String value) {
            addCriterion("uuid <", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLessThanOrEqualTo(String value) {
            addCriterion("uuid <=", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLike(String value) {
            addCriterion("uuid like", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotLike(String value) {
            addCriterion("uuid not like", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidIn(List<String> values) {
            addCriterion("uuid in", values, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotIn(List<String> values) {
            addCriterion("uuid not in", values, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidBetween(String value1, String value2) {
            addCriterion("uuid between", value1, value2, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotBetween(String value1, String value2) {
            addCriterion("uuid not between", value1, value2, "uuid");
            return (Criteria) this;
        }

        public Criteria andDateChangedIsNull() {
            addCriterion("date_changed is null");
            return (Criteria) this;
        }

        public Criteria andDateChangedIsNotNull() {
            addCriterion("date_changed is not null");
            return (Criteria) this;
        }

        public Criteria andDateChangedEqualTo(String value) {
            addCriterion("date_changed =", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedNotEqualTo(String value) {
            addCriterion("date_changed <>", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedGreaterThan(String value) {
            addCriterion("date_changed >", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedGreaterThanOrEqualTo(String value) {
            addCriterion("date_changed >=", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedLessThan(String value) {
            addCriterion("date_changed <", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedLessThanOrEqualTo(String value) {
            addCriterion("date_changed <=", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedLike(String value) {
            addCriterion("date_changed like", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedNotLike(String value) {
            addCriterion("date_changed not like", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedIn(List<String> values) {
            addCriterion("date_changed in", values, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedNotIn(List<String> values) {
            addCriterion("date_changed not in", values, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedBetween(String value1, String value2) {
            addCriterion("date_changed between", value1, value2, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedNotBetween(String value1, String value2) {
            addCriterion("date_changed not between", value1, value2, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andChangedByIsNull() {
            addCriterion("changed_by is null");
            return (Criteria) this;
        }

        public Criteria andChangedByIsNotNull() {
            addCriterion("changed_by is not null");
            return (Criteria) this;
        }

        public Criteria andChangedByEqualTo(String value) {
            addCriterion("changed_by =", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByNotEqualTo(String value) {
            addCriterion("changed_by <>", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByGreaterThan(String value) {
            addCriterion("changed_by >", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByGreaterThanOrEqualTo(String value) {
            addCriterion("changed_by >=", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByLessThan(String value) {
            addCriterion("changed_by <", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByLessThanOrEqualTo(String value) {
            addCriterion("changed_by <=", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByLike(String value) {
            addCriterion("changed_by like", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByNotLike(String value) {
            addCriterion("changed_by not like", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByIn(List<String> values) {
            addCriterion("changed_by in", values, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByNotIn(List<String> values) {
            addCriterion("changed_by not in", values, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByBetween(String value1, String value2) {
            addCriterion("changed_by between", value1, value2, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByNotBetween(String value1, String value2) {
            addCriterion("changed_by not between", value1, value2, "changedBy");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}