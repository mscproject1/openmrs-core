package com.home.springboot.convert;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class MyCovert implements Converter<String, Date>{
		Date d = null;
	@Override
	public Date convert(String arg0) {
		DateFormat df= new SimpleDateFormat("yyyy-MM-dd");
		try {
			if(arg0==null||arg0==""){
				return null;
			}
			d=df.parse(arg0);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return d;
	}

}
