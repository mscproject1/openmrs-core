package com.home.springboot.entry;

public class Admissions {
    private String rowId;

    private String subjectId;

    private String hadmId;

    private String admittime;

    private String dischtime;

    private String deathtime;

    private String admissionType;

    private String admissionLocation;

    private String dischargeLocation;

    private String insurance;

    private String language;

    private String religion;

    private String maritalStatus;

    private String ethnicity;

    private String edregtime;

    private String edouttime;

    private String diagnosis;

    private String hospitalExpireFlag;

    private String hasCharteventsData;

    public String getRowId() {
        return rowId;
    }

    public void setRowId(String rowId) {
        this.rowId = rowId == null ? null : rowId.trim();
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId == null ? null : subjectId.trim();
    }

    public String getHadmId() {
        return hadmId;
    }

    public void setHadmId(String hadmId) {
        this.hadmId = hadmId == null ? null : hadmId.trim();
    }

    public String getAdmittime() {
        return admittime;
    }

    public void setAdmittime(String admittime) {
        this.admittime = admittime == null ? null : admittime.trim();
    }

    public String getDischtime() {
        return dischtime;
    }

    public void setDischtime(String dischtime) {
        this.dischtime = dischtime == null ? null : dischtime.trim();
    }

    public String getDeathtime() {
        return deathtime;
    }

    public void setDeathtime(String deathtime) {
        this.deathtime = deathtime == null ? null : deathtime.trim();
    }

    public String getAdmissionType() {
        return admissionType;
    }

    public void setAdmissionType(String admissionType) {
        this.admissionType = admissionType == null ? null : admissionType.trim();
    }

    public String getAdmissionLocation() {
        return admissionLocation;
    }

    public void setAdmissionLocation(String admissionLocation) {
        this.admissionLocation = admissionLocation == null ? null : admissionLocation.trim();
    }

    public String getDischargeLocation() {
        return dischargeLocation;
    }

    public void setDischargeLocation(String dischargeLocation) {
        this.dischargeLocation = dischargeLocation == null ? null : dischargeLocation.trim();
    }

    public String getInsurance() {
        return insurance;
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance == null ? null : insurance.trim();
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language == null ? null : language.trim();
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion == null ? null : religion.trim();
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus == null ? null : maritalStatus.trim();
    }

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity == null ? null : ethnicity.trim();
    }

    public String getEdregtime() {
        return edregtime;
    }

    public void setEdregtime(String edregtime) {
        this.edregtime = edregtime == null ? null : edregtime.trim();
    }

    public String getEdouttime() {
        return edouttime;
    }

    public void setEdouttime(String edouttime) {
        this.edouttime = edouttime == null ? null : edouttime.trim();
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis == null ? null : diagnosis.trim();
    }

    public String getHospitalExpireFlag() {
        return hospitalExpireFlag;
    }

    public void setHospitalExpireFlag(String hospitalExpireFlag) {
        this.hospitalExpireFlag = hospitalExpireFlag == null ? null : hospitalExpireFlag.trim();
    }

    public String getHasCharteventsData() {
        return hasCharteventsData;
    }

    public void setHasCharteventsData(String hasCharteventsData) {
        this.hasCharteventsData = hasCharteventsData == null ? null : hasCharteventsData.trim();
    }
}