package com.home.springboot.mapper;

import com.home.springboot.entry.Concept;
import com.home.springboot.entry.ConceptExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ConceptMapper {
    int countByExample(ConceptExample example);

    int deleteByExample(ConceptExample example);

    int insert(Concept record);

    int insertSelective(Concept record);

    List<Concept> selectByExample(ConceptExample example);

    int updateByExampleSelective(@Param("record") Concept record, @Param("example") ConceptExample example);

    int updateByExample(@Param("record") Concept record, @Param("example") ConceptExample example);

    Concept selectConcept(Integer count);

    int updateConceptId(@Param("conceptId") String conceptId, @Param("conceptid") String conceptid);
}