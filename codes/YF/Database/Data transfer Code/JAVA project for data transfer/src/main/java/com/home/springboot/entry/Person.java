package com.home.springboot.entry;

public class Person {
    private String personId;

    private String gender;

    private String birthdate;

    private String birthdateEstimated;

    private String dead;

    private String deathDate;

    private String causeOfDeath;

    private String creator;

    private String dateCreated;

    private String changedBy;

    private String dateChanged;

    private String voided;

    private String voidedBy;

    private String dateVoided;

    private String voidReason;

    private String uuid;

    private String deathdateEstimated;

    private String birthtime;

    private String causeOfDeathNonCoded;

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId == null ? null : personId.trim();
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender == null ? null : gender.trim();
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate == null ? null : birthdate.trim();
    }

    public String getBirthdateEstimated() {
        return birthdateEstimated;
    }

    public void setBirthdateEstimated(String birthdateEstimated) {
        this.birthdateEstimated = birthdateEstimated == null ? null : birthdateEstimated.trim();
    }

    public String getDead() {
        return dead;
    }

    public void setDead(String dead) {
        this.dead = dead == null ? null : dead.trim();
    }

    public String getDeathDate() {
        return deathDate;
    }

    public void setDeathDate(String deathDate) {
        this.deathDate = deathDate == null ? null : deathDate.trim();
    }

    public String getCauseOfDeath() {
        return causeOfDeath;
    }

    public void setCauseOfDeath(String causeOfDeath) {
        this.causeOfDeath = causeOfDeath == null ? null : causeOfDeath.trim();
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated == null ? null : dateCreated.trim();
    }

    public String getChangedBy() {
        return changedBy;
    }

    public void setChangedBy(String changedBy) {
        this.changedBy = changedBy == null ? null : changedBy.trim();
    }

    public String getDateChanged() {
        return dateChanged;
    }

    public void setDateChanged(String dateChanged) {
        this.dateChanged = dateChanged == null ? null : dateChanged.trim();
    }

    public String getVoided() {
        return voided;
    }

    public void setVoided(String voided) {
        this.voided = voided == null ? null : voided.trim();
    }

    public String getVoidedBy() {
        return voidedBy;
    }

    public void setVoidedBy(String voidedBy) {
        this.voidedBy = voidedBy == null ? null : voidedBy.trim();
    }

    public String getDateVoided() {
        return dateVoided;
    }

    public void setDateVoided(String dateVoided) {
        this.dateVoided = dateVoided == null ? null : dateVoided.trim();
    }

    public String getVoidReason() {
        return voidReason;
    }

    public void setVoidReason(String voidReason) {
        this.voidReason = voidReason == null ? null : voidReason.trim();
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid == null ? null : uuid.trim();
    }

    public String getDeathdateEstimated() {
        return deathdateEstimated;
    }

    public void setDeathdateEstimated(String deathdateEstimated) {
        this.deathdateEstimated = deathdateEstimated == null ? null : deathdateEstimated.trim();
    }

    public String getBirthtime() {
        return birthtime;
    }

    public void setBirthtime(String birthtime) {
        this.birthtime = birthtime == null ? null : birthtime.trim();
    }

    public String getCauseOfDeathNonCoded() {
        return causeOfDeathNonCoded;
    }

    public void setCauseOfDeathNonCoded(String causeOfDeathNonCoded) {
        this.causeOfDeathNonCoded = causeOfDeathNonCoded == null ? null : causeOfDeathNonCoded.trim();
    }
}