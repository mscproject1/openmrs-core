package com.home.springboot.mapper;

import com.home.springboot.entry.Patient;
import com.home.springboot.entry.PatientExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PatientMapper {
    int countByExample(PatientExample example);

    int deleteByExample(PatientExample example);

    int deleteByPrimaryKey(Integer patientId);

    int insert(Patient record);

    int insertSelective(Patient record);

    List<Patient> selectByExample(PatientExample example);

    Patient selectByPrimaryKey(Integer patientId);

    int updateByExampleSelective(@Param("record") Patient record, @Param("example") PatientExample example);

    int updateByExample(@Param("record") Patient record, @Param("example") PatientExample example);

    int updateByPrimaryKeySelective(Patient record);

    int updateByPrimaryKey(Patient record);

    Patient selectPatient(Integer count);

    int updatePid(@Param("pidnew") int pidnew, @Param("pidold") int pidold);
}