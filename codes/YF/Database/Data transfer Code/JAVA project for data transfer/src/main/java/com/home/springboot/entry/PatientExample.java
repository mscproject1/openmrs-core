package com.home.springboot.entry;

import java.util.ArrayList;
import java.util.List;

public class PatientExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PatientExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andPatientIdIsNull() {
            addCriterion("patient_id is null");
            return (Criteria) this;
        }

        public Criteria andPatientIdIsNotNull() {
            addCriterion("patient_id is not null");
            return (Criteria) this;
        }

        public Criteria andPatientIdEqualTo(Integer value) {
            addCriterion("patient_id =", value, "patientId");
            return (Criteria) this;
        }

        public Criteria andPatientIdNotEqualTo(Integer value) {
            addCriterion("patient_id <>", value, "patientId");
            return (Criteria) this;
        }

        public Criteria andPatientIdGreaterThan(Integer value) {
            addCriterion("patient_id >", value, "patientId");
            return (Criteria) this;
        }

        public Criteria andPatientIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("patient_id >=", value, "patientId");
            return (Criteria) this;
        }

        public Criteria andPatientIdLessThan(Integer value) {
            addCriterion("patient_id <", value, "patientId");
            return (Criteria) this;
        }

        public Criteria andPatientIdLessThanOrEqualTo(Integer value) {
            addCriterion("patient_id <=", value, "patientId");
            return (Criteria) this;
        }

        public Criteria andPatientIdIn(List<Integer> values) {
            addCriterion("patient_id in", values, "patientId");
            return (Criteria) this;
        }

        public Criteria andPatientIdNotIn(List<Integer> values) {
            addCriterion("patient_id not in", values, "patientId");
            return (Criteria) this;
        }

        public Criteria andPatientIdBetween(Integer value1, Integer value2) {
            addCriterion("patient_id between", value1, value2, "patientId");
            return (Criteria) this;
        }

        public Criteria andPatientIdNotBetween(Integer value1, Integer value2) {
            addCriterion("patient_id not between", value1, value2, "patientId");
            return (Criteria) this;
        }

        public Criteria andCreatorIsNull() {
            addCriterion("creator is null");
            return (Criteria) this;
        }

        public Criteria andCreatorIsNotNull() {
            addCriterion("creator is not null");
            return (Criteria) this;
        }

        public Criteria andCreatorEqualTo(String value) {
            addCriterion("creator =", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotEqualTo(String value) {
            addCriterion("creator <>", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorGreaterThan(String value) {
            addCriterion("creator >", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorGreaterThanOrEqualTo(String value) {
            addCriterion("creator >=", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorLessThan(String value) {
            addCriterion("creator <", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorLessThanOrEqualTo(String value) {
            addCriterion("creator <=", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorLike(String value) {
            addCriterion("creator like", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotLike(String value) {
            addCriterion("creator not like", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorIn(List<String> values) {
            addCriterion("creator in", values, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotIn(List<String> values) {
            addCriterion("creator not in", values, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorBetween(String value1, String value2) {
            addCriterion("creator between", value1, value2, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotBetween(String value1, String value2) {
            addCriterion("creator not between", value1, value2, "creator");
            return (Criteria) this;
        }

        public Criteria andDateCreatedIsNull() {
            addCriterion("date_created is null");
            return (Criteria) this;
        }

        public Criteria andDateCreatedIsNotNull() {
            addCriterion("date_created is not null");
            return (Criteria) this;
        }

        public Criteria andDateCreatedEqualTo(String value) {
            addCriterion("date_created =", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedNotEqualTo(String value) {
            addCriterion("date_created <>", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedGreaterThan(String value) {
            addCriterion("date_created >", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedGreaterThanOrEqualTo(String value) {
            addCriterion("date_created >=", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedLessThan(String value) {
            addCriterion("date_created <", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedLessThanOrEqualTo(String value) {
            addCriterion("date_created <=", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedLike(String value) {
            addCriterion("date_created like", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedNotLike(String value) {
            addCriterion("date_created not like", value, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedIn(List<String> values) {
            addCriterion("date_created in", values, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedNotIn(List<String> values) {
            addCriterion("date_created not in", values, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedBetween(String value1, String value2) {
            addCriterion("date_created between", value1, value2, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andDateCreatedNotBetween(String value1, String value2) {
            addCriterion("date_created not between", value1, value2, "dateCreated");
            return (Criteria) this;
        }

        public Criteria andChangedByIsNull() {
            addCriterion("changed_by is null");
            return (Criteria) this;
        }

        public Criteria andChangedByIsNotNull() {
            addCriterion("changed_by is not null");
            return (Criteria) this;
        }

        public Criteria andChangedByEqualTo(String value) {
            addCriterion("changed_by =", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByNotEqualTo(String value) {
            addCriterion("changed_by <>", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByGreaterThan(String value) {
            addCriterion("changed_by >", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByGreaterThanOrEqualTo(String value) {
            addCriterion("changed_by >=", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByLessThan(String value) {
            addCriterion("changed_by <", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByLessThanOrEqualTo(String value) {
            addCriterion("changed_by <=", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByLike(String value) {
            addCriterion("changed_by like", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByNotLike(String value) {
            addCriterion("changed_by not like", value, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByIn(List<String> values) {
            addCriterion("changed_by in", values, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByNotIn(List<String> values) {
            addCriterion("changed_by not in", values, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByBetween(String value1, String value2) {
            addCriterion("changed_by between", value1, value2, "changedBy");
            return (Criteria) this;
        }

        public Criteria andChangedByNotBetween(String value1, String value2) {
            addCriterion("changed_by not between", value1, value2, "changedBy");
            return (Criteria) this;
        }

        public Criteria andDateChangedIsNull() {
            addCriterion("date_changed is null");
            return (Criteria) this;
        }

        public Criteria andDateChangedIsNotNull() {
            addCriterion("date_changed is not null");
            return (Criteria) this;
        }

        public Criteria andDateChangedEqualTo(String value) {
            addCriterion("date_changed =", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedNotEqualTo(String value) {
            addCriterion("date_changed <>", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedGreaterThan(String value) {
            addCriterion("date_changed >", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedGreaterThanOrEqualTo(String value) {
            addCriterion("date_changed >=", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedLessThan(String value) {
            addCriterion("date_changed <", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedLessThanOrEqualTo(String value) {
            addCriterion("date_changed <=", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedLike(String value) {
            addCriterion("date_changed like", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedNotLike(String value) {
            addCriterion("date_changed not like", value, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedIn(List<String> values) {
            addCriterion("date_changed in", values, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedNotIn(List<String> values) {
            addCriterion("date_changed not in", values, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedBetween(String value1, String value2) {
            addCriterion("date_changed between", value1, value2, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andDateChangedNotBetween(String value1, String value2) {
            addCriterion("date_changed not between", value1, value2, "dateChanged");
            return (Criteria) this;
        }

        public Criteria andVoidedByIsNull() {
            addCriterion("voided_by is null");
            return (Criteria) this;
        }

        public Criteria andVoidedByIsNotNull() {
            addCriterion("voided_by is not null");
            return (Criteria) this;
        }

        public Criteria andVoidedByEqualTo(String value) {
            addCriterion("voided_by =", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByNotEqualTo(String value) {
            addCriterion("voided_by <>", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByGreaterThan(String value) {
            addCriterion("voided_by >", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByGreaterThanOrEqualTo(String value) {
            addCriterion("voided_by >=", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByLessThan(String value) {
            addCriterion("voided_by <", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByLessThanOrEqualTo(String value) {
            addCriterion("voided_by <=", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByLike(String value) {
            addCriterion("voided_by like", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByNotLike(String value) {
            addCriterion("voided_by not like", value, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByIn(List<String> values) {
            addCriterion("voided_by in", values, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByNotIn(List<String> values) {
            addCriterion("voided_by not in", values, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByBetween(String value1, String value2) {
            addCriterion("voided_by between", value1, value2, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andVoidedByNotBetween(String value1, String value2) {
            addCriterion("voided_by not between", value1, value2, "voidedBy");
            return (Criteria) this;
        }

        public Criteria andDateVoidedIsNull() {
            addCriterion("date_voided is null");
            return (Criteria) this;
        }

        public Criteria andDateVoidedIsNotNull() {
            addCriterion("date_voided is not null");
            return (Criteria) this;
        }

        public Criteria andDateVoidedEqualTo(String value) {
            addCriterion("date_voided =", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedNotEqualTo(String value) {
            addCriterion("date_voided <>", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedGreaterThan(String value) {
            addCriterion("date_voided >", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedGreaterThanOrEqualTo(String value) {
            addCriterion("date_voided >=", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedLessThan(String value) {
            addCriterion("date_voided <", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedLessThanOrEqualTo(String value) {
            addCriterion("date_voided <=", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedLike(String value) {
            addCriterion("date_voided like", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedNotLike(String value) {
            addCriterion("date_voided not like", value, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedIn(List<String> values) {
            addCriterion("date_voided in", values, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedNotIn(List<String> values) {
            addCriterion("date_voided not in", values, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedBetween(String value1, String value2) {
            addCriterion("date_voided between", value1, value2, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andDateVoidedNotBetween(String value1, String value2) {
            addCriterion("date_voided not between", value1, value2, "dateVoided");
            return (Criteria) this;
        }

        public Criteria andVoidReasonIsNull() {
            addCriterion("void_reason is null");
            return (Criteria) this;
        }

        public Criteria andVoidReasonIsNotNull() {
            addCriterion("void_reason is not null");
            return (Criteria) this;
        }

        public Criteria andVoidReasonEqualTo(String value) {
            addCriterion("void_reason =", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonNotEqualTo(String value) {
            addCriterion("void_reason <>", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonGreaterThan(String value) {
            addCriterion("void_reason >", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonGreaterThanOrEqualTo(String value) {
            addCriterion("void_reason >=", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonLessThan(String value) {
            addCriterion("void_reason <", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonLessThanOrEqualTo(String value) {
            addCriterion("void_reason <=", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonLike(String value) {
            addCriterion("void_reason like", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonNotLike(String value) {
            addCriterion("void_reason not like", value, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonIn(List<String> values) {
            addCriterion("void_reason in", values, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonNotIn(List<String> values) {
            addCriterion("void_reason not in", values, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonBetween(String value1, String value2) {
            addCriterion("void_reason between", value1, value2, "voidReason");
            return (Criteria) this;
        }

        public Criteria andVoidReasonNotBetween(String value1, String value2) {
            addCriterion("void_reason not between", value1, value2, "voidReason");
            return (Criteria) this;
        }

        public Criteria andAllergyStatusIsNull() {
            addCriterion("allergy_status is null");
            return (Criteria) this;
        }

        public Criteria andAllergyStatusIsNotNull() {
            addCriterion("allergy_status is not null");
            return (Criteria) this;
        }

        public Criteria andAllergyStatusEqualTo(String value) {
            addCriterion("allergy_status =", value, "allergyStatus");
            return (Criteria) this;
        }

        public Criteria andAllergyStatusNotEqualTo(String value) {
            addCriterion("allergy_status <>", value, "allergyStatus");
            return (Criteria) this;
        }

        public Criteria andAllergyStatusGreaterThan(String value) {
            addCriterion("allergy_status >", value, "allergyStatus");
            return (Criteria) this;
        }

        public Criteria andAllergyStatusGreaterThanOrEqualTo(String value) {
            addCriterion("allergy_status >=", value, "allergyStatus");
            return (Criteria) this;
        }

        public Criteria andAllergyStatusLessThan(String value) {
            addCriterion("allergy_status <", value, "allergyStatus");
            return (Criteria) this;
        }

        public Criteria andAllergyStatusLessThanOrEqualTo(String value) {
            addCriterion("allergy_status <=", value, "allergyStatus");
            return (Criteria) this;
        }

        public Criteria andAllergyStatusLike(String value) {
            addCriterion("allergy_status like", value, "allergyStatus");
            return (Criteria) this;
        }

        public Criteria andAllergyStatusNotLike(String value) {
            addCriterion("allergy_status not like", value, "allergyStatus");
            return (Criteria) this;
        }

        public Criteria andAllergyStatusIn(List<String> values) {
            addCriterion("allergy_status in", values, "allergyStatus");
            return (Criteria) this;
        }

        public Criteria andAllergyStatusNotIn(List<String> values) {
            addCriterion("allergy_status not in", values, "allergyStatus");
            return (Criteria) this;
        }

        public Criteria andAllergyStatusBetween(String value1, String value2) {
            addCriterion("allergy_status between", value1, value2, "allergyStatus");
            return (Criteria) this;
        }

        public Criteria andAllergyStatusNotBetween(String value1, String value2) {
            addCriterion("allergy_status not between", value1, value2, "allergyStatus");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}