package com.home.springboot.entry;

import java.util.ArrayList;
import java.util.List;

public class DIcdDiagnosesExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public DIcdDiagnosesExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andRowIdIsNull() {
            addCriterion("ROW_ID is null");
            return (Criteria) this;
        }

        public Criteria andRowIdIsNotNull() {
            addCriterion("ROW_ID is not null");
            return (Criteria) this;
        }

        public Criteria andRowIdEqualTo(String value) {
            addCriterion("ROW_ID =", value, "rowId");
            return (Criteria) this;
        }

        public Criteria andRowIdNotEqualTo(String value) {
            addCriterion("ROW_ID <>", value, "rowId");
            return (Criteria) this;
        }

        public Criteria andRowIdGreaterThan(String value) {
            addCriterion("ROW_ID >", value, "rowId");
            return (Criteria) this;
        }

        public Criteria andRowIdGreaterThanOrEqualTo(String value) {
            addCriterion("ROW_ID >=", value, "rowId");
            return (Criteria) this;
        }

        public Criteria andRowIdLessThan(String value) {
            addCriterion("ROW_ID <", value, "rowId");
            return (Criteria) this;
        }

        public Criteria andRowIdLessThanOrEqualTo(String value) {
            addCriterion("ROW_ID <=", value, "rowId");
            return (Criteria) this;
        }

        public Criteria andRowIdLike(String value) {
            addCriterion("ROW_ID like", value, "rowId");
            return (Criteria) this;
        }

        public Criteria andRowIdNotLike(String value) {
            addCriterion("ROW_ID not like", value, "rowId");
            return (Criteria) this;
        }

        public Criteria andRowIdIn(List<String> values) {
            addCriterion("ROW_ID in", values, "rowId");
            return (Criteria) this;
        }

        public Criteria andRowIdNotIn(List<String> values) {
            addCriterion("ROW_ID not in", values, "rowId");
            return (Criteria) this;
        }

        public Criteria andRowIdBetween(String value1, String value2) {
            addCriterion("ROW_ID between", value1, value2, "rowId");
            return (Criteria) this;
        }

        public Criteria andRowIdNotBetween(String value1, String value2) {
            addCriterion("ROW_ID not between", value1, value2, "rowId");
            return (Criteria) this;
        }

        public Criteria andIcd9CodeIsNull() {
            addCriterion("ICD9_CODE is null");
            return (Criteria) this;
        }

        public Criteria andIcd9CodeIsNotNull() {
            addCriterion("ICD9_CODE is not null");
            return (Criteria) this;
        }

        public Criteria andIcd9CodeEqualTo(String value) {
            addCriterion("ICD9_CODE =", value, "icd9Code");
            return (Criteria) this;
        }

        public Criteria andIcd9CodeNotEqualTo(String value) {
            addCriterion("ICD9_CODE <>", value, "icd9Code");
            return (Criteria) this;
        }

        public Criteria andIcd9CodeGreaterThan(String value) {
            addCriterion("ICD9_CODE >", value, "icd9Code");
            return (Criteria) this;
        }

        public Criteria andIcd9CodeGreaterThanOrEqualTo(String value) {
            addCriterion("ICD9_CODE >=", value, "icd9Code");
            return (Criteria) this;
        }

        public Criteria andIcd9CodeLessThan(String value) {
            addCriterion("ICD9_CODE <", value, "icd9Code");
            return (Criteria) this;
        }

        public Criteria andIcd9CodeLessThanOrEqualTo(String value) {
            addCriterion("ICD9_CODE <=", value, "icd9Code");
            return (Criteria) this;
        }

        public Criteria andIcd9CodeLike(String value) {
            addCriterion("ICD9_CODE like", value, "icd9Code");
            return (Criteria) this;
        }

        public Criteria andIcd9CodeNotLike(String value) {
            addCriterion("ICD9_CODE not like", value, "icd9Code");
            return (Criteria) this;
        }

        public Criteria andIcd9CodeIn(List<String> values) {
            addCriterion("ICD9_CODE in", values, "icd9Code");
            return (Criteria) this;
        }

        public Criteria andIcd9CodeNotIn(List<String> values) {
            addCriterion("ICD9_CODE not in", values, "icd9Code");
            return (Criteria) this;
        }

        public Criteria andIcd9CodeBetween(String value1, String value2) {
            addCriterion("ICD9_CODE between", value1, value2, "icd9Code");
            return (Criteria) this;
        }

        public Criteria andIcd9CodeNotBetween(String value1, String value2) {
            addCriterion("ICD9_CODE not between", value1, value2, "icd9Code");
            return (Criteria) this;
        }

        public Criteria andShortTitleIsNull() {
            addCriterion("SHORT_TITLE is null");
            return (Criteria) this;
        }

        public Criteria andShortTitleIsNotNull() {
            addCriterion("SHORT_TITLE is not null");
            return (Criteria) this;
        }

        public Criteria andShortTitleEqualTo(String value) {
            addCriterion("SHORT_TITLE =", value, "shortTitle");
            return (Criteria) this;
        }

        public Criteria andShortTitleNotEqualTo(String value) {
            addCriterion("SHORT_TITLE <>", value, "shortTitle");
            return (Criteria) this;
        }

        public Criteria andShortTitleGreaterThan(String value) {
            addCriterion("SHORT_TITLE >", value, "shortTitle");
            return (Criteria) this;
        }

        public Criteria andShortTitleGreaterThanOrEqualTo(String value) {
            addCriterion("SHORT_TITLE >=", value, "shortTitle");
            return (Criteria) this;
        }

        public Criteria andShortTitleLessThan(String value) {
            addCriterion("SHORT_TITLE <", value, "shortTitle");
            return (Criteria) this;
        }

        public Criteria andShortTitleLessThanOrEqualTo(String value) {
            addCriterion("SHORT_TITLE <=", value, "shortTitle");
            return (Criteria) this;
        }

        public Criteria andShortTitleLike(String value) {
            addCriterion("SHORT_TITLE like", value, "shortTitle");
            return (Criteria) this;
        }

        public Criteria andShortTitleNotLike(String value) {
            addCriterion("SHORT_TITLE not like", value, "shortTitle");
            return (Criteria) this;
        }

        public Criteria andShortTitleIn(List<String> values) {
            addCriterion("SHORT_TITLE in", values, "shortTitle");
            return (Criteria) this;
        }

        public Criteria andShortTitleNotIn(List<String> values) {
            addCriterion("SHORT_TITLE not in", values, "shortTitle");
            return (Criteria) this;
        }

        public Criteria andShortTitleBetween(String value1, String value2) {
            addCriterion("SHORT_TITLE between", value1, value2, "shortTitle");
            return (Criteria) this;
        }

        public Criteria andShortTitleNotBetween(String value1, String value2) {
            addCriterion("SHORT_TITLE not between", value1, value2, "shortTitle");
            return (Criteria) this;
        }

        public Criteria andLongTitleIsNull() {
            addCriterion("LONG_TITLE is null");
            return (Criteria) this;
        }

        public Criteria andLongTitleIsNotNull() {
            addCriterion("LONG_TITLE is not null");
            return (Criteria) this;
        }

        public Criteria andLongTitleEqualTo(String value) {
            addCriterion("LONG_TITLE =", value, "longTitle");
            return (Criteria) this;
        }

        public Criteria andLongTitleNotEqualTo(String value) {
            addCriterion("LONG_TITLE <>", value, "longTitle");
            return (Criteria) this;
        }

        public Criteria andLongTitleGreaterThan(String value) {
            addCriterion("LONG_TITLE >", value, "longTitle");
            return (Criteria) this;
        }

        public Criteria andLongTitleGreaterThanOrEqualTo(String value) {
            addCriterion("LONG_TITLE >=", value, "longTitle");
            return (Criteria) this;
        }

        public Criteria andLongTitleLessThan(String value) {
            addCriterion("LONG_TITLE <", value, "longTitle");
            return (Criteria) this;
        }

        public Criteria andLongTitleLessThanOrEqualTo(String value) {
            addCriterion("LONG_TITLE <=", value, "longTitle");
            return (Criteria) this;
        }

        public Criteria andLongTitleLike(String value) {
            addCriterion("LONG_TITLE like", value, "longTitle");
            return (Criteria) this;
        }

        public Criteria andLongTitleNotLike(String value) {
            addCriterion("LONG_TITLE not like", value, "longTitle");
            return (Criteria) this;
        }

        public Criteria andLongTitleIn(List<String> values) {
            addCriterion("LONG_TITLE in", values, "longTitle");
            return (Criteria) this;
        }

        public Criteria andLongTitleNotIn(List<String> values) {
            addCriterion("LONG_TITLE not in", values, "longTitle");
            return (Criteria) this;
        }

        public Criteria andLongTitleBetween(String value1, String value2) {
            addCriterion("LONG_TITLE between", value1, value2, "longTitle");
            return (Criteria) this;
        }

        public Criteria andLongTitleNotBetween(String value1, String value2) {
            addCriterion("LONG_TITLE not between", value1, value2, "longTitle");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}