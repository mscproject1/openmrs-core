package com.home.springboot.mapper;

import com.home.springboot.entry.DIcdDiagnoses;
import com.home.springboot.entry.DIcdDiagnosesExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DIcdDiagnosesMapper {
    int countByExample(DIcdDiagnosesExample example);

    int deleteByExample(DIcdDiagnosesExample example);

    int insert(DIcdDiagnoses record);

    int insertSelective(DIcdDiagnoses record);

    List<DIcdDiagnoses> selectByExample(DIcdDiagnosesExample example);

    int updateByExampleSelective(@Param("record") DIcdDiagnoses record, @Param("example") DIcdDiagnosesExample example);

    int updateByExample(@Param("record") DIcdDiagnoses record, @Param("example") DIcdDiagnosesExample example);

    DIcdDiagnoses selectDIcdDiagnoses(Integer count);
}