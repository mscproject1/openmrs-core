package com.home.springboot.entry;

public class ConceptDescription {
    private String conceptDescriptionId;

    private String conceptId;

    private String description;

    private String locale;

    private String creator;

    private String dateCreated;

    private String changedBy;

    private String dateChanged;

    private String uuid;

    public String getConceptDescriptionId() {
        return conceptDescriptionId;
    }

    public void setConceptDescriptionId(String conceptDescriptionId) {
        this.conceptDescriptionId = conceptDescriptionId == null ? null : conceptDescriptionId.trim();
    }

    public String getConceptId() {
        return conceptId;
    }

    public void setConceptId(String conceptId) {
        this.conceptId = conceptId == null ? null : conceptId.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale == null ? null : locale.trim();
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated == null ? null : dateCreated.trim();
    }

    public String getChangedBy() {
        return changedBy;
    }

    public void setChangedBy(String changedBy) {
        this.changedBy = changedBy == null ? null : changedBy.trim();
    }

    public String getDateChanged() {
        return dateChanged;
    }

    public void setDateChanged(String dateChanged) {
        this.dateChanged = dateChanged == null ? null : dateChanged.trim();
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid == null ? null : uuid.trim();
    }
}