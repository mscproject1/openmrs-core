package com.home.springboot.controller;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.home.springboot.entry.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;
import com.home.springboot.service.ObsService;


@RestController
@RequestMapping("/userController")
@CrossOrigin
@EnableAsync
public class ObsController {

	@Autowired
	ObsService obsservice;

	@Async
	@Scheduled(cron = "0 30 12 ? * *")
//	@Scheduled(cron = "*/5 * * * * ?")
	public void obs() throws ParseException {
		// Define date type data conversion methods
		SimpleDateFormat sf = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss");
		//Defining variable data
		String year = "";
		String month = "";
		String day = "";
		String hour = "";
		String minute = "";
		String second = "";
		String date = null;
		int i = 0;
		//Traversing the entire table
		while(i < 44550){
			//Get obs table data
			Obs obs = obsservice.selectObs(i);
			//obs_datetime field handling
			//Random generation of logical month, day, hour and minute data
			double year0 = Math.random();
			double month0 = Math.ceil(Math.random() * 12);
			double day0 = Math.ceil(Math.random() * 30);
			double hour0 = Math.ceil(Math.random() * 24);
			double minute0 = Math.ceil(Math.random() * 60);
			double second0 = Math.ceil(Math.random() * 60);
			//Data type conversion
			BigDecimal month1 = new BigDecimal(month0);
			BigDecimal day1 = new BigDecimal(day0);
			BigDecimal hour1 = new BigDecimal(hour0);
			BigDecimal minute1 = new BigDecimal(minute0);
			BigDecimal second1 = new BigDecimal(second0);
			BigDecimal s = new BigDecimal("3");
			BigDecimal l = new BigDecimal("6");
			BigDecimal d = new BigDecimal("28");
			BigDecimal m = null;
			//Year: 2020 or 2021
			if (year0 <= 0.5) {
				year = "0";
			} else {
				year = "1";
			}
			//Months: March-December for 2020, January-June for 2021, 0 before single digit
			if (year0 <= 0.5 && month1.toString().length() < 2) {
				//Month greater than or equal to 3, go straight to month
				if (month1.compareTo(s) > -1){
					month = "0" + month1.toString();
				}
				//Month less than 3, plus 3
				else {
					m = month1.add(s);
					month = "0" + m.toString();
				}
			}
			else {
				//2020 with double-digit months -> months greater than 3
				if (year0 <= 0.5 && month1.toString().length() == 2 ){
					if (month1.compareTo(s) > -1){
						month = month1.toString();
					}
					//Month less than 3, plus 3
					else {
						m = month1.add(s);
						month = m.toString();
					}
				}
				//2021, month in single digits -> month preceded by 0 and less than 6
				else {
					if (year0 > 0.5 && month1.toString().length() < 2) {
						//Month less than or equal to 6, go straight to month
						if (month1.compareTo(l) < 1) {
							month = "0" + month1.toString();
						}
						//Month greater than 6, minus 6
						else {
							m = month1.subtract(l);
							month = "0" + m.toString();
						}
					}
					//2021 with two-digit month -> month preceded by 0, minus 6
					else {
						m = month1.subtract(l);
						month = "0" + m.toString();
					}
				}
			}
			//Number of days: 0 before each digit
			if (day1.toString().length() < 2) {
				day = "0" + day1.toString();
			} else {
				day = day1.toString();
			}
			//Hours: 0 before each digit
			if (hour1.toString().length() < 2) {
				hour = "0" + hour1.toString();
			} else {
				hour = hour1.toString();
			}
			//Minutes: 0 before single digit
			if (minute1.toString().length() < 2) {
				minute = "0" + minute1.toString();
			} else {
				minute = minute1.toString();
			}
			//Seconds: 0 before the digit
			if (second1.toString().length() < 2) {
				second = "0" + second1.toString();
			} else {
				second = second1.toString();
			}
			//Month is 2 when the maximum number of days is 27
			if (month == "02" && day1.compareTo(d) == 1){
				day = day1.subtract(s).toString();
			}
			//Splice to full string type date data
			date = "202" + year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" +second;
			//Convert string data to date
			Date date0 = sf.parse(date);
			//Replace original data
			obs.setObsDatetime(date0);
			//date_created field processing
			//Dates in the range 2000-01-01 to 2021-06-01
			double year1 = Math.ceil(Math.random() * 21);
			BigDecimal year11 = new BigDecimal(year1);
			if (year11.toString().length() < 2){
				year = "0" + year11.toString();
			} else {
				year = year11.toString();
			}
			date = "20" + year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" +second;
			Date date1 = sf.parse(date);
			obs.setDateCreated(date1);
			//value_numeric field handling
			double v0 = Math.ceil(Math.random() * 3000) / 100 + 50;
			NumberFormat nf = NumberFormat.getNumberInstance();
			nf.setMaximumFractionDigits(2);
			String v1 = nf.format(v0);
			double v;
			v = Double.parseDouble(v1);
			if(i >= 294799){
				obs.setValueNumeric(null);
			} else {
				obs.setValueNumeric(v);
			}
			//Value_drug field handling
//			Integer valueCoded = obs.getValueCoded();
//			if (valueCoded != null && valueCoded > 0){
//				obs.setValueDrug(20);
//			}
			// Modify obs table data
			int i1 = obsservice.updateObs(obs);
			// Print the results of the changes
			System.out.println(i1);
			i++;
		}
		//obs table modification complete
		System.out.println("job done!");
	}

	@Async
	@Scheduled(cron = "0 14 22 ? * *")
//	@Scheduled(cron = "*/5 * * * * ?")
	public void person(){
		int i = 0;
		// traversing the entire table data
		while (i < 44550){
			// Get person table data
			Person personold = obsservice.selectPerson(i);
			//Get data from the deathdate field
			String deathDateold = personold.getDeathDate();
			//Determine if deathdate is empty
			if (deathDateold != null && deathDateold.length() > 0){
				//intercept the deathdate field for modification
				String s1 = deathDateold.substring(0, 6);
				String s2 = deathDateold.substring(6, 7);
				String s3 = deathDateold.substring(7, deathDateold.length());
				// Convert intercepted string type fields to int type
				int i1 = Integer.parseInt(s2);
				//Field assignment
				i1 = (int) Math.ceil(Math.random() * 6);
				// Convert modified fields to string type
				String s21 = Integer.toString(i1);
				// Spliced into new deathdate field
				String deathDatenew = s1 + s21 + s3;
				// Get the personId of this data
				String personId = personold.getPersonId();
				// assign a new personId to this data
				String personid = Integer.toString(i+1);
				// Modify data
				int i2 = obsservice.updatePerson(personid, deathDatenew, personId);
				//printing changes
				System.out.println(i2);
			}
			i++;
		}
		//Person table operation complete
		System.out.println("job done!");
	}

	@Async
	@Scheduled(cron = "0 11 13 ? * *")
//	@Scheduled(cron = "*/5 * * * * ?")
	public void changeCtoP(){
		int i = 0;
		// traversing the entire table data
		while (i < 17084){
			//Get data from the concept table
			Concept concept = obsservice.selectConcrpt(i);
			//Get the datecreated field data
			String concept_dateCreated = concept.getDateCreated();
			// Get person table data
			Person person = obsservice.selectPerson(i);
			// Get the personId field data
			String personId = person.getPersonId();
			//replace the person table field with a datecreated field
			int i1 = obsservice.updateCtoP(concept_dateCreated, personId);
			// Print the results of the changes
			System.out.println(i1);
			i++;
		}
		//person table modification complete
		System.out.println("job done!");
	}

	@Async
	@Scheduled(cron = "0 18 8 ? * *")
//	@Scheduled(cron = "*/5 * * * * ?")
	public void modifyTimes (){
		int i = 0;
		//Traversing the entire table
		while(i < 58976){
			//Get admissions table data
			Admissions admissions = obsservice.selectAdmissions(i);
			//Get the data to be modified
			String admittime = admissions.getAdmittime();
			String dischtime = admissions.getDischtime();
			//Intercepting the data to be modified
			String admittimestring1 = admittime.substring(0,3);
			String dischtimestring1 = dischtime.substring(0,3);
			String admittimestring2 = admittime.substring(4,admittime.length());
			String dischtimestring2 = dischtime.substring(4,dischtime.length());
			//Convert the part to be modified to int type data
			int admittimeint = Integer.parseInt(admittimestring1);
			int dischtimeint = Integer.parseInt(dischtimestring1);
			//Modify data
			int admittimeintnew = admittimeint - 180;
			int dischtimeintnew = dischtimeint - 180;
			//Convert modified data to string type
			String admittimestringnew1 = Integer.toString(admittimeintnew);
			String dischtimeistringnew1 = Integer.toString(dischtimeintnew);
			//Spliced to modified complete data
			String admittimestringnew = admittimestringnew1 + admittimestring2;
			String dischtimeistringnew = dischtimeistringnew1 + dischtimestring2;
			//Replace the modified data with this entry
			admissions.setAdmittime(admittimestringnew);
			admissions.setDischtime(dischtimeistringnew);
			//Modify data
			int i1 = obsservice.updateByPrimaryKey(admissions);
			//printing changes
			System.out.println(i1);
			i++;
		}
		//Admissions form changes complete
		System.out.println("job done!");
	}

	@Async
	@Scheduled(cron = "0 18 8 ? * *")
//	@Scheduled(cron = "*/5 * * * * ?")
	public void updatePid(){
		int i = 0;
		//Traversing the entire table
		while (i < 46520){
			//Get data from the patients table
			Patients patients = obsservice.selectPatients(i);
			//Get the row_id field
			String rowId = patients.getRowId();
			//Data type conversion
			int rowid = Integer.parseInt(rowId);
			//Get patient table data
			Patient patient = obsservice.selectPatient(i);
			//Get patient_id data
			Integer patientId = patient.getPatientId();
			//Modify patient table data
			int i1 = obsservice.updatePid(rowid, patientId);
			//Print the changes
			System.out.println(i1);
			i++;
		}
		//patient form modification complete
		System.out.println("job done!");
	}
	@Async
	@Scheduled(cron = "0 18 8 ? * *")
//	@Scheduled(cron = "*/5 * * * * ?")
	public void updatePerson(){
		int i =0;
		//Traversing the entire table
		while (i < 46520){
			//Get data from the patients table
			Patients patients = obsservice.selectPatients(i);
			//Get the required field data
			String subjectId = patients.getSubjectId();
			String gender = patients.getGender();
			String dob = patients.getDob();
			String dod = patients.getDod();
			String expireFlag = patients.getExpireFlag();
			//Get person table data
			Person person = obsservice.selectPerson(i);
			String personId = person.getPersonId();
			//Modify person table
			int i1 = obsservice.updatePerson(subjectId, gender, dob, dod, expireFlag, personId);
			//Print the changes
			System.out.println(i1);
			i++;
		}
		//Person table modification complete
		System.out.println("job done!");
	}

	@Async
	@Scheduled(cron = "0 18 8 ? * *")
//	@Scheduled(cron = "*/5 * * * * ?")
	public void updateConceptId(){
		int i = 0;
		// traversing the whole table
		while (i < 14567){
			//Get d_icd_diagnoses table data
			DIcdDiagnoses dIcdDiagnoses = obsservice.selectDicddiagnoses(i);
			//Get the data you need
			String icd9Code = dIcdDiagnoses.getIcd9Code();
			String shortTitle = dIcdDiagnoses.getShortTitle();
			String longTitle = dIcdDiagnoses.getLongTitle();
			//Get the data of the table to be modified
			Concept concept = obsservice.selectConcrpt(i);
			ConceptName conceptName = obsservice.selectConceptName(i);
			ConceptDescription conceptDescription = obsservice.selectConceptDescription(i);
			//Get id data
			String conceptId = concept.getConceptId();
			String conceptNameId = conceptName.getConceptNameId();
			String conceptDescriptionId = conceptDescription.getConceptDescriptionId();
			//Modify data
			int i1 = obsservice.updateConceptId(icd9Code, conceptId);
			int i2 = obsservice.updateConceptName(shortTitle, conceptNameId);
			int i3 = obsservice.updateConceptDescription(conceptDescriptionId, conceptDescriptionId);
			//Print the changes
			System.out.println(i1+"---"+i2+"---"+i3);
			i++;
		}
		//Modifications completed
		System.out.println("job done!");
	}

	@Async
	@Scheduled(cron = "0 18 8 ? * *")
//	@Scheduled(cron = "*/5 * * * * ?")
	public void updateVisit(){
		int i =0;
		//Traversing the entire table
		while (i < 44550){
			//Get table data
			Admissions admissions = obsservice.selectAdmissions(i);
			Visit visit = obsservice.selectVisit(i);
			//Get the data you need
			String subjectId = admissions.getSubjectId();
			String deathtime = admissions.getDeathtime();
			String dischtime = admissions.getDischtime();
			String visitId = visit.getVisitId();
			//Modify the visit table
			int i1 = obsservice.updateVisit(subjectId, deathtime, dischtime, visitId);
			//Print the changes
			System.out.println(i1);
			i++;
		}
		//Visit form modification complete
		System.out.println("job done!");
	}


}
