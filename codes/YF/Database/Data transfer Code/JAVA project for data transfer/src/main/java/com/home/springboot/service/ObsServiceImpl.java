package com.home.springboot.service;

import com.home.springboot.entry.*;

import com.home.springboot.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ObsServiceImpl implements ObsService {

	@Autowired
	PatientMapper patientMapper;
	@Autowired
	PatientCopy1Mapper patientCopy1Mapper;
	@Autowired
	PersonCopy1Mapper personCopy1Mapper;
	@Autowired
	ObsMapper obsMapper;
	@Autowired
	PersonMapper personMapper;
	@Autowired
	ConceptMapper conceptMapper;
	@Autowired
	AdmissionsMapper admissionsMapper;
	@Autowired
	PatientsMapper patientsMapper;
	@Autowired
	DIcdDiagnosesMapper dIcdDiagnosesMapper;
	@Autowired
	ConceptNameMapper conceptNameMapper;
	@Autowired
	ConceptDescriptionMapper conceptDescriptionMapper;
	@Autowired
	VisitMapper visitMapper;


	@Override
	public Person selectPerson(int count) {
		return personMapper.selectPerson(count);
	}

	@Override
	public int updatePerson(String personid, String deathdate, String personId) {
		return personMapper.updateByPrimaryKey(personid,deathdate,personId);
	}

	@Override
	public Concept selectConcrpt(int count) {
		return conceptMapper.selectConcept(count);
	}

	@Override
	public Patient selectPatient(int count) {
		return patientMapper.selectPatient(count);
	}


	@Override
	public Obs selectObs(int count) {
		return obsMapper.selectObs(count);
	}

	@Override
	public int updateObs(Obs obs) {
		return obsMapper.updateByPrimaryKey(obs);
	}

	@Override
	public int updateCtoP(String dateCreated, String personId) {
		return personMapper.updateCtoP(dateCreated, personId);
	}

	@Override
	public Admissions selectAdmissions(int count) {
		return admissionsMapper.selectAdmissions(count);
	}

	@Override
	public int updateByPrimaryKey(Admissions admissions) {
		return admissionsMapper.updateByPrimaryKey(admissions);
	}

	@Override
	public Patients selectPatients(int count) {
		return patientsMapper.selectPatients(count);
	}

	@Override
	public int updatePid(int pidnew, int pidold) {
		return patientMapper.updatePid(pidnew, pidold);
	}

	@Override
	public int updatePerson(String subjectId, String gender, String DOB, String DOD, String expireFlag, String personID) {
		return personMapper.updatePerson(subjectId, gender, DOB, DOD ,expireFlag, personID);
	}

	@Override
	public DIcdDiagnoses selectDicddiagnoses(int count) {
		return dIcdDiagnosesMapper.selectDIcdDiagnoses(count);
	}

	@Override
	public ConceptName selectConceptName(int count) {
		return conceptNameMapper.selectConceptName(count);
	}

	@Override
	public ConceptDescription selectConceptDescription(int count) {
		return conceptDescriptionMapper.selectConceptDescription(count);
	}

	@Override
	public int updateConceptId(String Id, String id) {
		return conceptMapper.updateConceptId(Id, id);
	}

	@Override
	public int updateConceptName(String name, String id) {
		return conceptNameMapper.updateConceptName(name, id);
	}

	@Override
	public int updateConceptDescription(String description, String id) {
		return conceptDescriptionMapper.updateConceptDescription(description, id);
	}

	@Override
	public Visit selectVisit(int count) {
		return visitMapper.sieletVisit(count);
	}

	@Override
	public int updateVisit(String patientId, String dateStarted, String dateStopped, String id) {
		return visitMapper.updateVisit(patientId, dateStarted, dateStopped, id);
	}


}
