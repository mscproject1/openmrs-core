package com.home.springboot.mapper;

import com.home.springboot.entry.Visit;
import com.home.springboot.entry.VisitExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface VisitMapper {
    int countByExample(VisitExample example);

    int deleteByExample(VisitExample example);

    int insert(Visit record);

    int insertSelective(Visit record);

    List<Visit> selectByExample(VisitExample example);

    int updateByExampleSelective(@Param("record") Visit record, @Param("example") VisitExample example);

    int updateByExample(@Param("record") Visit record, @Param("example") VisitExample example);

    Visit sieletVisit(Integer count);

    int updateVisit(@Param("patientId") String patientId,
                    @Param("dateStarted") String dateStarted,
                    @Param("dateStopped") String dateStopped,
                    @Param("id") String id);
}