package com.home.springboot.mapper;

import com.home.springboot.entry.PatientCopy1;
import com.home.springboot.entry.PatientCopy1Example;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PatientCopy1Mapper {
    int countByExample(PatientCopy1Example example);

    int deleteByExample(PatientCopy1Example example);

    int deleteByPrimaryKey(Integer patientId);

    int insert(PatientCopy1 record);

    int insertSelective(PatientCopy1 record);

    List<PatientCopy1> selectByExample(PatientCopy1Example example);

    PatientCopy1 selectByPrimaryKey(Integer patientId);

    int updateByExampleSelective(@Param("record") PatientCopy1 record, @Param("example") PatientCopy1Example example);

    int updateByExample(@Param("record") PatientCopy1 record, @Param("example") PatientCopy1Example example);

    int updateByPrimaryKeySelective(PatientCopy1 record);

    int updateByPrimaryKey(PatientCopy1 record);
}