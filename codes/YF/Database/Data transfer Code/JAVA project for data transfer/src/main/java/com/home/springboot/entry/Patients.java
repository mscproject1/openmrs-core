package com.home.springboot.entry;

public class Patients {
    private String rowId;

    private String subjectId;

    private String gender;

    private String dob;

    private String dod;

    private String dodHosp;

    private String dodSsn;

    private String expireFlag;

    public String getRowId() {
        return rowId;
    }

    public void setRowId(String rowId) {
        this.rowId = rowId == null ? null : rowId.trim();
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId == null ? null : subjectId.trim();
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender == null ? null : gender.trim();
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob == null ? null : dob.trim();
    }

    public String getDod() {
        return dod;
    }

    public void setDod(String dod) {
        this.dod = dod == null ? null : dod.trim();
    }

    public String getDodHosp() {
        return dodHosp;
    }

    public void setDodHosp(String dodHosp) {
        this.dodHosp = dodHosp == null ? null : dodHosp.trim();
    }

    public String getDodSsn() {
        return dodSsn;
    }

    public void setDodSsn(String dodSsn) {
        this.dodSsn = dodSsn == null ? null : dodSsn.trim();
    }

    public String getExpireFlag() {
        return expireFlag;
    }

    public void setExpireFlag(String expireFlag) {
        this.expireFlag = expireFlag == null ? null : expireFlag.trim();
    }
}