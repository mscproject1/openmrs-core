package com.home.springboot.mapper;

import com.home.springboot.entry.Person;
import com.home.springboot.entry.PersonExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PersonMapper {
    int countByExample(PersonExample example);

    int deleteByExample(PersonExample example);

    int insert(Person record);

    int insertSelective(Person record);

    List<Person> selectByExample(PersonExample example);

    int updateByExampleSelective(@Param("record") Person record, @Param("example") PersonExample example);

    int updateByExample(@Param("record") Person record, @Param("example") PersonExample example);

    Person selectPerson(Integer count);

    int updateByPrimaryKey(@Param("personid") String personid, @Param("deathDate") String deathDate, @Param("personId") String personId);

    int updateCtoP(@Param("dateCreated") String dateCreated, @Param("personId") String personId);

    int updatePerson(@Param("subjectId") String subjectId,
                     @Param("gender") String gender,
                     @Param("DOB") String DOB,
                     @Param("DOD") String DOD,
                     @Param("expire_flag") String expireFlag,
                     @Param("personId") String personId);

}